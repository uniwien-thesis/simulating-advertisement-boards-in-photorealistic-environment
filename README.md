# Simulating advertisement boards in photoreaslistic 3D environment

![Render of the final scene](./src/assets/textures/loading-bg.jpg)

## Requirements

`node.js` >= v12.18.3

`npm` >= 7.16.0

## Run local development server

In order to start a dev server execute following commands in the root of the project:

1. `npm install` - to install all external dependencies that are required for project to run.

2. `npm run start` - to start the local development server.

## Port

The application is available at localhost::4200 by default. If the post is taken, the prompt will ask to provide another port to serve on.

## Ad blocker

Please, disable ad blocker for the page in your browser. The ad blocker conflicts with Three.js and blocks loading of some of the 3D assets.

