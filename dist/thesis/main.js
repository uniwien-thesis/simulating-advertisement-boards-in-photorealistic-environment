(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var three_examples_jsm_loaders_GLTFLoader_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! three/examples/jsm/loaders/GLTFLoader.js */ "./node_modules/three/examples/jsm/loaders/GLTFLoader.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _view_port_3d__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./view-port-3d */ "./src/app/view-port-3d/index.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");







const RATIO_2X3 = '2x3';
const RATIO_5X2 = '5x2';
const RATIO_5X3 = '5x3';
class AppComponent {
    constructor() {
        this.title = 'thesis';
        this.textureLoader = new three__WEBPACK_IMPORTED_MODULE_1__["TextureLoader"]();
        this.gltfLoader = new three_examples_jsm_loaders_GLTFLoader_js__WEBPACK_IMPORTED_MODULE_2__["GLTFLoader"]();
        // preload all textures before running app
        Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["combineLatest"])([
            Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["from"])(this.loadBakedTextures()),
            Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["from"])(this.loadLightMapTextures()),
            Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["from"])(this.loadAdsTemplateTextures()),
            Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["from"])(this.loadVideoTextures()),
        ])
            .pipe()
            .subscribe(([combinedTexturesMap, lightmapTexture, adsInitialTexturesMap, videoTextures,]) => {
            new _view_port_3d__WEBPACK_IMPORTED_MODULE_4__["ViewPort3D"]({
                combinedTexturesMap,
                videoTextures,
                adsInitialTexturesMap,
                lightmapTexture,
            }).awake();
        });
    }
    loadAdsTemplateTextures() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const ELEMENTS_SIZE = 3;
            const textures = {};
            const names = [
                { type: RATIO_5X2, path: 'advertisement-5x2.jpg' },
                { type: RATIO_5X3, path: 'advertisement-5x3.jpg' },
                { type: RATIO_2X3, path: 'advertisement-2x3.jpg' },
            ];
            for (let i = 0; i < names.length; i++) {
                const item = names[i];
                const path = `assets/textures/ads/templates/${item.path}`;
                const texture = yield this.textureLoader.loadAsync(path);
                textures[item.type] = texture;
            }
            return textures;
        });
    }
    loadLightMapTextures() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const path = `assets/textures/lightmaps/lightmap.jpg`;
            const texture = yield this.textureLoader.loadAsync(path);
            return texture;
        });
    }
    loadBakedTextures() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const ELEMENTS_SIZE = 43;
            // const ELEMENTS_SIZE = 1a
            const textures = {};
            for (let i = 1; i <= ELEMENTS_SIZE; i++) {
                const key = `part-${i}`;
                const textureName = `${key}_Bake1_cyclesbake_COMBINED.jpg`;
                const path = `assets/textures/baked/${textureName}`;
                const texture = yield this.textureLoader.loadAsync(path);
                textures[key] = texture;
            }
            return textures;
        });
    }
    loadVideoTextures() {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const textures = {};
            const EXAMPLE_VIDEO_KEY = 'example-video';
            for (let i = 1; i < 7; i++) {
                var video = document.createElement('video');
                video.src = `assets/textures/videos/${EXAMPLE_VIDEO_KEY}-${i}.mp4`;
                video.loop = true;
                // DO I NEED TO PLAY IT FROM HERE???
                video.play();
                const videoTexture = new three__WEBPACK_IMPORTED_MODULE_1__["VideoTexture"](video);
                textures[i] = videoTexture;
            }
            return textures;
        });
    }
    loadTexture(path) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return this.textureLoader.loadAsync(path);
        });
    }
    loadModels(path) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            return this.gltfLoader.loadAsync(path);
        });
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 13, vars: 0, consts: [["id", "container"], ["id", "stats"], ["type", "file", "id", "texture-upload", "accept", ".jpg, .png, .jpeg, .mp4", 2, "display", "none"], [1, "loading-screen"], [1, "container"], [1, "anim"], [1, "el"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](2, "input", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](3, "div", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](4, "div", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementStart"](5, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](6, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](7, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](8, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](10, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](11, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelement"](12, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵelementEnd"]();
    } }, styles: [".loading-screen[_ngcontent-%COMP%] {\n  width: 100vw;\n  height: 100vh;\n\n  position: absolute;\n  top: 0;\n  left: 0;\n\n  background-image: url('/assets/loading-bg.jpg');\n}\n\n.hidden[_ngcontent-%COMP%] {\n  display: none;\n}\n\n.container[_ngcontent-%COMP%] {\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  height: 100vh;\n}\n\n.anim[_ngcontent-%COMP%] {\n  width: 50px;\n  height: 50px;\n  position: relative;\n  animation: global 2.5s infinite linear both;\n}\n\n.el[_ngcontent-%COMP%] {\n  position: absolute;\n  width: 100%;\n  height: 100%;\n  left: 0;\n  top: 0;\n  animation: local 2s infinite ease-in-out both;\n}\n\n.el[_ngcontent-%COMP%]:before {\n  content: '';\n  display: block;\n  width: 25%;\n  height: 25%;\n  background-color: lightgreen;\n  border-radius: 100%;\n  animation: el 2s infinite ease-in-out both;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(1) {\n  animation-delay: -1.1s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(2) {\n  animation-delay: -1s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(3) {\n  animation-delay: -0.9s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(4) {\n  animation-delay: -0.8s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(5) {\n  animation-delay: -0.7s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(6) {\n  animation-delay: -0.6s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(7) {\n  animation-delay: -0.5s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(8) {\n  animation-delay: -0.4s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(1):before {\n  animation-delay: -1.1s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(2):before {\n  animation-delay: -1s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(3):before {\n  animation-delay: -0.9s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(4):before {\n  animation-delay: -0.8s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(5):before {\n  animation-delay: -0.7s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(6):before {\n  animation-delay: -0.6s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(7):before {\n  animation-delay: -0.5s;\n}\n\n.el[_ngcontent-%COMP%]:nth-child(8):before {\n  animation-delay: -0.4s;\n}\n\n@keyframes global {\n  100% {\n    transform: rotate(360deg);\n  }\n}\n\n@keyframes el {\n  50% {\n    transform: scale(0.4);\n  }\n  100%,\n  0% {\n    transform: scale(1);\n  }\n}\n\n@keyframes local {\n  80%,\n  100% {\n    transform: rotate(360deg);\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxZQUFZO0VBQ1osYUFBYTs7RUFFYixrQkFBa0I7RUFDbEIsTUFBTTtFQUNOLE9BQU87O0VBRVAsK0NBQStDO0FBQ2pEOztBQUVBO0VBQ0UsYUFBYTtBQUNmOztBQUVBO0VBQ0UsYUFBYTtFQUNiLHVCQUF1QjtFQUN2QixtQkFBbUI7RUFDbkIsYUFBYTtBQUNmOztBQUVBO0VBQ0UsV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsMkNBQTJDO0FBQzdDOztBQUVBO0VBQ0Usa0JBQWtCO0VBQ2xCLFdBQVc7RUFDWCxZQUFZO0VBQ1osT0FBTztFQUNQLE1BQU07RUFDTiw2Q0FBNkM7QUFDL0M7O0FBRUE7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLFVBQVU7RUFDVixXQUFXO0VBQ1gsNEJBQTRCO0VBQzVCLG1CQUFtQjtFQUNuQiwwQ0FBMEM7QUFDNUM7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxvQkFBb0I7QUFDdEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxvQkFBb0I7QUFDdEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBQ0E7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRTtJQUNFLHlCQUF5QjtFQUMzQjtBQUNGOztBQUVBO0VBQ0U7SUFDRSxxQkFBcUI7RUFDdkI7RUFDQTs7SUFFRSxtQkFBbUI7RUFDckI7QUFDRjs7QUFFQTtFQUNFOztJQUVFLHlCQUF5QjtFQUMzQjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubG9hZGluZy1zY3JlZW4ge1xuICB3aWR0aDogMTAwdnc7XG4gIGhlaWdodDogMTAwdmg7XG5cbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG5cbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCcvYXNzZXRzL2xvYWRpbmctYmcuanBnJyk7XG59XG5cbi5oaWRkZW4ge1xuICBkaXNwbGF5OiBub25lO1xufVxuXG4uY29udGFpbmVyIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGhlaWdodDogMTAwdmg7XG59XG5cbi5hbmltIHtcbiAgd2lkdGg6IDUwcHg7XG4gIGhlaWdodDogNTBweDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBhbmltYXRpb246IGdsb2JhbCAyLjVzIGluZmluaXRlIGxpbmVhciBib3RoO1xufVxuXG4uZWwge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIGxlZnQ6IDA7XG4gIHRvcDogMDtcbiAgYW5pbWF0aW9uOiBsb2NhbCAycyBpbmZpbml0ZSBlYXNlLWluLW91dCBib3RoO1xufVxuXG4uZWw6YmVmb3JlIHtcbiAgY29udGVudDogJyc7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogMjUlO1xuICBoZWlnaHQ6IDI1JTtcbiAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmVlbjtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgYW5pbWF0aW9uOiBlbCAycyBpbmZpbml0ZSBlYXNlLWluLW91dCBib3RoO1xufVxuXG4uZWw6bnRoLWNoaWxkKDEpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMS4xcztcbn1cbi5lbDpudGgtY2hpbGQoMikge1xuICBhbmltYXRpb24tZGVsYXk6IC0xcztcbn1cbi5lbDpudGgtY2hpbGQoMykge1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjlzO1xufVxuLmVsOm50aC1jaGlsZCg0KSB7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuOHM7XG59XG4uZWw6bnRoLWNoaWxkKDUpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC43cztcbn1cbi5lbDpudGgtY2hpbGQoNikge1xuICBhbmltYXRpb24tZGVsYXk6IC0wLjZzO1xufVxuLmVsOm50aC1jaGlsZCg3KSB7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuNXM7XG59XG4uZWw6bnRoLWNoaWxkKDgpIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMC40cztcbn1cbi5lbDpudGgtY2hpbGQoMSk6YmVmb3JlIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMS4xcztcbn1cbi5lbDpudGgtY2hpbGQoMik6YmVmb3JlIHtcbiAgYW5pbWF0aW9uLWRlbGF5OiAtMXM7XG59XG4uZWw6bnRoLWNoaWxkKDMpOmJlZm9yZSB7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuOXM7XG59XG4uZWw6bnRoLWNoaWxkKDQpOmJlZm9yZSB7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuOHM7XG59XG4uZWw6bnRoLWNoaWxkKDUpOmJlZm9yZSB7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuN3M7XG59XG4uZWw6bnRoLWNoaWxkKDYpOmJlZm9yZSB7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuNnM7XG59XG4uZWw6bnRoLWNoaWxkKDcpOmJlZm9yZSB7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuNXM7XG59XG4uZWw6bnRoLWNoaWxkKDgpOmJlZm9yZSB7XG4gIGFuaW1hdGlvbi1kZWxheTogLTAuNHM7XG59XG5cbkBrZXlmcmFtZXMgZ2xvYmFsIHtcbiAgMTAwJSB7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbiAgfVxufVxuXG5Aa2V5ZnJhbWVzIGVsIHtcbiAgNTAlIHtcbiAgICB0cmFuc2Zvcm06IHNjYWxlKDAuNCk7XG4gIH1cbiAgMTAwJSxcbiAgMCUge1xuICAgIHRyYW5zZm9ybTogc2NhbGUoMSk7XG4gIH1cbn1cblxuQGtleWZyYW1lcyBsb2NhbCB7XG4gIDgwJSxcbiAgMTAwJSB7XG4gICAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbiAgfVxufVxuIl19 */"] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵsetClassMetadata"](AppComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"],
        args: [{
                selector: 'app-root',
                templateUrl: './app.component.html',
                styleUrls: ['./app.component.css'],
            }]
    }], function () { return []; }, null); })();


/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");




class AppModule {
}
AppModule.ɵfac = function AppModule_Factory(t) { return new (t || AppModule)(); };
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ providers: [], imports: [[
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]], imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"]] }); })();
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](AppModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"],
        args: [{
                declarations: [
                    _app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]
                ],
                imports: [
                    _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"]
                ],
                providers: [],
                bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]]
            }]
    }], null, null); })();


/***/ }),

/***/ "./src/app/view-port-3d/camera/camera-manager.ts":
/*!*******************************************************!*\
  !*** ./src/app/view-port-3d/camera/camera-manager.ts ***!
  \*******************************************************/
/*! exports provided: CameraManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CameraManager", function() { return CameraManager; });
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var three_examples_jsm_controls_OrbitControls__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three/examples/jsm/controls/OrbitControls */ "./node_modules/three/examples/jsm/controls/OrbitControls.js");
/* harmony import */ var _ecs_entity__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ecs/entity */ "./src/app/view-port-3d/ecs/entity.ts");
/* harmony import */ var _camera_fsm__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./camera.fsm */ "./src/app/view-port-3d/camera/camera.fsm.ts");
/* harmony import */ var _states__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./states */ "./src/app/view-port-3d/camera/states/index.ts");





class CameraManager extends _ecs_entity__WEBPACK_IMPORTED_MODULE_2__["ECSEntity"] {
    constructor(renderer, input, gui) {
        super();
        this.cameraTargetType = 'character';
        this.settings = {
            isOrbitControlsEnabled: false,
        };
        this.cameraState = {
            lookAt: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](),
            offset: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](),
        };
        this.input = input;
        this.camera = this.makePerspectiveCamera();
        this.renderer = renderer;
        this.fsm = new _camera_fsm__WEBPACK_IMPORTED_MODULE_3__["CameraFSM"](this);
        this.fsm.setState(_states__WEBPACK_IMPORTED_MODULE_4__["CAMERA_STATE"].THIRD_PERSON_VIEW);
        this.cameraGUI = gui.addFolder('Camera');
        this.cameraGUI
            .add(this.settings, 'isOrbitControlsEnabled')
            .onChange((value) => {
            var _a, _b;
            if (value) {
                this.orbitControls = new three_examples_jsm_controls_OrbitControls__WEBPACK_IMPORTED_MODULE_1__["OrbitControls"](this.camera, (_a = this.renderer) === null || _a === void 0 ? void 0 : _a.domElement);
            }
            else {
                (_b = this.orbitControls) === null || _b === void 0 ? void 0 : _b.dispose();
                this.orbitControls = undefined;
            }
        });
    }
    makePerspectiveCamera() {
        const aspect = window.innerWidth / window.innerHeight;
        const near = 1.0;
        const fov = 75;
        const far = 10000.0;
        const camera = new three__WEBPACK_IMPORTED_MODULE_0__["PerspectiveCamera"](fov, aspect, near, far);
        camera.position.set(0, 10, 15);
        camera.lookAt(0, 0, 0);
        return camera;
    }
    update(dt) {
        var _a, _b, _c;
        super.update(dt);
        if (this.settings.isOrbitControlsEnabled) {
            (_a = this.orbitControls) === null || _a === void 0 ? void 0 : _a.update();
            this.orbitControls.target = (_c = (_b = this.cameraTarget) === null || _b === void 0 ? void 0 : _b.wireframe) === null || _c === void 0 ? void 0 : _c.position;
        }
        this.fsm.update(dt);
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/camera/camera.controller.ts":
/*!**********************************************************!*\
  !*** ./src/app/view-port-3d/camera/camera.controller.ts ***!
  \**********************************************************/
/*! exports provided: CameraController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CameraController", function() { return CameraController; });
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");

class CameraController extends _ecs_component__WEBPACK_IMPORTED_MODULE_0__["ECSComponent"] {
    awake() { }
    update(deltaTime) {
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/camera/camera.fsm.ts":
/*!***************************************************!*\
  !*** ./src/app/view-port-3d/camera/camera.fsm.ts ***!
  \***************************************************/
/*! exports provided: CAMERA_OFFSET_AND_LOOK_AT, CameraFSM */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CAMERA_OFFSET_AND_LOOK_AT", function() { return CAMERA_OFFSET_AND_LOOK_AT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CameraFSM", function() { return CameraFSM; });
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _fsm_fsm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../fsm/fsm */ "./src/app/view-port-3d/fsm/fsm.ts");
/* harmony import */ var _states__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./states */ "./src/app/view-port-3d/camera/states/index.ts");
/* harmony import */ var _states_first_person_view_state__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./states/first-person-view.state */ "./src/app/view-port-3d/camera/states/first-person-view.state.ts");
/* harmony import */ var _states_third_person_view_state__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./states/third-person-view.state */ "./src/app/view-port-3d/camera/states/third-person-view.state.ts");





const CAMERA_OFFSET_AND_LOOK_AT = {
    [_states__WEBPACK_IMPORTED_MODULE_2__["CAMERA_STATE"].THIRD_PERSON_VIEW]: {
        offset: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](-5, 8, -15),
        lookAt: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0, 5, 20),
    },
    [_states__WEBPACK_IMPORTED_MODULE_2__["CAMERA_STATE"].FIRST_PERSON_VIEW]: {
        offset: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](-1, 5, 1.8),
        lookAt: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0, 5, 20),
    },
    [_states__WEBPACK_IMPORTED_MODULE_2__["CAMERA_STATE"].GLOBAL_VIEW]: {
        offset: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](20, 50, -150),
        lookAt: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0, 0, 0),
    },
};
class CameraFSM extends _fsm_fsm__WEBPACK_IMPORTED_MODULE_1__["FiniteStateMachine"] {
    constructor(proxy) {
        super();
        this.proxy = proxy;
        /**
         * add possible states
         */
        this.addState(_states__WEBPACK_IMPORTED_MODULE_2__["CAMERA_STATE"].THIRD_PERSON_VIEW, _states_third_person_view_state__WEBPACK_IMPORTED_MODULE_4__["ThirdPersonViewState"]);
        this.addState(_states__WEBPACK_IMPORTED_MODULE_2__["CAMERA_STATE"].FIRST_PERSON_VIEW, _states_first_person_view_state__WEBPACK_IMPORTED_MODULE_3__["FirstPersonViewState"]);
    }
    update(dt) {
        super.update(dt);
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/camera/states/first-person-view.state.ts":
/*!***********************************************************************!*\
  !*** ./src/app/view-port-3d/camera/states/first-person-view.state.ts ***!
  \***********************************************************************/
/*! exports provided: FirstPersonViewState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FirstPersonViewState", function() { return FirstPersonViewState; });
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! . */ "./src/app/view-port-3d/camera/states/index.ts");
/* harmony import */ var _fsm_finite_state__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../fsm/finite.state */ "./src/app/view-port-3d/fsm/finite.state.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utils */ "./src/app/view-port-3d/utils.ts");




// this.character.velocity.length()
const cameraOptions = {
    offset: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](-1, 5, 1.8),
    lookAt: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0, 5, 20),
};
class FirstPersonViewState extends _fsm_finite_state__WEBPACK_IMPORTED_MODULE_2__["FiniteState"] {
    constructor() {
        super(...arguments);
        this.name = ___WEBPACK_IMPORTED_MODULE_1__["CAMERA_STATE"].FIRST_PERSON_VIEW;
    }
    enter(prevState) {
        const fsm = this.fsm;
    }
    exit() { }
    update(dt) {
        const fsm = this.fsm;
        const proxy = fsm.proxy;
        if (!proxy.cameraTarget) {
            return;
        }
        if (proxy.settings.isOrbitControlsEnabled) {
            return;
        }
        this.updateCameraPosition(dt, {
            position: Object(_utils__WEBPACK_IMPORTED_MODULE_3__["toThreeVec3"])(proxy.cameraTarget.body.position),
            quaternion: Object(_utils__WEBPACK_IMPORTED_MODULE_3__["toThreeQuat"])(proxy.cameraTarget.body.quaternion),
        }, proxy.camera, proxy.cameraState.offset, proxy.cameraState.lookAt);
        if (fsm.proxy.input.keys.digit_1) {
            fsm.setState(___WEBPACK_IMPORTED_MODULE_1__["CAMERA_STATE"].THIRD_PERSON_VIEW);
        }
        else if (fsm.proxy.input.keys.digit_3) {
            fsm.setState(___WEBPACK_IMPORTED_MODULE_1__["CAMERA_STATE"].GLOBAL_VIEW);
        }
    }
    updateCameraPosition(timeElapsed, cameraTarget, camera, currentLookAt, currentOffset) {
        const targetPosition = cameraTarget.position;
        const targetQuaternion = cameraTarget.quaternion;
        const constantOffset = cameraOptions.offset.clone();
        const constantLookAt = cameraOptions.lookAt.clone();
        constantLookAt.applyQuaternion(targetQuaternion);
        constantLookAt.add(targetPosition);
        constantLookAt.y = cameraOptions.lookAt.y;
        constantOffset.applyQuaternion(targetQuaternion);
        constantOffset.add(targetPosition);
        constantOffset.y = cameraOptions.offset.y;
        const t = 1.0 - Math.pow(0.01, timeElapsed);
        currentLookAt.copy(constantOffset);
        currentOffset.copy(constantLookAt);
        currentLookAt.y = cameraOptions.offset.y;
        currentLookAt.y = cameraOptions.offset.y;
        camera.position.copy(currentLookAt);
        camera.lookAt(currentOffset);
    }
    calculateIdealOffset(position, quaternion) {
        const idealOffset = cameraOptions.offset.clone();
        idealOffset.applyQuaternion(quaternion);
        idealOffset.add(position);
        return idealOffset;
    }
    calculateIdealLookAt(position, quaternion) {
        const idealLookAt = cameraOptions.lookAt.clone();
        idealLookAt.applyQuaternion(quaternion);
        idealLookAt.add(position);
        return idealLookAt;
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/camera/states/index.ts":
/*!*****************************************************!*\
  !*** ./src/app/view-port-3d/camera/states/index.ts ***!
  \*****************************************************/
/*! exports provided: CAMERA_STATE */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CAMERA_STATE", function() { return CAMERA_STATE; });
var CAMERA_STATE;
(function (CAMERA_STATE) {
    CAMERA_STATE["THIRD_PERSON_VIEW"] = "THIRD_PERSON_VIEW";
    CAMERA_STATE["FIRST_PERSON_VIEW"] = "FIRST_PERSON_VIEW";
    CAMERA_STATE["GLOBAL_VIEW"] = "GLOBAL_VIEW";
})(CAMERA_STATE || (CAMERA_STATE = {}));


/***/ }),

/***/ "./src/app/view-port-3d/camera/states/third-person-view.state.ts":
/*!***********************************************************************!*\
  !*** ./src/app/view-port-3d/camera/states/third-person-view.state.ts ***!
  \***********************************************************************/
/*! exports provided: ThirdPersonViewState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ThirdPersonViewState", function() { return ThirdPersonViewState; });
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! . */ "./src/app/view-port-3d/camera/states/index.ts");
/* harmony import */ var _fsm_finite_state__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../fsm/finite.state */ "./src/app/view-port-3d/fsm/finite.state.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../utils */ "./src/app/view-port-3d/utils.ts");




const cameraOptions = {
    offset: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](-2, 2, -6),
    lookAt: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0, 5, 20),
};
const cameraCarOptions = {
    offset: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0, 4, 6),
    lookAt: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0, 4, -10),
};
class ThirdPersonViewState extends _fsm_finite_state__WEBPACK_IMPORTED_MODULE_2__["FiniteState"] {
    constructor() {
        super(...arguments);
        this.name = ___WEBPACK_IMPORTED_MODULE_1__["CAMERA_STATE"].THIRD_PERSON_VIEW;
    }
    enter(prevState) {
        const fsm = this.fsm;
    }
    exit() { }
    update(dt) {
        const fsm = this.fsm;
        const proxy = fsm.proxy;
        if (!proxy.cameraTarget) {
            return;
        }
        if (proxy.settings.isOrbitControlsEnabled) {
            return;
        }
        this.updateCameraPosition(dt, {
            position: Object(_utils__WEBPACK_IMPORTED_MODULE_3__["toThreeVec3"])(proxy.cameraTarget.body.position),
            quaternion: Object(_utils__WEBPACK_IMPORTED_MODULE_3__["toThreeQuat"])(proxy.cameraTarget.body.quaternion),
        }, proxy.camera, proxy.cameraState.offset, proxy.cameraState.lookAt);
        if (fsm.proxy.input.keys.digit_2) {
            fsm.setState(___WEBPACK_IMPORTED_MODULE_1__["CAMERA_STATE"].FIRST_PERSON_VIEW);
        }
        else if (fsm.proxy.input.keys.digit_3) {
            fsm.setState(___WEBPACK_IMPORTED_MODULE_1__["CAMERA_STATE"].GLOBAL_VIEW);
        }
    }
    followCar(timeElapsed, cameraTarget, camera, currentLookAt, currentOffset) {
        const targetPosition = cameraTarget.position.clone();
        const targetQuaternion = cameraTarget.quaternion.clone();
        const constantOffset = cameraCarOptions.offset.clone();
        const constantLookAt = cameraCarOptions.lookAt.clone();
        const defaultRotation = new three__WEBPACK_IMPORTED_MODULE_0__["Quaternion"]().setFromAxisAngle(new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0, 1, 0), Math.PI / 2);
        constantLookAt.applyQuaternion(defaultRotation);
        constantLookAt.applyQuaternion(targetQuaternion);
        constantOffset.applyQuaternion(defaultRotation);
        constantOffset.applyQuaternion(targetQuaternion);
        constantOffset.add(targetPosition);
        constantLookAt.add(targetPosition);
        const t = 1.0 - Math.pow(0.01, timeElapsed);
        camera.position.lerp(constantOffset, t);
        camera.lookAt(constantLookAt);
    }
    followCharacter(timeElapsed, cameraTarget, camera, currentLookAt, currentOffset) {
        const targetPosition = cameraTarget.position.clone();
        const targetQuaternion = cameraTarget.quaternion.clone();
        const constantOffset = cameraOptions.offset.clone();
        const constantLookAt = cameraOptions.lookAt.clone();
        constantLookAt.applyQuaternion(targetQuaternion);
        constantLookAt.add(targetPosition);
        constantOffset.applyQuaternion(targetQuaternion);
        constantOffset.add(targetPosition);
        const t = 1.0 - Math.pow(0.001, timeElapsed);
        // currentLookAt.lerp(constantOffset, t)
        // currentOffset.lerp(constantLookAt, t)
        camera.position.copy(constantOffset);
        camera.lookAt(constantLookAt);
    }
    updateCameraPosition(timeElapsed, cameraTarget, camera, currentLookAt, currentOffset) {
        const fsm = this.fsm;
        if (fsm.proxy.cameraTargetType === 'car') {
            this.followCar(timeElapsed, cameraTarget, camera, currentLookAt, currentOffset);
        }
        else {
            this.followCharacter(timeElapsed, cameraTarget, camera, currentLookAt, currentOffset);
        }
    }
    calculateIdealLookAt(position, quaternion) {
        const idealLookAt = cameraOptions.lookAt.clone();
        idealLookAt.applyQuaternion(quaternion);
        idealLookAt.add(position);
        return idealLookAt;
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/car/car.controller.ts":
/*!****************************************************!*\
  !*** ./src/app/view-port-3d/car/car.controller.ts ***!
  \****************************************************/
/*! exports provided: CarController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarController", function() { return CarController; });
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var three_examples_jsm_loaders_GLTFLoader_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three/examples/jsm/loaders/GLTFLoader.js */ "./node_modules/three/examples/jsm/loaders/GLTFLoader.js");
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils */ "./src/app/view-port-3d/utils.ts");
/* harmony import */ var _car_physics__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./car.physics */ "./src/app/view-port-3d/car/car.physics.ts");

// import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'




class CarController extends _ecs_component__WEBPACK_IMPORTED_MODULE_2__["ECSComponent"] {
    constructor() {
        super(...arguments);
        this.isDisabled = true;
        this.carPhysics = null;
        this.wheels = [
            { meshes: [] },
            { meshes: [] },
            { meshes: [] },
            { meshes: [] },
        ];
        this.defaultWireframeMaterial = new three__WEBPACK_IMPORTED_MODULE_0__["MeshBasicMaterial"]({
            color: new three__WEBPACK_IMPORTED_MODULE_0__["Color"](0xff0000),
        });
    }
    awake() {
        this.loadModel();
        this.carPhysics = this.entity.getComponent(_car_physics__WEBPACK_IMPORTED_MODULE_4__["CarPhysics"]);
        // Apply force on keydown
        document.addEventListener('keydown', (event) => this.onKeyDown(event));
        // Reset force on keyup
        document.addEventListener('keyup', (event) => this.onKeyUp(event));
        this.disable();
    }
    update(dt) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j;
        (_a = this.carModel) === null || _a === void 0 ? void 0 : _a.position.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_3__["toThreeVec3"])((_d = (_c = (_b = this.carPhysics) === null || _b === void 0 ? void 0 : _b.car) === null || _c === void 0 ? void 0 : _c.chassis) === null || _d === void 0 ? void 0 : _d.body.position));
        ((_e = this.carModel) === null || _e === void 0 ? void 0 : _e.position) && (this.carModel.position.y = 0);
        const q = Object(_utils__WEBPACK_IMPORTED_MODULE_3__["toThreeQuat"])((_h = (_g = (_f = this.carPhysics) === null || _f === void 0 ? void 0 : _f.car) === null || _g === void 0 ? void 0 : _g.chassis) === null || _h === void 0 ? void 0 : _h.body.quaternion);
        q.multiplyQuaternions(q, new three__WEBPACK_IMPORTED_MODULE_0__["Quaternion"]().setFromAxisAngle(new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0, 1, 0), Math.PI * -0.5));
        (_j = this.carModel) === null || _j === void 0 ? void 0 : _j.quaternion.copy(q);
    }
    enable() {
        this.carPhysics.shouldBrake = false;
    }
    disable() {
        var _a;
        this.carPhysics.shouldBrake = true;
        (_a = this.carPhysics) === null || _a === void 0 ? void 0 : _a.break();
    }
    onKeyDown(event) {
        var _a, _b, _c, _d, _e, _f;
        if (this.carPhysics.shouldBrake) {
            return;
        }
        (_a = this.carPhysics) === null || _a === void 0 ? void 0 : _a.resetBreak();
        switch (event.key) {
            case 'w':
            case 'ArrowUp':
                (_b = this.carPhysics) === null || _b === void 0 ? void 0 : _b.moveForward();
                break;
            case 's':
            case 'ArrowDown':
                (_c = this.carPhysics) === null || _c === void 0 ? void 0 : _c.moveBackward();
                break;
            case 'a':
            case 'ArrowLeft':
                (_d = this.carPhysics) === null || _d === void 0 ? void 0 : _d.turnLeft();
                break;
            case 'd':
            case 'ArrowRight':
                (_e = this.carPhysics) === null || _e === void 0 ? void 0 : _e.turnRight();
                break;
            case 'b':
                (_f = this.carPhysics) === null || _f === void 0 ? void 0 : _f.break();
                break;
        }
    }
    onKeyUp(event) {
        var _a, _b, _c;
        if (this.carPhysics.shouldBrake) {
            return;
        }
        (_a = this.carPhysics) === null || _a === void 0 ? void 0 : _a.resetBreak();
        switch (event.key) {
            case 'w':
            case 'ArrowUp':
            case 's':
            case 'ArrowDown':
                (_b = this.carPhysics) === null || _b === void 0 ? void 0 : _b.slowdown();
                break;
            case 'a':
            case 'ArrowLeft':
            case 'd':
            case 'ArrowRight':
                (_c = this.carPhysics) === null || _c === void 0 ? void 0 : _c.resetWheelsRotation();
                break;
        }
    }
    loadModel() {
        const loader = new three_examples_jsm_loaders_GLTFLoader_js__WEBPACK_IMPORTED_MODULE_1__["GLTFLoader"]();
        loader.setPath('assets/models/city/');
        loader.load('car.glb', (gltf) => {
            var _a, _b;
            const car = gltf.scene;
            car.traverse((ch) => {
                if (ch instanceof three__WEBPACK_IMPORTED_MODULE_0__["Mesh"]) {
                    // circle001 front left
                    // circle004 front right
                    // circle006 back left
                    // circle012 back right
                    const name = ch.name.toLowerCase();
                    if (name.includes('circle')) {
                        if (name.includes('circle001')) {
                            this.wheels[0].meshes.push(ch);
                        }
                        if (name.includes('circle004')) {
                            this.wheels[1].meshes.push(ch);
                        }
                        if (name.includes('circle006')) {
                            this.wheels[2].meshes.push(ch);
                        }
                        if (name.includes('circle0012')) {
                            this.wheels[3].meshes.push(ch);
                        }
                    }
                    // debugger
                    // ch.material.wireframe = true
                }
            });
            const factor = 1;
            car.scale.set(factor, factor, factor);
            (_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.scene) === null || _b === void 0 ? void 0 : _b.add(car);
            this.carModel = car;
        });
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/car/car.gui.ts":
/*!*********************************************!*\
  !*** ./src/app/view-port-3d/car/car.gui.ts ***!
  \*********************************************/
/*! exports provided: addCarGUI, addWheelsGUI */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addCarGUI", function() { return addCarGUI; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addWheelsGUI", function() { return addWheelsGUI; });
/* harmony import */ var _car_settings__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./car.settings */ "./src/app/view-port-3d/car/car.settings.ts");

function addCarGUI(options) {
    const { gui, carOptions, recreate } = options;
    const folder = gui.addFolder('Chassis');
    folder
        .add(carOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_WIDTH"])
        .step(0.001)
        .min(0)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_WIDTH"])
        .onFinishChange(() => recreate());
    folder
        .add(carOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_HEIGHT"])
        .step(0.001)
        .min(0)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_HEIGHT"])
        .onFinishChange(() => recreate());
    folder
        .add(carOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_DEPTH"])
        .step(0.001)
        .min(0)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_DEPTH"])
        .onFinishChange(() => recreate());
    folder
        .add(carOptions[_car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_OFFSET"]], 'z')
        .step(0.001)
        .min(-5)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_OFFSET"] + ' x')
        .onFinishChange(() => recreate());
    folder
        .add(carOptions[_car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_OFFSET"]], 'y')
        .step(0.001)
        .min(-5)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_OFFSET"] + ' y')
        .onFinishChange(() => recreate());
    folder
        .add(carOptions[_car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_OFFSET"]], 'x')
        .step(0.001)
        .min(-5)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_OFFSET"] + ' z')
        .onFinishChange(() => recreate());
    folder
        .add(carOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_MASS"])
        .step(0.001)
        .min(0)
        .max(150)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["CHASSIS_MASS"])
        .onFinishChange(() => recreate());
}
function addWheelsGUI(options) {
    const { gui, wheelOptions, recreate } = options;
    const folder = gui.addFolder('Wheels');
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_FRONT_OFFSET_DEPTH"])
        .step(0.001)
        .min(0)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_FRONT_OFFSET_DEPTH"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_BACK_OFFSET_DEPTH"])
        .step(0.001)
        .min(0)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_BACK_OFFSET_DEPTH"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_OFFSET_WIDTH"])
        .step(0.001)
        .min(0)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_OFFSET_WIDTH"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_OFFSET_HEIGHT"])
        .step(0.001)
        .min(0)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_OFFSET_HEIGHT"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_RADIUS"])
        .step(0.001)
        .min(0)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_RADIUS"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_DEPTH"])
        .step(0.001)
        .min(0)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_DEPTH"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_MASS"])
        .step(0.001)
        .min(0)
        .max(5)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["WHEEL_MASS"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_SLIDING_ROTATIONAL_SPEED"])
        .step(1)
        .min(-100)
        .max(100)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["CUSTOM_SLIDING_ROTATIONAL_SPEED"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["DAMPING_COMPRESSION"])
        .step(0.1)
        .min(0)
        .max(10)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["DAMPING_COMPRESSION"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["DAMPING_RELAXATION"])
        .step(0.1)
        .min(0)
        .max(10)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["DAMPING_RELAXATION"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["FRICTION_SLIP"])
        .step(0.1)
        .min(0)
        .max(10)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["FRICTION_SLIP"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["MAX_SUSPENSION_FORCE"])
        .step(100)
        .min(0)
        .max(100000)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["MAX_SUSPENSION_FORCE"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["MAX_SUSPENSION_TRAVEL"])
        .step(0.001)
        .min(0)
        .max(1)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["MAX_SUSPENSION_TRAVEL"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["RADIUS"])
        .step(0.001)
        .min(0)
        .max(1)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["RADIUS"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["ROLL_INFLUENCE"])
        .step(0.001)
        .min(0)
        .max(1)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["ROLL_INFLUENCE"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["SUSPENSION_REST_LENGTH"])
        .step(0.001)
        .min(0)
        .max(1)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["SUSPENSION_REST_LENGTH"])
        .onFinishChange(() => recreate());
    folder
        .add(wheelOptions, _car_settings__WEBPACK_IMPORTED_MODULE_0__["SUSPENSION_STIFFNESS"])
        .step(0.1)
        .min(0)
        .max(100)
        .name(_car_settings__WEBPACK_IMPORTED_MODULE_0__["SUSPENSION_STIFFNESS"])
        .onFinishChange(() => recreate());
}


/***/ }),

/***/ "./src/app/view-port-3d/car/car.physics.ts":
/*!*************************************************!*\
  !*** ./src/app/view-port-3d/car/car.physics.ts ***!
  \*************************************************/
/*! exports provided: CarPhysics */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CarPhysics", function() { return CarPhysics; });
/* harmony import */ var cannon_es__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! cannon-es */ "./node_modules/cannon-es/dist/cannon-es.js");
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _physics_conversion_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../physics/conversion-utils */ "./src/app/view-port-3d/physics/conversion-utils.ts");
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");
/* harmony import */ var _car_settings__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./car.settings */ "./src/app/view-port-3d/car/car.settings.ts");
/* harmony import */ var _car_gui__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./car.gui */ "./src/app/view-port-3d/car/car.gui.ts");






class CarPhysics extends _ecs_component__WEBPACK_IMPORTED_MODULE_3__["ECSComponent"] {
    constructor() {
        super(...arguments);
        this.callbacks = {
            recreate: () => {
                var _a, _b;
                this.removeCar();
                this.addCar();
                (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.chassis) === null || _b === void 0 ? void 0 : _b.body.wakeUp();
            },
        };
        this.physics = null;
        this.maxSteerVal = Math.PI * 0.17;
        this.maxForce = 500;
        this.brakeForce = 10;
        this.shouldBrake = false;
    }
    awake() {
        var _a, _b, _c, _d;
        this.physics = (_a = this.options) === null || _a === void 0 ? void 0 : _a.physics;
        (_b = this.physics) === null || _b === void 0 ? void 0 : _b.world.addEventListener('postStep', () => this.handlePostStep());
        Object(_car_gui__WEBPACK_IMPORTED_MODULE_5__["addCarGUI"])({
            gui: (_c = this.options) === null || _c === void 0 ? void 0 : _c.gui,
            carOptions: _car_settings__WEBPACK_IMPORTED_MODULE_4__["CAR_OPTIONS"],
            recreate: this.callbacks.recreate,
        });
        Object(_car_gui__WEBPACK_IMPORTED_MODULE_5__["addWheelsGUI"])({
            gui: (_d = this.options) === null || _d === void 0 ? void 0 : _d.gui,
            wheelOptions: _car_settings__WEBPACK_IMPORTED_MODULE_4__["WHEEL_OPTIONS"],
            recreate: this.callbacks.recreate,
        });
    }
    update(deltaTime) {
        // KEEP it for later
        // if (this.car?.chassis?.body) {
        //   const target = this.car?.chassis?.wireframe?.position.clone()
        //   const start = target!
        //   const direction = new Vector3(0, 0, 1)
        //   direction
        //     .applyQuaternion(
        //       toThreeQuat(this.car?.chassis?.body.quaternion),
        //     )
        //     .normalize()
        //     .multiplyScalar(2)
        //   start.add(direction!)
        //   // console.log(start)
        //   this.cube!.position.copy(start!)
        //   this.cube!.quaternion.copy(
        //     toThreeQuat(this.car?.chassis?.body.quaternion),
        //   )
        // }
    }
    /**
     * @description
     * adds vehicle to cannonjs world and wireframe to threejs scene
     */
    addCar() {
        var _a, _b, _c, _d, _e, _f, _g, _h;
        this.car = this.makeCar({
            carOptions: _car_settings__WEBPACK_IMPORTED_MODULE_4__["CAR_OPTIONS"],
            wheelOptions: _car_settings__WEBPACK_IMPORTED_MODULE_4__["WHEEL_OPTIONS"],
            wheelMaterial: this.physics.wheelMaterial,
        });
        // Add chassis
        (_a = this.physics) === null || _a === void 0 ? void 0 : _a.world.addBody((_b = this.car.chassis) === null || _b === void 0 ? void 0 : _b.body);
        (_d = (_c = this.options) === null || _c === void 0 ? void 0 : _c.scene) === null || _d === void 0 ? void 0 : _d.add((_e = this.car.chassis) === null || _e === void 0 ? void 0 : _e.wireframe);
        (_f = this.physics) === null || _f === void 0 ? void 0 : _f.collidable.push(this.car.chassis);
        // Add wheels
        this.car.wheels.forEach((wheel) => {
            var _a, _b, _c, _d;
            (_a = this.physics) === null || _a === void 0 ? void 0 : _a.world.addBody(wheel.body);
            (_c = (_b = this.options) === null || _b === void 0 ? void 0 : _b.scene) === null || _c === void 0 ? void 0 : _c.add(wheel.wireframe);
            (_d = this.physics) === null || _d === void 0 ? void 0 : _d.collidable.push(wheel);
        });
        // Add vehicle
        (_g = this.car.vehicle) === null || _g === void 0 ? void 0 : _g.addToWorld((_h = this.physics) === null || _h === void 0 ? void 0 : _h.world);
    }
    /**
     * @description
     * Removes all parts of vehicle from scene and world
     */
    removeCar() {
        var _a, _b, _c, _d, _e, _f;
        if (!this.car || !this.car.chassis) {
            // remove chassis from colidables
            this.physics.collidable = this.physics.collidable.filter((collidable) => { var _a; return collidable !== ((_a = this.car) === null || _a === void 0 ? void 0 : _a.chassis); });
            return;
        }
        // remove chassis
        (_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.scene) === null || _b === void 0 ? void 0 : _b.remove((_c = this.car.chassis) === null || _c === void 0 ? void 0 : _c.wireframe);
        (_d = this.physics) === null || _d === void 0 ? void 0 : _d.world.removeBody((_e = this.car.chassis) === null || _e === void 0 ? void 0 : _e.body);
        // remove wheels
        (_f = this.car.wheels) === null || _f === void 0 ? void 0 : _f.forEach((wheel) => {
            var _a, _b, _c;
            (_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.scene) === null || _b === void 0 ? void 0 : _b.remove(wheel.wireframe);
            (_c = this.physics) === null || _c === void 0 ? void 0 : _c.world.removeBody(wheel.body);
            // TODO: remove wheel from collidable or static or objects
        });
        // reset object
        this.car.wheels = [];
        this.car.vehicle = null;
        this.car.chassis = null;
    }
    /**
     * @description
     * sync wheels position with chassis transformation
     */
    handlePostStep() {
        var _a, _b;
        // const carController = this.entity?.getComponent(CarController)!
        (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.vehicle) === null || _b === void 0 ? void 0 : _b.wheelInfos.forEach((wheel, i) => {
            var _a, _b, _c, _d, _e, _f, _g;
            // const wheelParts = carController.wheels[i].meshes
            (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.vehicle) === null || _b === void 0 ? void 0 : _b.updateWheelTransform(i);
            const wheelInfo = (_e = (_d = (_c = this.car) === null || _c === void 0 ? void 0 : _c.vehicle) === null || _d === void 0 ? void 0 : _d.wheelInfos) === null || _e === void 0 ? void 0 : _e[i];
            const physicsObject = (_g = (_f = this.car) === null || _f === void 0 ? void 0 : _f.wheels) === null || _g === void 0 ? void 0 : _g[i];
            const { body } = physicsObject;
            const transform = wheelInfo.worldTransform;
            body.position.copy(transform.position);
            body.quaternion.copy(transform.quaternion);
            // wheelParts.forEach(parts => {
            //   parts.position.copy(toThreeVec3(transform.position))
            //   parts.quaternion.copy(toThreeQuat(transform.quaternion))
            // })
        });
    }
    /**
     * @description
     * Creates a CannonJs RaycasterVehicle and wireframe
     */
    makeCar(options) {
        const { carOptions, wheelOptions, wheelMaterial } = options;
        const car = {
            wheels: [],
            chassis: null,
            vehicle: null,
        };
        /**
         * Chassis
         */
        const chassisShape = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Box"](new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](carOptions.chassisDepth, carOptions.chassisHeight, carOptions.chassisWidth));
        const chassisBody = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Body"]({
            mass: carOptions.chassisMass,
        });
        chassisBody.position.set(-78.89380190195465, 0.8, -3.5311534929445020);
        chassisBody.quaternion.setFromAxisAngle(new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](0, 1, 0), Math.PI * 0.5);
        chassisBody.angularVelocity.set(0, 0, 0);
        chassisBody.addShape(chassisShape, carOptions.chassisOffset);
        const chassisMesh = Object(_physics_conversion_utils__WEBPACK_IMPORTED_MODULE_2__["bodyToMesh"])(chassisBody, new three__WEBPACK_IMPORTED_MODULE_1__["MeshPhongMaterial"]({
            color: new three__WEBPACK_IMPORTED_MODULE_1__["Color"](0x00ffff),
        }));
        chassisMesh.visible = false;
        car.chassis = {
            body: chassisBody,
            wireframe: chassisMesh,
            isWireframeVisible: true,
        };
        /**
         * Vehicle
         */
        car.vehicle = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["RaycastVehicle"]({
            chassisBody: chassisBody,
        });
        /**
         * Wheels
         */
        const wheelParameters = {
            axleLocal: new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](0, 0, 1),
            chassisConnectionPointLocal: wheelOptions.chassisConnectionPointLocal,
            customSlidingRotationalSpeed: wheelOptions.customSlidingRotationalSpeed,
            // height: wheelOptions.wheelRadius,
            dampingCompression: wheelOptions.dampingCompression,
            dampingRelaxation: wheelOptions.dampingRelaxation,
            directionLocal: new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](0, -1, 0),
            frictionSlip: wheelOptions.frictionSlip,
            maxSuspensionForce: wheelOptions.maxSuspensionForce,
            maxSuspensionTravel: wheelOptions.maxSuspensionTravel,
            radius: wheelOptions.wheelRadius,
            rollInfluence: wheelOptions.rollInfluence,
            suspensionRestLength: wheelOptions.suspensionRestLength,
            suspensionStiffness: wheelOptions.suspensionStiffness,
            useCustomSlidingRotationalSpeed: true,
        };
        // Front left
        wheelParameters.chassisConnectionPointLocal.set(-wheelOptions.wheelFrontOffsetDepth, wheelOptions.wheelOffsetHeight, wheelOptions.wheelOffsetWidth);
        car.vehicle.addWheel(wheelParameters);
        // Front right
        wheelParameters.chassisConnectionPointLocal.set(-wheelOptions.wheelFrontOffsetDepth, wheelOptions.wheelOffsetHeight, -wheelOptions.wheelOffsetWidth);
        car.vehicle.addWheel(wheelParameters);
        // Back left
        wheelParameters.chassisConnectionPointLocal.set(wheelOptions.wheelBackOffsetDepth, wheelOptions.wheelOffsetHeight, wheelOptions.wheelOffsetWidth);
        car.vehicle.addWheel(wheelParameters);
        // Back right
        wheelParameters.chassisConnectionPointLocal.set(wheelOptions.wheelBackOffsetDepth, wheelOptions.wheelOffsetHeight, -wheelOptions.wheelOffsetWidth);
        car.vehicle.addWheel(wheelParameters);
        // make wheel bodies and wireframes
        for (let i = 0; i < car.vehicle.wheelInfos.length; i++) {
            const _wheelInfos = car.vehicle.wheelInfos[i];
            const shape = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Cylinder"](_wheelInfos.radius, _wheelInfos.radius, wheelOptions.wheelRadius, 20);
            const body = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Body"]({
                mass: wheelOptions.wheelMass,
                material: wheelMaterial,
            });
            const quaternion = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Quaternion"]();
            quaternion.setFromAxisAngle(new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](1, 0, 0), Math.PI / 2);
            body.type = cannon_es__WEBPACK_IMPORTED_MODULE_0__["Body"].KINEMATIC;
            body.collisionFilterGroup = 0; // turn of collisions
            body.addShape(shape, new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](), quaternion);
            // 0 front left
            // 1 front right
            // 2 back left
            // 3 back right
            const color = i === 2 ? 0xff0000 : 0xff00ff;
            const mat = new three__WEBPACK_IMPORTED_MODULE_1__["MeshPhongMaterial"]({
                color: new three__WEBPACK_IMPORTED_MODULE_1__["Color"](color),
            });
            const wheelMesh = Object(_physics_conversion_utils__WEBPACK_IMPORTED_MODULE_2__["bodyToMesh"])(body, mat);
            wheelMesh.visible = false;
            car.wheels.push({
                body,
                wireframe: wheelMesh,
                isWireframeVisible: true,
            });
        }
        return car;
    }
    /**
     * Apply force to car
     */
    moveForward() {
        var _a, _b, _c, _d;
        (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.vehicle) === null || _b === void 0 ? void 0 : _b.applyEngineForce(-this.maxForce, 2);
        (_d = (_c = this.car) === null || _c === void 0 ? void 0 : _c.vehicle) === null || _d === void 0 ? void 0 : _d.applyEngineForce(-this.maxForce, 3);
    }
    moveBackward() {
        var _a, _b, _c, _d;
        (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.vehicle) === null || _b === void 0 ? void 0 : _b.applyEngineForce(this.maxForce, 2);
        (_d = (_c = this.car) === null || _c === void 0 ? void 0 : _c.vehicle) === null || _d === void 0 ? void 0 : _d.applyEngineForce(this.maxForce, 3);
    }
    turnLeft() {
        var _a, _b, _c, _d;
        (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.vehicle) === null || _b === void 0 ? void 0 : _b.setSteeringValue(this.maxSteerVal, 0);
        (_d = (_c = this.car) === null || _c === void 0 ? void 0 : _c.vehicle) === null || _d === void 0 ? void 0 : _d.setSteeringValue(this.maxSteerVal, 1);
    }
    turnRight() {
        var _a, _b, _c, _d;
        (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.vehicle) === null || _b === void 0 ? void 0 : _b.setSteeringValue(-this.maxSteerVal, 0);
        (_d = (_c = this.car) === null || _c === void 0 ? void 0 : _c.vehicle) === null || _d === void 0 ? void 0 : _d.setSteeringValue(-this.maxSteerVal, 1);
    }
    resetBreak() {
        var _a, _b, _c, _d, _e, _f, _g, _h;
        (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.vehicle) === null || _b === void 0 ? void 0 : _b.setBrake(0, 0);
        (_d = (_c = this.car) === null || _c === void 0 ? void 0 : _c.vehicle) === null || _d === void 0 ? void 0 : _d.setBrake(0, 1);
        (_f = (_e = this.car) === null || _e === void 0 ? void 0 : _e.vehicle) === null || _f === void 0 ? void 0 : _f.setBrake(0, 2);
        (_h = (_g = this.car) === null || _g === void 0 ? void 0 : _g.vehicle) === null || _h === void 0 ? void 0 : _h.setBrake(0, 3);
    }
    break() {
        var _a, _b, _c, _d, _e, _f, _g, _h;
        (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.vehicle) === null || _b === void 0 ? void 0 : _b.setBrake(this.brakeForce, 0);
        (_d = (_c = this.car) === null || _c === void 0 ? void 0 : _c.vehicle) === null || _d === void 0 ? void 0 : _d.setBrake(this.brakeForce, 1);
        (_f = (_e = this.car) === null || _e === void 0 ? void 0 : _e.vehicle) === null || _f === void 0 ? void 0 : _f.setBrake(this.brakeForce, 2);
        (_h = (_g = this.car) === null || _g === void 0 ? void 0 : _g.vehicle) === null || _h === void 0 ? void 0 : _h.setBrake(this.brakeForce, 3);
    }
    slowdown() {
        var _a, _b, _c, _d;
        (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.vehicle) === null || _b === void 0 ? void 0 : _b.applyEngineForce(0, 2);
        (_d = (_c = this.car) === null || _c === void 0 ? void 0 : _c.vehicle) === null || _d === void 0 ? void 0 : _d.applyEngineForce(0, 3);
    }
    resetWheelsRotation() {
        var _a, _b, _c, _d;
        (_b = (_a = this.car) === null || _a === void 0 ? void 0 : _a.vehicle) === null || _b === void 0 ? void 0 : _b.setSteeringValue(0, 0);
        (_d = (_c = this.car) === null || _c === void 0 ? void 0 : _c.vehicle) === null || _d === void 0 ? void 0 : _d.setSteeringValue(0, 1);
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/car/car.settings.ts":
/*!**************************************************!*\
  !*** ./src/app/view-port-3d/car/car.settings.ts ***!
  \**************************************************/
/*! exports provided: CHASSIS_WIDTH, CHASSIS_HEIGHT, CHASSIS_DEPTH, CHASSIS_OFFSET, CHASSIS_MASS, WHEEL_FRONT_OFFSET_DEPTH, WHEEL_BACK_OFFSET_DEPTH, WHEEL_OFFSET_WIDTH, WHEEL_OFFSET_HEIGHT, WHEEL_RADIUS, WHEEL_DEPTH, WHEEL_MASS, AXLE_LOCAL, CHASSIS_CONNECTION_POINT_LOCAL, CUSTOM_SLIDING_ROTATIONAL_SPEED, DAMPING_COMPRESSION, DAMPING_RELAXATION, DIRECTION_LOCAL, FRICTION_SLIP, MAX_SUSPENSION_FORCE, MAX_SUSPENSION_TRAVEL, RADIUS, ROLL_INFLUENCE, SUSPENSION_REST_LENGTH, SUSPENSION_STIFFNESS, USE_CUSTOM_SLIDING_ROTATIONAL_SPEED, CAR_OPTIONS, WHEEL_OPTIONS, WHEEL_INDEXES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHASSIS_WIDTH", function() { return CHASSIS_WIDTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHASSIS_HEIGHT", function() { return CHASSIS_HEIGHT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHASSIS_DEPTH", function() { return CHASSIS_DEPTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHASSIS_OFFSET", function() { return CHASSIS_OFFSET; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHASSIS_MASS", function() { return CHASSIS_MASS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WHEEL_FRONT_OFFSET_DEPTH", function() { return WHEEL_FRONT_OFFSET_DEPTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WHEEL_BACK_OFFSET_DEPTH", function() { return WHEEL_BACK_OFFSET_DEPTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WHEEL_OFFSET_WIDTH", function() { return WHEEL_OFFSET_WIDTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WHEEL_OFFSET_HEIGHT", function() { return WHEEL_OFFSET_HEIGHT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WHEEL_RADIUS", function() { return WHEEL_RADIUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WHEEL_DEPTH", function() { return WHEEL_DEPTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WHEEL_MASS", function() { return WHEEL_MASS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AXLE_LOCAL", function() { return AXLE_LOCAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CHASSIS_CONNECTION_POINT_LOCAL", function() { return CHASSIS_CONNECTION_POINT_LOCAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CUSTOM_SLIDING_ROTATIONAL_SPEED", function() { return CUSTOM_SLIDING_ROTATIONAL_SPEED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DAMPING_COMPRESSION", function() { return DAMPING_COMPRESSION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DAMPING_RELAXATION", function() { return DAMPING_RELAXATION; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DIRECTION_LOCAL", function() { return DIRECTION_LOCAL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FRICTION_SLIP", function() { return FRICTION_SLIP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MAX_SUSPENSION_FORCE", function() { return MAX_SUSPENSION_FORCE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MAX_SUSPENSION_TRAVEL", function() { return MAX_SUSPENSION_TRAVEL; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RADIUS", function() { return RADIUS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ROLL_INFLUENCE", function() { return ROLL_INFLUENCE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SUSPENSION_REST_LENGTH", function() { return SUSPENSION_REST_LENGTH; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SUSPENSION_STIFFNESS", function() { return SUSPENSION_STIFFNESS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "USE_CUSTOM_SLIDING_ROTATIONAL_SPEED", function() { return USE_CUSTOM_SLIDING_ROTATIONAL_SPEED; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CAR_OPTIONS", function() { return CAR_OPTIONS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WHEEL_OPTIONS", function() { return WHEEL_OPTIONS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WHEEL_INDEXES", function() { return WHEEL_INDEXES; });
/* harmony import */ var cannon_es__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! cannon-es */ "./node_modules/cannon-es/dist/cannon-es.js");

const CHASSIS_WIDTH = 'chassisWidth';
const CHASSIS_HEIGHT = 'chassisHeight';
const CHASSIS_DEPTH = 'chassisDepth';
const CHASSIS_OFFSET = 'chassisOffset';
const CHASSIS_MASS = 'chassisMass';
const WHEEL_FRONT_OFFSET_DEPTH = 'wheelFrontOffsetDepth';
const WHEEL_BACK_OFFSET_DEPTH = 'wheelBackOffsetDepth';
const WHEEL_OFFSET_WIDTH = 'wheelOffsetWidth';
const WHEEL_OFFSET_HEIGHT = 'wheelOffsetHeight';
const WHEEL_RADIUS = 'wheelRadius';
const WHEEL_DEPTH = 'wheelDepth';
const WHEEL_MASS = 'wheelMass';
const AXLE_LOCAL = 'axleLocal';
const CHASSIS_CONNECTION_POINT_LOCAL = 'chassisConnectionPointLocal';
const CUSTOM_SLIDING_ROTATIONAL_SPEED = 'customSlidingRotationalSpeed';
const DAMPING_COMPRESSION = 'dampingCompression';
const DAMPING_RELAXATION = 'dampingRelaxation';
const DIRECTION_LOCAL = 'directionLocal';
const FRICTION_SLIP = 'frictionSlip';
const MAX_SUSPENSION_FORCE = 'maxSuspensionForce';
const MAX_SUSPENSION_TRAVEL = 'maxSuspensionTravel';
const RADIUS = 'radius';
const ROLL_INFLUENCE = 'rollInfluence';
const SUSPENSION_REST_LENGTH = 'suspensionRestLength';
const SUSPENSION_STIFFNESS = 'suspensionStiffness';
const USE_CUSTOM_SLIDING_ROTATIONAL_SPEED = 'useCustomSlidingRotationalSpeed';
const CAR_OPTIONS = {
    [CHASSIS_WIDTH]: 0.6,
    [CHASSIS_HEIGHT]: 0.235,
    [CHASSIS_DEPTH]: 1.19,
    [CHASSIS_OFFSET]: new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](0, 0.159, 0),
    [CHASSIS_MASS]: 150,
};
const WHEEL_OPTIONS = {
    // wheel position
    [WHEEL_FRONT_OFFSET_DEPTH]: 1.013,
    [WHEEL_BACK_OFFSET_DEPTH]: 0.847,
    [WHEEL_OFFSET_WIDTH]: 0.739,
    [WHEEL_OFFSET_HEIGHT]: 0,
    [WHEEL_RADIUS]: 0.233,
    [WHEEL_DEPTH]: 0.45,
    [WHEEL_MASS]: 5,
    [AXLE_LOCAL]: new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](0, 0, 1),
    [CHASSIS_CONNECTION_POINT_LOCAL]: new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](-1, 0, 1),
    [DIRECTION_LOCAL]: new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](0, -1, 0),
    [CUSTOM_SLIDING_ROTATIONAL_SPEED]: -30,
    [DAMPING_COMPRESSION]: 4.4,
    [DAMPING_RELAXATION]: 2.3,
    [FRICTION_SLIP]: 5,
    [MAX_SUSPENSION_FORCE]: 100000,
    [MAX_SUSPENSION_TRAVEL]: 0.3,
    [RADIUS]: 0.6,
    [ROLL_INFLUENCE]: 0.01,
    [SUSPENSION_REST_LENGTH]: 0.3,
    [SUSPENSION_STIFFNESS]: 30,
    [USE_CUSTOM_SLIDING_ROTATIONAL_SPEED]: true,
};
// TODO: remove
const WHEEL_INDEXES = {
    frontLeft: 0,
    frontRight: 1,
    backLeft: 2,
    backRight: 3,
};


/***/ }),

/***/ "./src/app/view-port-3d/car/car.ts":
/*!*****************************************!*\
  !*** ./src/app/view-port-3d/car/car.ts ***!
  \*****************************************/
/*! exports provided: Car */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Car", function() { return Car; });
/* harmony import */ var _ecs_entity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ecs/entity */ "./src/app/view-port-3d/ecs/entity.ts");

class Car extends _ecs_entity__WEBPACK_IMPORTED_MODULE_0__["ECSEntity"] {
}


/***/ }),

/***/ "./src/app/view-port-3d/character/character.controller.ts":
/*!****************************************************************!*\
  !*** ./src/app/view-port-3d/character/character.controller.ts ***!
  \****************************************************************/
/*! exports provided: CharacterController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CharacterController", function() { return CharacterController; });
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");
/* harmony import */ var _input_keyboard_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../input/keyboard.input */ "./src/app/view-port-3d/input/keyboard.input.ts");
/* harmony import */ var _character_physics__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./character.physics */ "./src/app/view-port-3d/character/character.physics.ts");
/* harmony import */ var three_examples_jsm_loaders_FBXLoader_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! three/examples/jsm/loaders/FBXLoader.js */ "./node_modules/three/examples/jsm/loaders/FBXLoader.js");
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _states__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./states */ "./src/app/view-port-3d/character/states/index.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../utils */ "./src/app/view-port-3d/utils.ts");







class CharacterController extends _ecs_component__WEBPACK_IMPORTED_MODULE_0__["ECSComponent"] {
    constructor(options, cameraManager) {
        super(options);
        this.isHidden = false;
        this.bones = [];
        this.cameraManager = cameraManager;
    }
    awake() {
        this.loadModel();
        this.input = this.parent.getComponent(_input_keyboard_input__WEBPACK_IMPORTED_MODULE_1__["KeyboardInput"]);
        this.characterPhysics = this.parent.getComponent(_character_physics__WEBPACK_IMPORTED_MODULE_2__["CharacterPhysics"]);
    }
    loadModel() {
        const parent = this.entity;
        const loader = new three_examples_jsm_loaders_FBXLoader_js__WEBPACK_IMPORTED_MODULE_3__["FBXLoader"]();
        loader.setPath('assets/models/character/guard/');
        loader.load('castle_guard_01.fbx', (fbx) => {
            var _a, _b;
            this.model = fbx;
            this.model.scale.setScalar(0.008);
            this.model.updateWorldMatrix(false, true);
            (_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.scene) === null || _b === void 0 ? void 0 : _b.add(this.model);
            // @ts-ignore
            const bones = this.model.children[1].skeleton.bones;
            for (let b of bones) {
                this.bones[b.name] = b;
            }
            this.model.traverse((c) => {
                c.castShadow = true;
                c.receiveShadow = true;
                const mesh = c;
                const material = mesh.material;
                if (material && material.map) {
                    material.map.encoding = three__WEBPACK_IMPORTED_MODULE_4__["sRGBEncoding"];
                }
            });
            this.mixer = new three__WEBPACK_IMPORTED_MODULE_4__["AnimationMixer"](this.model);
            this.manager = new three__WEBPACK_IMPORTED_MODULE_4__["LoadingManager"]();
            this.manager.onLoad = () => {
                var _a, _b;
                (_a = parent.fsm) === null || _a === void 0 ? void 0 : _a.setState(_states__WEBPACK_IMPORTED_MODULE_5__["PLAYER_STATES"].IDLE);
                this.cameraManager.cameraTarget = (_b = this.characterPhysics) === null || _b === void 0 ? void 0 : _b.character;
                this.cameraManager.cameraTargetType = 'character';
                const loadingScreenElement = document.querySelector('.loading-screen');
                setTimeout(() => {
                    loadingScreenElement === null || loadingScreenElement === void 0 ? void 0 : loadingScreenElement.classList.add('hidden');
                }, 500);
            };
            const _OnLoad = (animName, anim) => {
                var _a;
                const clip = anim.animations[0];
                const action = (_a = this.mixer) === null || _a === void 0 ? void 0 : _a.clipAction(clip);
                parent.animations[animName] = {
                    clip: clip,
                    action: action,
                };
            };
            const loader = new three_examples_jsm_loaders_FBXLoader_js__WEBPACK_IMPORTED_MODULE_3__["FBXLoader"](this.manager);
            loader.setPath('assets/models/character/guard/');
            loader.load('Sword And Shield Idle.fbx', (a) => {
                _OnLoad(_states__WEBPACK_IMPORTED_MODULE_5__["PLAYER_STATES"].IDLE, a);
            });
            loader.load('Sword And Shield Run.fbx', (a) => {
                _OnLoad(_states__WEBPACK_IMPORTED_MODULE_5__["PLAYER_STATES"].RUN, a);
            });
            loader.load('Sword And Shield Walk.fbx', (a) => {
                _OnLoad(_states__WEBPACK_IMPORTED_MODULE_5__["PLAYER_STATES"].WALK, a);
            });
        });
    }
    get parent() {
        return this.entity;
    }
    update(dt) {
        var _a, _b, _c, _d, _e, _f, _g;
        if (this.isHidden) {
            return;
        }
        if (this.mixer) {
            this.mixer.update(dt);
        }
        const input = this.input;
        if (!input) {
            debugger;
            return;
        }
        (_a = this.characterPhysics) === null || _a === void 0 ? void 0 : _a.moveCharacter(dt, input);
        (_b = this.model) === null || _b === void 0 ? void 0 : _b.position.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_6__["toThreeVec3"])((_d = (_c = this.characterPhysics) === null || _c === void 0 ? void 0 : _c.character) === null || _d === void 0 ? void 0 : _d.body.position));
        this.model && (this.model.position.y -= 0.5);
        (_e = this.model) === null || _e === void 0 ? void 0 : _e.quaternion.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_6__["toThreeQuat"])((_g = (_f = this.characterPhysics) === null || _f === void 0 ? void 0 : _f.character) === null || _g === void 0 ? void 0 : _g.body.quaternion));
    }
    hide() {
        var _a;
        this.model.visible = false;
        (_a = this.characterPhysics) === null || _a === void 0 ? void 0 : _a.removeCharacter();
        this.isHidden = true;
    }
    show(car) {
        var _a, _b;
        // place left of car
        this.model.visible = true;
        if (car) {
            const distance = new three__WEBPACK_IMPORTED_MODULE_4__["Vector3"](2, 0, 0);
            const carPosition = Object(_utils__WEBPACK_IMPORTED_MODULE_6__["toThreeVec3"])(car.position.clone());
            const carQuaternion = Object(_utils__WEBPACK_IMPORTED_MODULE_6__["toThreeQuat"])(car.quaternion.clone());
            distance.applyQuaternion(new three__WEBPACK_IMPORTED_MODULE_4__["Quaternion"]().setFromAxisAngle(new three__WEBPACK_IMPORTED_MODULE_4__["Vector3"](0, 1, 0), Math.PI * -0.5));
            distance.applyQuaternion(carQuaternion);
            distance.add(carPosition);
            // this.characterPhysics?.character?.body.position.copy(
            // toCannonVec3(distance),
            // )
            (_a = this.characterPhysics) === null || _a === void 0 ? void 0 : _a.addCharacter(distance);
        }
        else {
            (_b = this.characterPhysics) === null || _b === void 0 ? void 0 : _b.addCharacter();
        }
        this.isHidden = false;
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/character/character.fsm.ts":
/*!*********************************************************!*\
  !*** ./src/app/view-port-3d/character/character.fsm.ts ***!
  \*********************************************************/
/*! exports provided: CharacterFSM */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CharacterFSM", function() { return CharacterFSM; });
/* harmony import */ var _fsm_fsm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../fsm/fsm */ "./src/app/view-port-3d/fsm/fsm.ts");
/* harmony import */ var _states__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./states */ "./src/app/view-port-3d/character/states/index.ts");
/* harmony import */ var _states_idle_state__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./states/idle-state */ "./src/app/view-port-3d/character/states/idle-state.ts");
/* harmony import */ var _states_run_state__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./states/run-state */ "./src/app/view-port-3d/character/states/run-state.ts");
/* harmony import */ var _states_walk_state__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./states/walk-state */ "./src/app/view-port-3d/character/states/walk-state.ts");





class CharacterFSM extends _fsm_fsm__WEBPACK_IMPORTED_MODULE_0__["FiniteStateMachine"] {
    constructor(character) {
        super();
        this.character = character;
        this.init();
    }
    init() {
        this.addState(_states__WEBPACK_IMPORTED_MODULE_1__["PLAYER_STATES"].IDLE, _states_idle_state__WEBPACK_IMPORTED_MODULE_2__["IdleState"]);
        this.addState(_states__WEBPACK_IMPORTED_MODULE_1__["PLAYER_STATES"].WALK, _states_walk_state__WEBPACK_IMPORTED_MODULE_4__["WalkState"]);
        this.addState(_states__WEBPACK_IMPORTED_MODULE_1__["PLAYER_STATES"].RUN, _states_run_state__WEBPACK_IMPORTED_MODULE_3__["RunState"]);
    }
    update(dt) {
        super.update(dt);
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/character/character.physics.ts":
/*!*************************************************************!*\
  !*** ./src/app/view-port-3d/character/character.physics.ts ***!
  \*************************************************************/
/*! exports provided: CharacterPhysics */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CharacterPhysics", function() { return CharacterPhysics; });
/* harmony import */ var cannon_es__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! cannon-es */ "./node_modules/cannon-es/dist/cannon-es.js");
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _physics_conversion_utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../physics/conversion-utils */ "./src/app/view-port-3d/physics/conversion-utils.ts");
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils */ "./src/app/view-port-3d/utils.ts");





const ITEM_NOT_FOUND_IN_ARRAY = -1;
class CharacterPhysics extends _ecs_component__WEBPACK_IMPORTED_MODULE_3__["ECSComponent"] {
    constructor() {
        super(...arguments);
        this.acceleration = new three__WEBPACK_IMPORTED_MODULE_1__["Vector3"](0.1, 0.125, 50.0);
        this.velocity = new three__WEBPACK_IMPORTED_MODULE_1__["Vector3"](0, 0.3, 0);
        this.position = new three__WEBPACK_IMPORTED_MODULE_1__["Vector3"]();
    }
    awake() {
        var _a, _b, _c, _d;
        this.physics = (_a = this.options) === null || _a === void 0 ? void 0 : _a.physics;
        this.characterMaterial = this.physics.defaultMaterial;
        const boxGeometry = new three__WEBPACK_IMPORTED_MODULE_1__["BoxGeometry"](1, 1, 1);
        const mat = (_b = this.physics) === null || _b === void 0 ? void 0 : _b.defaultWireframeMaterial;
        this.cube = new three__WEBPACK_IMPORTED_MODULE_1__["Mesh"](boxGeometry, mat);
        (_d = (_c = this.options) === null || _c === void 0 ? void 0 : _c.scene) === null || _d === void 0 ? void 0 : _d.add(this.cube);
    }
    update(deltaTime) {
        // console.log(this.character?.body.position)
        // if (this.character?.body) {
        //   const target = this.character.wireframe?.position.clone()
        //   const start = target!
        //   const direction = new Vector3(0, 0, 1)
        //   direction
        //     .applyQuaternion(toThreeQuat(this.character.body.quaternion))
        //     .normalize()
        //     .multiplyScalar(4)
        //   start.add(direction!)
        //   // console.log(start)
        //   this.cube!.position.copy(start!)
        //   this.cube!.quaternion.copy(
        //     toThreeQuat(this.character.body.quaternion),
        //   )
        // }
    }
    addCharacter(positions) {
        var _a, _b;
        this.character = this.makeCharacter(positions);
        (_a = this.physics) === null || _a === void 0 ? void 0 : _a.world.addBody(this.character.body);
        // this.options?.scene?.add(this.character.wireframe!)
        (_b = this.physics) === null || _b === void 0 ? void 0 : _b.collidable.push(this.character);
    }
    removeCharacter() {
        var _a, _b, _c, _d, _e, _f, _g;
        (_a = this.physics) === null || _a === void 0 ? void 0 : _a.world.removeBody((_b = this.character) === null || _b === void 0 ? void 0 : _b.body);
        (_d = (_c = this.options) === null || _c === void 0 ? void 0 : _c.scene) === null || _d === void 0 ? void 0 : _d.remove((_e = this.character) === null || _e === void 0 ? void 0 : _e.wireframe);
        const index = (_f = this.physics) === null || _f === void 0 ? void 0 : _f.collidable.indexOf(this.character);
        if (index !== ITEM_NOT_FOUND_IN_ARRAY) {
            (_g = this.physics) === null || _g === void 0 ? void 0 : _g.collidable.splice(index);
        }
    }
    makeCharacter(position) {
        var _a;
        const sphereShape = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Sphere"](0.5);
        const boxBody = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Body"]({
            mass: 1,
            shape: sphereShape,
            material: this.characterMaterial,
            angularDamping: 0.9,
            linearDamping: 0.99,
            allowSleep: false,
        });
        const mat = (_a = this.options) === null || _a === void 0 ? void 0 : _a.physics.defaultWireframeMaterial.clone();
        // mat!.wireframe = true
        if (position) {
            boxBody.position.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_4__["toCannonVec3"])(position));
        }
        else {
            // initial position
            boxBody.position.set(-83.89380190195465, 0.5, 3.531153492944502);
        }
        const initialRotation = new three__WEBPACK_IMPORTED_MODULE_1__["Quaternion"]().setFromAxisAngle(new three__WEBPACK_IMPORTED_MODULE_1__["Vector3"](0, 1, 0), Math.PI / 2);
        boxBody.quaternion.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_4__["toCannonQuat"])(initialRotation));
        const boxMesh = Object(_physics_conversion_utils__WEBPACK_IMPORTED_MODULE_2__["bodyToMesh"])(boxBody, mat);
        boxMesh.visible = false;
        const character = {
            body: boxBody,
            wireframe: boxMesh,
            isWireframeVisible: false,
        };
        return character;
    }
    /**
     * @description
     * Adjust velocity and rotation of character body
     */
    moveCharacter(dt, input) {
        var _a, _b, _c, _d, _e, _f, _g;
        const axis = new three__WEBPACK_IMPORTED_MODULE_1__["Vector3"]();
        const resultQuaternion = new three__WEBPACK_IMPORTED_MODULE_1__["Quaternion"]();
        const currentQuaternion = Object(_utils__WEBPACK_IMPORTED_MODULE_4__["toThreeQuat"])(this.character.body.quaternion.clone());
        const currentAcceleration = this.acceleration.clone();
        this.velocity.set(0, 0, 0);
        if (input.keys.shift) {
            currentAcceleration.multiplyScalar(2.0);
        }
        if (input.keys.forward) {
            this.velocity.z += currentAcceleration.z * dt;
        }
        if (input.keys.backward) {
            this.velocity.z -= currentAcceleration.z * dt;
        }
        if (input.keys.left) {
            axis.set(0, 1, 0);
            resultQuaternion.setFromAxisAngle(axis, 4.0 * Math.PI * dt * this.acceleration.y);
            currentQuaternion.multiply(resultQuaternion);
        }
        if (input.keys.right) {
            axis.set(0, 1, 0);
            resultQuaternion.setFromAxisAngle(axis, 4.0 * -Math.PI * dt * this.acceleration.y);
            currentQuaternion.multiply(resultQuaternion);
        }
        /**
         * HACK
         * because of sphere shape, need to somehow
         * disable for now rotation in x or z axis
         */
        currentQuaternion.x = 0;
        currentQuaternion.z = 0;
        this.velocity.applyQuaternion(currentQuaternion);
        (_a = this.character) === null || _a === void 0 ? void 0 : _a.body.quaternion.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_4__["toCannonQuat"])(currentQuaternion));
        this.character.body.velocity.x += this.velocity.x / 2;
        // this.character!.body.velocity.y += this.velocity.y
        this.character.body.velocity.z += this.velocity.z / 2;
        (_c = (_b = this.character) === null || _b === void 0 ? void 0 : _b.wireframe) === null || _c === void 0 ? void 0 : _c.position.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_4__["toThreeVec3"])((_d = this.character) === null || _d === void 0 ? void 0 : _d.body.position));
        (_f = (_e = this.character) === null || _e === void 0 ? void 0 : _e.wireframe) === null || _f === void 0 ? void 0 : _f.quaternion.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_4__["toThreeQuat"])((_g = this.character) === null || _g === void 0 ? void 0 : _g.body.quaternion));
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/character/character.ts":
/*!*****************************************************!*\
  !*** ./src/app/view-port-3d/character/character.ts ***!
  \*****************************************************/
/*! exports provided: Character */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Character", function() { return Character; });
/* harmony import */ var _ecs_entity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ecs/entity */ "./src/app/view-port-3d/ecs/entity.ts");
/* harmony import */ var _character_fsm__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./character.fsm */ "./src/app/view-port-3d/character/character.fsm.ts");


class Character extends _ecs_entity__WEBPACK_IMPORTED_MODULE_0__["ECSEntity"] {
    constructor() {
        super();
        this.animations = {};
        this.fsm = new _character_fsm__WEBPACK_IMPORTED_MODULE_1__["CharacterFSM"](this);
    }
    awake() {
        super.awake();
    }
    update(dt) {
        var _a;
        super.update(dt);
        (_a = this.fsm) === null || _a === void 0 ? void 0 : _a.update(dt);
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/character/states/idle-state.ts":
/*!*************************************************************!*\
  !*** ./src/app/view-port-3d/character/states/idle-state.ts ***!
  \*************************************************************/
/*! exports provided: IdleState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IdleState", function() { return IdleState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! . */ "./src/app/view-port-3d/character/states/index.ts");
/* harmony import */ var _fsm_finite_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../fsm/finite.state */ "./src/app/view-port-3d/fsm/finite.state.ts");
/* harmony import */ var _input_keyboard_input__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../input/keyboard.input */ "./src/app/view-port-3d/input/keyboard.input.ts");



class IdleState extends _fsm_finite_state__WEBPACK_IMPORTED_MODULE_1__["FiniteState"] {
    constructor(parent) {
        super(parent);
        this.name = ___WEBPACK_IMPORTED_MODULE_0__["PLAYER_STATES"].IDLE;
        const fsm = this.fsm;
        this.input = fsm.character.getComponent(_input_keyboard_input__WEBPACK_IMPORTED_MODULE_2__["KeyboardInput"]);
    }
    enter(prevState) {
        const fsm = this.fsm;
        const { animations } = fsm.character;
        const state = animations[this.name];
        const idleAction = state.action;
        if (!idleAction) {
            return;
        }
        if (prevState) {
            const prevAction = animations[prevState === null || prevState === void 0 ? void 0 : prevState.name].action;
            idleAction.time = 0.0;
            idleAction.enabled = true;
            idleAction.setEffectiveTimeScale(1.0);
            idleAction.setEffectiveWeight(1.0);
            idleAction.crossFadeFrom(prevAction, 0.25, true);
            idleAction.play();
        }
        else {
            idleAction.play();
        }
    }
    exit() { }
    update(_timeElapsed) {
        var _a, _b;
        const fsm = this.fsm;
        if (((_a = this.input) === null || _a === void 0 ? void 0 : _a.keys.forward) || ((_b = this.input) === null || _b === void 0 ? void 0 : _b.keys.backward)) {
            fsm.setState(___WEBPACK_IMPORTED_MODULE_0__["PLAYER_STATES"].WALK);
        }
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/character/states/index.ts":
/*!********************************************************!*\
  !*** ./src/app/view-port-3d/character/states/index.ts ***!
  \********************************************************/
/*! exports provided: PLAYER_STATES */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PLAYER_STATES", function() { return PLAYER_STATES; });
var PLAYER_STATES;
(function (PLAYER_STATES) {
    PLAYER_STATES["WALK"] = "WALK";
    PLAYER_STATES["RUN"] = "RUN";
    PLAYER_STATES["IDLE"] = "IDLE";
})(PLAYER_STATES || (PLAYER_STATES = {}));


/***/ }),

/***/ "./src/app/view-port-3d/character/states/run-state.ts":
/*!************************************************************!*\
  !*** ./src/app/view-port-3d/character/states/run-state.ts ***!
  \************************************************************/
/*! exports provided: RunState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RunState", function() { return RunState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! . */ "./src/app/view-port-3d/character/states/index.ts");
/* harmony import */ var _fsm_finite_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../fsm/finite.state */ "./src/app/view-port-3d/fsm/finite.state.ts");
/* harmony import */ var _input_keyboard_input__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../input/keyboard.input */ "./src/app/view-port-3d/input/keyboard.input.ts");



class RunState extends _fsm_finite_state__WEBPACK_IMPORTED_MODULE_1__["FiniteState"] {
    constructor(parent) {
        super(parent);
        this.name = ___WEBPACK_IMPORTED_MODULE_0__["PLAYER_STATES"].RUN;
        const fsm = this.fsm;
        this.input = fsm.character.getComponent(_input_keyboard_input__WEBPACK_IMPORTED_MODULE_2__["KeyboardInput"]);
    }
    enter(prevState) {
        const fsm = this.fsm;
        const { animations } = fsm.character;
        const curAction = animations[this.name].action;
        if (prevState) {
            const prevAction = animations[prevState.name].action;
            curAction.enabled = true;
            if (prevState.name == ___WEBPACK_IMPORTED_MODULE_0__["PLAYER_STATES"].WALK) {
                const ratio = (curAction === null || curAction === void 0 ? void 0 : curAction.getClip().duration) /
                    (prevAction === null || prevAction === void 0 ? void 0 : prevAction.getClip().duration);
                curAction.time = prevAction.time * ratio;
            }
            else {
                curAction.time = 0.0;
                curAction.setEffectiveTimeScale(1.0);
                curAction.setEffectiveWeight(1);
            }
            curAction.crossFadeFrom(prevAction, 0.1, true);
            curAction.play();
        }
        else {
            curAction.play();
        }
    }
    exit() { }
    update(_time) {
        var _a, _b, _c;
        const fsm = this.fsm;
        if (((_a = this.input) === null || _a === void 0 ? void 0 : _a.keys.forward) || ((_b = this.input) === null || _b === void 0 ? void 0 : _b.keys.backward)) {
            if (!((_c = this.input) === null || _c === void 0 ? void 0 : _c.keys.shift)) {
                fsm.setState(___WEBPACK_IMPORTED_MODULE_0__["PLAYER_STATES"].WALK);
            }
            return;
        }
        fsm.setState(___WEBPACK_IMPORTED_MODULE_0__["PLAYER_STATES"].IDLE);
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/character/states/walk-state.ts":
/*!*************************************************************!*\
  !*** ./src/app/view-port-3d/character/states/walk-state.ts ***!
  \*************************************************************/
/*! exports provided: WalkState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WalkState", function() { return WalkState; });
/* harmony import */ var ___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! . */ "./src/app/view-port-3d/character/states/index.ts");
/* harmony import */ var _fsm_finite_state__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../fsm/finite.state */ "./src/app/view-port-3d/fsm/finite.state.ts");
/* harmony import */ var _input_keyboard_input__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../input/keyboard.input */ "./src/app/view-port-3d/input/keyboard.input.ts");



class WalkState extends _fsm_finite_state__WEBPACK_IMPORTED_MODULE_1__["FiniteState"] {
    constructor(parent) {
        super(parent);
        this.name = ___WEBPACK_IMPORTED_MODULE_0__["PLAYER_STATES"].WALK;
        const fsm = this.fsm;
        this.input = fsm.character.getComponent(_input_keyboard_input__WEBPACK_IMPORTED_MODULE_2__["KeyboardInput"]);
    }
    enter(prevState) {
        const fsm = this.fsm;
        const { animations } = fsm.character;
        const curAction = animations[this.name].action;
        if (prevState) {
            const prevAction = animations[prevState.name].action;
            curAction.enabled = true;
            if (prevState.name == ___WEBPACK_IMPORTED_MODULE_0__["PLAYER_STATES"].RUN) {
                const ratio = (curAction === null || curAction === void 0 ? void 0 : curAction.getClip().duration) /
                    (prevAction === null || prevAction === void 0 ? void 0 : prevAction.getClip().duration);
                curAction.time = prevAction.time * ratio;
                curAction.crossFadeFrom(prevAction, 0.4, true);
            }
            else {
                curAction.time = 0.0;
                curAction.setEffectiveTimeScale(1.0);
                curAction.setEffectiveWeight(1);
                curAction.crossFadeFrom(prevAction, 0.1, true);
            }
            curAction.play();
        }
        else {
            curAction.play();
        }
    }
    exit() { }
    update(_time) {
        var _a, _b, _c;
        const fsm = this.fsm;
        if (((_a = this.input) === null || _a === void 0 ? void 0 : _a.keys.forward) || ((_b = this.input) === null || _b === void 0 ? void 0 : _b.keys.backward)) {
            if ((_c = this.input) === null || _c === void 0 ? void 0 : _c.keys.shift) {
                fsm.setState(___WEBPACK_IMPORTED_MODULE_0__["PLAYER_STATES"].RUN);
            }
            return;
        }
        fsm.setState(___WEBPACK_IMPORTED_MODULE_0__["PLAYER_STATES"].IDLE);
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/ecs/component.ts":
/*!***********************************************!*\
  !*** ./src/app/view-port-3d/ecs/component.ts ***!
  \***********************************************/
/*! exports provided: ECSComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ECSComponent", function() { return ECSComponent; });
class ECSComponent {
    constructor(options) {
        this.options = options;
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/ecs/entity-manager.ts":
/*!****************************************************!*\
  !*** ./src/app/view-port-3d/ecs/entity-manager.ts ***!
  \****************************************************/
/*! exports provided: ECSEntityManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ECSEntityManager", function() { return ECSEntityManager; });
class ECSEntityManager {
    constructor() {
        this._entities = [];
    }
    update(dt) {
        Object.values(this._entities).forEach((entity) => entity.update(dt));
    }
    awake() {
        Object.values(this._entities).forEach((entity) => entity.awake());
    }
    addEntity(entity) {
        entity.entityManger = this;
        this._entities.push(entity);
    }
    getEntity(constr) {
        const result = this._entities.find((entity) => entity instanceof constr);
        return result;
    }
    removeEntity(constr) {
        this._entities = this._entities.filter((entity) => !(entity instanceof constr));
    }
    hasComponent(constr) {
        return Boolean(this.getEntity(constr));
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/ecs/entity.ts":
/*!********************************************!*\
  !*** ./src/app/view-port-3d/ecs/entity.ts ***!
  \********************************************/
/*! exports provided: ECSEntity */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ECSEntity", function() { return ECSEntity; });
class ECSEntity {
    constructor() {
        this._components = [];
    }
    get components() {
        return this._components;
    }
    addComponent(component) {
        component.entity = this;
        this._components.push(component);
    }
    getComponent(constr) {
        const result = this._components.find((component) => component instanceof constr);
        return result !== null && result !== void 0 ? result : null;
    }
    removeComponent(constr) {
        this._components = this._components.filter((component) => !(component instanceof constr));
    }
    hasComponent(constr) {
        return Boolean(this.getComponent(constr));
    }
    update(deltaTime) {
        this._components.forEach((component) => component.update(deltaTime));
    }
    awake() {
        this._components.forEach((component) => component.awake());
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/fsm/finite.state.ts":
/*!**************************************************!*\
  !*** ./src/app/view-port-3d/fsm/finite.state.ts ***!
  \**************************************************/
/*! exports provided: FiniteState */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiniteState", function() { return FiniteState; });
class FiniteState {
    constructor(fsm) {
        this.name = null;
        this.fsm = fsm;
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/fsm/fsm.ts":
/*!*****************************************!*\
  !*** ./src/app/view-port-3d/fsm/fsm.ts ***!
  \*****************************************/
/*! exports provided: FiniteStateMachine */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiniteStateMachine", function() { return FiniteStateMachine; });
class FiniteStateMachine {
    constructor() {
        this.states = {};
        this.currentState = null;
    }
    addState(key, type) {
        this.states[key] = type;
    }
    setState(key) {
        const prevState = this.currentState;
        if (prevState) {
            if (prevState.name === key) {
                return;
            }
            prevState.exit();
        }
        try {
            const stateClass = this.states[key];
            const state = new stateClass(this);
            this.currentState = state;
            state.enter(prevState);
        }
        catch (e) {
            console.error(`NO FINITE STATE FOR NAME: ${key}`, e);
            debugger;
        }
    }
    update(timeElapsed) {
        if (this.currentState) {
            this.currentState.update(timeElapsed);
        }
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/index.ts":
/*!***************************************!*\
  !*** ./src/app/view-port-3d/index.ts ***!
  \***************************************/
/*! exports provided: ViewPort3D */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewPort3D", function() { return ViewPort3D; });
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _ecs_entity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ecs/entity */ "./src/app/view-port-3d/ecs/entity.ts");
/* harmony import */ var _ecs_entity_manager__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ecs/entity-manager */ "./src/app/view-port-3d/ecs/entity-manager.ts");
/* harmony import */ var _tools_stats_tool__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tools/stats-tool */ "./src/app/view-port-3d/tools/stats-tool.ts");
/* harmony import */ var _physics_physics__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./physics/physics */ "./src/app/view-port-3d/physics/physics.ts");
/* harmony import */ var _player_player__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./player/player */ "./src/app/view-port-3d/player/player.ts");
/* harmony import */ var _player_player_controller__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./player/player-controller */ "./src/app/view-port-3d/player/player-controller.ts");
/* harmony import */ var _level_level__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./level/level */ "./src/app/view-port-3d/level/level.ts");
/* harmony import */ var _level_level_controller__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./level/level-controller */ "./src/app/view-port-3d/level/level-controller.ts");
/* harmony import */ var _level_sky__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./level/sky */ "./src/app/view-port-3d/level/sky.ts");
/* harmony import */ var _car_car__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./car/car */ "./src/app/view-port-3d/car/car.ts");
/* harmony import */ var _car_car_controller__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./car/car.controller */ "./src/app/view-port-3d/car/car.controller.ts");
/* harmony import */ var dat_gui__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! dat.gui */ "./node_modules/dat.gui/build/dat.gui.module.js");
/* harmony import */ var _input_keyboard_input__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./input/keyboard.input */ "./src/app/view-port-3d/input/keyboard.input.ts");
/* harmony import */ var _car_car_physics__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./car/car.physics */ "./src/app/view-port-3d/car/car.physics.ts");
/* harmony import */ var _character_character__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./character/character */ "./src/app/view-port-3d/character/character.ts");
/* harmony import */ var _character_character_controller__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./character/character.controller */ "./src/app/view-port-3d/character/character.controller.ts");
/* harmony import */ var _character_character_physics__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./character/character.physics */ "./src/app/view-port-3d/character/character.physics.ts");
/* harmony import */ var _camera_camera_manager__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./camera/camera-manager */ "./src/app/view-port-3d/camera/camera-manager.ts");
/* harmony import */ var _camera_camera_controller__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./camera/camera.controller */ "./src/app/view-port-3d/camera/camera.controller.ts");
/* harmony import */ var _level_ads_controller__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./level/ads-controller */ "./src/app/view-port-3d/level/ads-controller.ts");





















class ViewPort3D extends _ecs_entity__WEBPACK_IMPORTED_MODULE_1__["ECSEntity"] {
    constructor(textures) {
        super();
        this.entityManager = new _ecs_entity_manager__WEBPACK_IMPORTED_MODULE_2__["ECSEntityManager"]();
        this.scene = new three__WEBPACK_IMPORTED_MODULE_0__["Scene"]();
        this.gui = new dat_gui__WEBPACK_IMPORTED_MODULE_12__["GUI"]();
        this.previousTick = 0;
        this.clock = new three__WEBPACK_IMPORTED_MODULE_0__["Clock"]();
        /**
         * Player state
         */
        this.isPlayerInVehicle = false;
        this.textures = textures;
    }
    awake() {
        this.addComponent(new _tools_stats_tool__WEBPACK_IMPORTED_MODULE_3__["StatsTool"]('#stats'));
        this.setTheeJs();
        /**
         * Input
         */
        const keyboardInput = new _input_keyboard_input__WEBPACK_IMPORTED_MODULE_13__["KeyboardInput"]();
        /**
         * Camera manager
         */
        this.cameraManager = new _camera_camera_manager__WEBPACK_IMPORTED_MODULE_18__["CameraManager"](this.renderer, keyboardInput, this.gui);
        this.entityManager.addEntity(this.cameraManager);
        const cameraController = new _camera_camera_controller__WEBPACK_IMPORTED_MODULE_19__["CameraController"]({
            scene: this.scene,
            gui: this.gui,
        });
        this.cameraManager.addComponent(cameraController);
        /**
         * Physics
         */
        this.physics = new _physics_physics__WEBPACK_IMPORTED_MODULE_4__["Physics"]({
            scene: this.scene,
            gui: this.gui,
        });
        this.entityManager.addEntity(this.physics);
        /**
         * Character
         */
        const character = new _character_character__WEBPACK_IMPORTED_MODULE_15__["Character"]();
        character.addComponent(keyboardInput);
        const characterController = new _character_character_controller__WEBPACK_IMPORTED_MODULE_16__["CharacterController"]({
            scene: this.scene,
            physics: this.physics,
            gui: this.gui,
        }, this.cameraManager);
        character.addComponent(characterController);
        const characterPhysics = new _character_character_physics__WEBPACK_IMPORTED_MODULE_17__["CharacterPhysics"]({
            scene: this.scene,
            physics: this.physics,
            gui: this.gui,
        });
        character.addComponent(characterPhysics);
        this.entityManager.addEntity(character);
        /**
         * Car
         */
        const car = new _car_car__WEBPACK_IMPORTED_MODULE_10__["Car"]();
        const carController = new _car_car_controller__WEBPACK_IMPORTED_MODULE_11__["CarController"]({
            scene: this.scene,
            physics: this.physics,
            gui: this.gui,
        });
        car.addComponent(carController);
        const carPhysics = new _car_car_physics__WEBPACK_IMPORTED_MODULE_14__["CarPhysics"]({
            scene: this.scene,
            physics: this.physics,
            gui: this.gui,
        });
        car.addComponent(carPhysics);
        this.entityManager.addEntity(car);
        /**
         * Player
         */
        const player = new _player_player__WEBPACK_IMPORTED_MODULE_5__["Player"]();
        player.addComponent(keyboardInput);
        const playerController = new _player_player_controller__WEBPACK_IMPORTED_MODULE_6__["PlayerController"]({
            scene: this.scene,
            physics: this.physics,
            gui: this.gui,
        }, carController, characterController, this.cameraManager);
        player.addComponent(playerController);
        this.entityManager.addEntity(player);
        const level = new _level_level__WEBPACK_IMPORTED_MODULE_7__["Level"]();
        const adsController = new _level_ads_controller__WEBPACK_IMPORTED_MODULE_20__["AdsController"](Object.assign({ scene: this.scene, camera: this.cameraManager.camera, physics: this.physics, gui: this.gui }, this.textures));
        level.addComponent(adsController);
        const levelController = new _level_level_controller__WEBPACK_IMPORTED_MODULE_8__["LevelController"](Object.assign({ scene: this.scene, camera: this.cameraManager.camera, physics: this.physics, gui: this.gui }, this.textures));
        level.addComponent(levelController);
        const sky = new _level_sky__WEBPACK_IMPORTED_MODULE_9__["Sky"]({
            scene: this.scene,
            camera: this.cameraManager.camera,
            physics: this.physics,
            gui: this.gui,
        });
        level.addComponent(sky);
        this.entityManager.addEntity(level);
        this.entityManager.awake();
        window.requestAnimationFrame(() => {
            this.previousTick = Date.now();
            this.update();
        });
        carPhysics.addCar();
        characterPhysics.addCharacter();
    }
    update() {
        var _a;
        (_a = this.renderer) === null || _a === void 0 ? void 0 : _a.render(this.scene, this.cameraManager.camera);
        const elapsedTime = this.clock.getElapsedTime();
        const deltaTime = elapsedTime - this.previousTick;
        this.previousTick = elapsedTime;
        super.update(deltaTime);
        this.entityManager.update(deltaTime);
        // this.controls?.update()
        window.requestAnimationFrame(() => this.update());
    }
    setTheeJs() {
        this.setRenderer();
        this.setScene();
        this.setComposer();
        // this.adjustBuiltInLambertShader()
    }
    // https://stackoverflow.com/questions/47367181/threejs-material-with-shadows-but-no-lights
    // update: doesn't work with current threejs version, using shadow material with mesh duplication
    adjustBuiltInLambertShader() {
        three__WEBPACK_IMPORTED_MODULE_0__["ShaderLib"]['lambert'].fragmentShader = three__WEBPACK_IMPORTED_MODULE_0__["ShaderLib"]['lambert'].fragmentShader.replace(`vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + totalEmissiveRadiance;`, `#ifndef CUSTOM
          vec3 outgoingLight = reflectedLight.directDiffuse + reflectedLight.indirectDiffuse + totalEmissiveRadiance;
      #else
          vec3 outgoingLight = diffuseColor.rgb * ( 1.0 - 0.5 * ( 1.0 - getShadowMask() ) ); // shadow intensity hardwired to 0.5 here
      #endif`);
    }
    setRenderer() {
        var _a;
        this.renderer = new three__WEBPACK_IMPORTED_MODULE_0__["WebGLRenderer"]({
            antialias: true,
        });
        this.renderer.outputEncoding = three__WEBPACK_IMPORTED_MODULE_0__["sRGBEncoding"];
        // this.renderer.shadowMap.enabled = true
        this.renderer.shadowMap.type = three__WEBPACK_IMPORTED_MODULE_0__["PCFSoftShadowMap"];
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.domElement.id = 'threejs';
        this.renderer.physicallyCorrectLights = true;
        this.renderer.toneMapping = three__WEBPACK_IMPORTED_MODULE_0__["ACESFilmicToneMapping"];
        this.renderer.shadowMap.enabled = true;
        this.renderer.shadowMap.type = three__WEBPACK_IMPORTED_MODULE_0__["PCFSoftShadowMap"];
        (_a = document
            .getElementById('container')) === null || _a === void 0 ? void 0 : _a.appendChild(this.renderer.domElement);
        window.addEventListener('resize', () => {
            this.onWindowResize();
        }, false);
    }
    onWindowResize() {
        var _a, _b, _c;
        if ((_a = this.cameraManager) === null || _a === void 0 ? void 0 : _a.camera) {
            const camera = this.cameraManager.camera;
            if (camera instanceof three__WEBPACK_IMPORTED_MODULE_0__["PerspectiveCamera"]) {
                camera.aspect = window.innerWidth / window.innerHeight;
                camera.updateProjectionMatrix();
            }
        }
        (_b = this.renderer) === null || _b === void 0 ? void 0 : _b.setSize(window.innerWidth, window.innerHeight);
        (_c = this.renderer) === null || _c === void 0 ? void 0 : _c.setPixelRatio(Math.min(window.devicePixelRatio, 2));
    }
    setScene() {
        this.scene.background = new three__WEBPACK_IMPORTED_MODULE_0__["Color"](0xffffff);
        this.scene.fog = new three__WEBPACK_IMPORTED_MODULE_0__["FogExp2"](0x89b2eb, 0.002);
    }
    setComposer() { }
}


/***/ }),

/***/ "./src/app/view-port-3d/input/keyboard.input.ts":
/*!******************************************************!*\
  !*** ./src/app/view-port-3d/input/keyboard.input.ts ***!
  \******************************************************/
/*! exports provided: KeyboardInput */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KeyboardInput", function() { return KeyboardInput; });
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");

class KeyboardInput extends _ecs_component__WEBPACK_IMPORTED_MODULE_0__["ECSComponent"] {
    constructor() {
        super();
        this.keys = {
            forward: false,
            backward: false,
            left: false,
            right: false,
            space: false,
            shift: false,
            digit_1: false,
            digit_2: false,
            digit_3: false,
            digit_4: false,
            digit_0: false,
        };
        this.listenKeyDown((event) => this.onKeyDown(event));
        this.listenKeyUp((event) => this.onKeyUp(event));
    }
    awake() { }
    listenKeyUp(callback) {
        document.addEventListener('keyup', (e) => callback(e), false);
    }
    listenKeyDown(callback) {
        document.addEventListener('keydown', (e) => callback(e), false);
    }
    onKeyUp(event) {
        switch (event.keyCode) {
            case 87: // w
                this.keys.forward = false;
                break;
            case 65: // a
                this.keys.left = false;
                break;
            case 83: // s
                this.keys.backward = false;
                break;
            case 68: // d
                this.keys.right = false;
                break;
            case 32: // SPACE
                this.keys.space = false;
                break;
            case 16: // SHIFT
                this.keys.shift = false;
                break;
            case 49:
                this.keys.digit_1 = false;
                break;
            case 50:
                this.keys.digit_2 = false;
                break;
            case 51:
                this.keys.digit_3 = false;
                break;
            case 52:
                this.keys.digit_4 = false;
                break;
            case 48:
                this.keys.digit_0 = false;
                break;
        }
    }
    onKeyDown(event) {
        switch (event.keyCode) {
            case 87: // w
                this.keys.forward = true;
                break;
            case 65: // a
                this.keys.left = true;
                break;
            case 83: // s
                this.keys.backward = true;
                break;
            case 68: // d
                this.keys.right = true;
                break;
            case 32: // SPACE
                this.keys.space = true;
                break;
            case 16: // SHIFT
                this.keys.shift = true;
                break;
            case 49:
                this.keys.digit_1 = true;
                break;
            case 50:
                this.keys.digit_2 = true;
                break;
            case 51:
                this.keys.digit_3 = true;
                break;
            case 52:
                this.keys.digit_4 = true;
                break;
            case 48:
                this.keys.digit_0 = true;
                break;
        }
    }
    update(deltaTime) { }
}


/***/ }),

/***/ "./src/app/view-port-3d/level/ads-controller.ts":
/*!******************************************************!*\
  !*** ./src/app/view-port-3d/level/ads-controller.ts ***!
  \******************************************************/
/*! exports provided: AdsController, setCustomUvs, removeFolderFromGUI */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdsController", function() { return AdsController; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setCustomUvs", function() { return setCustomUvs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeFolderFromGUI", function() { return removeFolderFromGUI; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");
/* harmony import */ var three_examples_jsm_loaders_GLTFLoader_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! three/examples/jsm/loaders/GLTFLoader.js */ "./node_modules/three/examples/jsm/loaders/GLTFLoader.js");
/* harmony import */ var _uv_map__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./uv-map */ "./src/app/view-port-3d/level/uv-map.ts");





class AdsController extends _ecs_component__WEBPACK_IMPORTED_MODULE_2__["ECSComponent"] {
    constructor(options) {
        super(options);
        this.raycaster = new three__WEBPACK_IMPORTED_MODULE_1__["Raycaster"]();
        this.banners = [];
        this.gltfLoader = new three_examples_jsm_loaders_GLTFLoader_js__WEBPACK_IMPORTED_MODULE_3__["GLTFLoader"]();
        this.textureLoader = new three__WEBPACK_IMPORTED_MODULE_1__["TextureLoader"]();
        this.videoTextures = options.videoTextures;
        this.adsInitialTexturesMap = options.adsInitialTexturesMap;
        this.lightmapTexture = options.lightmapTexture;
        this.gui = options.gui;
        const inputEl = document.querySelector('#texture-upload');
        // @ts-ignore
        inputEl === null || inputEl === void 0 ? void 0 : inputEl.addEventListener('input', (event) => this.onTextureLoad(event));
    }
    onTextureLoad(event) {
        return Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"])(this, void 0, void 0, function* () {
            const target = event.target;
            const files = target.files;
            if (!files.length) {
                return;
            }
            const file = files[0];
            const { type } = file;
            let fileUrl = URL.createObjectURL(file);
            if (type.includes('image')) {
                const texture = yield this.textureLoader.loadAsync(fileUrl);
                // @ts-ignore
                this.selectedBanner.material.map = texture;
                // @ts-ignore
                this.selectedBanner.material.map.needsUpdate = true;
                // @ts-ignore
                this.selectedBanner.material.needsUpdate = true;
            }
            else {
                var video = document.createElement('video');
                video.src = fileUrl;
                video.loop = true;
                video.play();
                const videoTexture = new three__WEBPACK_IMPORTED_MODULE_1__["VideoTexture"](video);
                // videoTexture.flipY
                // @ts-ignore
                this.selectedBanner.material.map = videoTexture;
                // @ts-ignore
                this.selectedBanner.material.map.needsUpdate = true;
                // @ts-ignore
                this.selectedBanner.material.needsUpdate = true;
                // textur
            }
        });
    }
    awake() {
        document.addEventListener('click', (event) => this.onADSMeshSelect(event));
        this.loadAdsModel();
    }
    onADSMeshSelect(event) {
        var _a;
        const mouse3D = new three__WEBPACK_IMPORTED_MODULE_1__["Vector3"]((event.clientX / window.innerWidth) * 2 - 1, -(event.clientY / window.innerHeight) * 2 + 1, 0.5);
        this.raycaster.setFromCamera(mouse3D, (_a = this.options) === null || _a === void 0 ? void 0 : _a.camera);
        var intersects = this.raycaster.intersectObjects(this.banners);
        if (!intersects.length) {
            return;
        }
        const mesh = intersects[0].object;
        if (mesh) {
            if (this.selectedBanner &&
                mesh.name !== this.selectedBanner.name) {
                removeFolderFromGUI(this.selectedBanner.name, this.gui);
            }
            this.selectedBanner = mesh;
            const doesFolderWithGivenNameExist = this.gui.__folders[this.selectedBanner.name];
            if (!doesFolderWithGivenNameExist) {
                /**
                 * Create a folder for a selected mesh
                 */
                const selectedObjectFolder = this.gui.addFolder(this.selectedBanner.name);
                selectedObjectFolder
                    .add(this, 'importVideo')
                    .name('import video/image');
                selectedObjectFolder
                    .add(this, 'setVideo1ToSelectedMesh')
                    .name('video 1');
                selectedObjectFolder
                    .add(this, 'setVideo2ToSelectedMesh')
                    .name('video 2');
                selectedObjectFolder
                    .add(this, 'setVideo3ToSelectedMesh')
                    .name('video 3');
                selectedObjectFolder
                    .add(this, 'setVideo4ToSelectedMesh')
                    .name('video 4');
                selectedObjectFolder
                    .add(this, 'setVideo5ToSelectedMesh')
                    .name('video 5');
                selectedObjectFolder
                    .add(this, 'setVideo6ToSelectedMesh')
                    .name('video 6');
            }
        }
        else {
            if (this.selectedBanner) {
                removeFolderFromGUI(this.selectedBanner.name, this.gui);
            }
            this.selectedBanner = undefined;
        }
    }
    importVideo() {
        const input = document.querySelector('#texture-upload');
        // @ts-ignorea
        input.click();
    }
    setVideoToSelectedMesh(videoIndex) {
        var _a;
        // @ts-ignore
        if (!((_a = this.selectedBanner) === null || _a === void 0 ? void 0 : _a.material.map)) {
            debugger;
            return;
        }
        const videoTexture = this.videoTextures[videoIndex];
        // @ts-ignore
        this.selectedBanner.material.map = videoTexture;
        // @ts-ignore
        this.selectedBanner.material.needsUpdate = true;
    }
    setVideo1ToSelectedMesh() {
        this.setVideoToSelectedMesh(1);
    }
    setVideo2ToSelectedMesh() {
        this.setVideoToSelectedMesh(2);
    }
    setVideo3ToSelectedMesh() {
        this.setVideoToSelectedMesh(3);
    }
    setVideo4ToSelectedMesh() {
        this.setVideoToSelectedMesh(4);
    }
    setVideo5ToSelectedMesh() {
        this.setVideoToSelectedMesh(5);
    }
    setVideo6ToSelectedMesh() {
        this.setVideoToSelectedMesh(6);
    }
    getInitialADSTextureMap(name) {
        try {
            const ratioMatch = name.match(/\dx\d/);
            let type = ratioMatch[0];
            const texture = this.adsInitialTexturesMap[type];
            return texture;
        }
        catch (e) {
            return null;
        }
    }
    update(deltaTime) { }
    loadAdsModel() {
        // PATTERN: ${frame ratio}.ads-{type of frame}-lightmapTextures-${lightmapTexture id}.${blender ending}
        this.gltfLoader.setPath('assets/models/city/');
        try {
            this.gltfLoader.load('model-ads.glb', (gltf) => {
                var _a, _b;
                const { scene } = gltf;
                scene.traverse((child) => {
                    if (!(child instanceof three__WEBPACK_IMPORTED_MODULE_1__["Mesh"])) {
                        return;
                    }
                    if (!child.name.includes('ads')) {
                        debugger;
                    }
                    /**
                     * get lightmapTexture atlas for mesh
                     */
                    const lightmapTexture = this.lightmapTexture;
                    if (!lightmapTexture) {
                        debugger;
                        return;
                    }
                    /**
                     * get initial frame texture
                     */
                    const texture = this.getInitialADSTextureMap(child.name);
                    if (!texture) {
                        debugger;
                        return;
                    }
                    lightmapTexture.flipY = false;
                    const material = new three__WEBPACK_IMPORTED_MODULE_1__["MeshBasicMaterial"]({
                        color: new three__WEBPACK_IMPORTED_MODULE_1__["Color"](0xffffff),
                        map: texture,
                        // @ts-ignore
                        // TODO: check if three.js types are broken? -> broken
                        lightMap: lightmapTexture,
                    });
                    child.material = material;
                    // set secondary UV
                    const uvs = child.geometry.attributes.uv.clone().array;
                    child.geometry.setAttribute('uv2', new three__WEBPACK_IMPORTED_MODULE_1__["BufferAttribute"](uvs, 2));
                    this.setPrimaryUvs(child);
                    child.geometry.uvsNeedUpdate = true;
                    this.banners.push(child);
                });
                (_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.scene) === null || _b === void 0 ? void 0 : _b.add(scene);
            });
        }
        catch (e) {
            console.error(e);
            debugger;
        }
    }
    setPrimaryUvs(mesh) {
        // @ts-ignore
        const uvs = _uv_map__WEBPACK_IMPORTED_MODULE_4__["UV_MAP_BY_NAME"][mesh.name];
        try {
            setCustomUvs(mesh, uvs);
        }
        catch (e) {
            debugger;
        }
    }
}
function setCustomUvs(mesh, uvs) {
    const uvAttr = mesh.geometry.attributes.uv;
    // @ts-ignore
    mesh.geometry.uvsNeedUpdate = true;
    for (let i = 0; i < uvAttr.count; i++) {
        const arr = Array.prototype.slice.call(mesh.geometry.attributes.position.array);
        uvAttr.setXY(i, uvs[i][0], uvs[i][1]);
    }
    uvAttr.needsUpdate = true;
}
function removeFolderFromGUI(name, gui) {
    const folder = gui.__folders[name];
    if (!folder) {
        return;
    }
    folder.close();
    //@ts-ignore
    gui.__ul.removeChild(folder.domElement.parentNode);
    delete gui.__folders[name];
    //@ts-ignore
    gui.onResize();
}


/***/ }),

/***/ "./src/app/view-port-3d/level/level-controller.ts":
/*!********************************************************!*\
  !*** ./src/app/view-port-3d/level/level-controller.ts ***!
  \********************************************************/
/*! exports provided: RATIO_2X3, RATIO_5X2, RATIO_5X3, LevelController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RATIO_2X3", function() { return RATIO_2X3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RATIO_5X2", function() { return RATIO_5X2; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RATIO_5X3", function() { return RATIO_5X3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LevelController", function() { return LevelController; });
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var three_examples_jsm_loaders_GLTFLoader_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three/examples/jsm/loaders/GLTFLoader.js */ "./node_modules/three/examples/jsm/loaders/GLTFLoader.js");
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");
/* harmony import */ var _physics_conversion_utils__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../physics/conversion-utils */ "./src/app/view-port-3d/physics/conversion-utils.ts");




const RATIO_2X3 = '2x3';
const RATIO_5X2 = '5x2';
const RATIO_5X3 = '5x3';
class LevelController extends _ecs_component__WEBPACK_IMPORTED_MODULE_2__["ECSComponent"] {
    constructor(options) {
        super();
        this.adsBanners = [];
        this.defaultWireframeMaterial = new three__WEBPACK_IMPORTED_MODULE_0__["MeshBasicMaterial"]({
            color: new three__WEBPACK_IMPORTED_MODULE_0__["Color"](0xff0000),
        });
        this.textures = [];
        this.textureLoader = new three__WEBPACK_IMPORTED_MODULE_0__["TextureLoader"]();
        this.gltfLoader = new three_examples_jsm_loaders_GLTFLoader_js__WEBPACK_IMPORTED_MODULE_1__["GLTFLoader"]();
        this.options = options;
        this.gui = options.gui;
        this.combinedTexturesMap = options.combinedTexturesMap;
    }
    awake() {
        this.loadModel();
        this.loadBlockMesh();
    }
    update(deltaTime) { }
    /**
     * DO NOT APPLY SCALE FOR VOXELS IN BLENDER!!!
     */
    // TODO: Move to level.physics.ts
    loadBlockMesh() {
        const loader = new three_examples_jsm_loaders_GLTFLoader_js__WEBPACK_IMPORTED_MODULE_1__["GLTFLoader"]();
        loader.setPath('assets/models/city/');
        try {
            loader.load('model-phys.glb', (gltf) => {
                var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
                const scene = Object(_physics_conversion_utils__WEBPACK_IMPORTED_MODULE_3__["sceneToWorld"])(gltf.scene, (_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.physics) === null || _b === void 0 ? void 0 : _b.defaultMaterial, (_d = (_c = this.options) === null || _c === void 0 ? void 0 : _c.physics) === null || _d === void 0 ? void 0 : _d.defaultWireframeMaterial);
                (_f = (_e = this.options) === null || _e === void 0 ? void 0 : _e.scene) === null || _f === void 0 ? void 0 : _f.add(scene.wireframe);
                (_h = (_g = this.options) === null || _g === void 0 ? void 0 : _g.physics) === null || _h === void 0 ? void 0 : _h.world.addBody(scene.body);
                (_k = (_j = this.options) === null || _j === void 0 ? void 0 : _j.physics) === null || _k === void 0 ? void 0 : _k.collidable.push(scene);
            });
        }
        catch (e) {
            console.error(e);
            debugger;
        }
    }
    loadModel() {
        this.gltfLoader.setPath('assets/models/city/');
        try {
            this.gltfLoader.load('model-city.glb', (gltf) => {
                var _a, _b, _c, _d;
                const { scene } = gltf;
                const shadowGroup = new three__WEBPACK_IMPORTED_MODULE_0__["Group"]();
                scene.traverse((child) => {
                    if (child instanceof three__WEBPACK_IMPORTED_MODULE_0__["Mesh"]) {
                        const key = child.name;
                        const texture = this.combinedTexturesMap[key];
                        if (!texture) {
                            return;
                        }
                        // TODO: UNCOMMENT
                        const shadowMesh = child.clone();
                        const shadowMaterial = new three__WEBPACK_IMPORTED_MODULE_0__["ShadowMaterial"]();
                        shadowMaterial.opacity = 0.2;
                        shadowMesh.material = shadowMaterial;
                        shadowMesh.receiveShadow = true;
                        shadowGroup.add(shadowMesh);
                        const bakedMaterial = new three__WEBPACK_IMPORTED_MODULE_0__["MeshBasicMaterial"]({
                            map: texture,
                        });
                        child.material = bakedMaterial;
                        child.material.map.flipY = false;
                        if (child.material && child.material.map) {
                            child.material.map.encoding = three__WEBPACK_IMPORTED_MODULE_0__["sRGBEncoding"];
                        }
                    }
                });
                (_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.scene) === null || _b === void 0 ? void 0 : _b.add(scene);
                (_d = (_c = this.options) === null || _c === void 0 ? void 0 : _c.scene) === null || _d === void 0 ? void 0 : _d.add(shadowGroup);
            });
        }
        catch (e) {
            console.error(e);
            debugger;
        }
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/level/level.ts":
/*!*********************************************!*\
  !*** ./src/app/view-port-3d/level/level.ts ***!
  \*********************************************/
/*! exports provided: Level */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Level", function() { return Level; });
/* harmony import */ var _ecs_entity__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ecs/entity */ "./src/app/view-port-3d/ecs/entity.ts");

class Level extends _ecs_entity__WEBPACK_IMPORTED_MODULE_0__["ECSEntity"] {
}


/***/ }),

/***/ "./src/app/view-port-3d/level/sky.ts":
/*!*******************************************!*\
  !*** ./src/app/view-port-3d/level/sky.ts ***!
  \*******************************************/
/*! exports provided: Sky */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Sky", function() { return Sky; });
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _character_character__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../character/character */ "./src/app/view-port-3d/character/character.ts");
/* harmony import */ var _character_character_physics__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../character/character.physics */ "./src/app/view-port-3d/character/character.physics.ts");
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");




class Sky extends _ecs_component__WEBPACK_IMPORTED_MODULE_3__["ECSComponent"] {
    constructor(options) {
        super(options);
        this.settings = {
            HSL: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0.1, 1, 0.95),
            position: new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](-1, 1.75, 1),
            // positionScalar: 30,
            d: 200,
            far: 3500,
            bias: -0.0001,
        };
        this.lightGUI = options.gui.addFolder('Lights');
    }
    awake() {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m, _o, _p;
        /**
         * Hemisphere light
         */
        this.hemisphereLight = new three__WEBPACK_IMPORTED_MODULE_0__["HemisphereLight"](0xffffff, 0xffffff, 0.6);
        this.hemisphereLight.color.setHSL(0.6, 1, 0.6);
        this.hemisphereLight.groundColor.setHSL(0.095, 1, 0.75);
        this.hemisphereLight.position.set(0, 50, 0);
        this.hemisphereLightHelper = new three__WEBPACK_IMPORTED_MODULE_0__["HemisphereLightHelper"](this.hemisphereLight, 10);
        (_b = (_a = this.options) === null || _a === void 0 ? void 0 : _a.scene) === null || _b === void 0 ? void 0 : _b.add(this.hemisphereLight);
        (_d = (_c = this.options) === null || _c === void 0 ? void 0 : _c.scene) === null || _d === void 0 ? void 0 : _d.add(this.hemisphereLightHelper);
        /**
         * Directional light
         */
        this.directionalLight = new three__WEBPACK_IMPORTED_MODULE_0__["DirectionalLight"](0xffffff, 2);
        this.directionalLight.color.setHSL(this.settings.HSL.x, this.settings.HSL.y, this.settings.HSL.z);
        this.directionalLight.position.set(57, 58, 39);
        // this.directionalLight.position.multiplyScalar(
        //   this.settings.positionScalar,
        // )
        this.directionalLight.castShadow = true;
        this.directionalLight.shadow.mapSize.width = 2048;
        this.directionalLight.shadow.mapSize.height = 2048;
        this.directionalLight.shadow.camera.left = -this.settings.d;
        this.directionalLight.shadow.camera.right = this.settings.d;
        this.directionalLight.shadow.camera.top = this.settings.d;
        this.directionalLight.shadow.camera.bottom = -this.settings.d;
        this.directionalLight.shadow.camera.far = this.settings.far;
        this.directionalLight.shadow.bias = this.settings.bias;
        // this.directionalLightHelper = new DirectionalLightHelper(
        //   this.directionalLight,
        //   10,
        // )
        (_f = (_e = this.options) === null || _e === void 0 ? void 0 : _e.scene) === null || _f === void 0 ? void 0 : _f.add(this.directionalLight);
        // this.options?.scene?.add(this.directionalLightHelper)
        const position = this.lightGUI.addFolder('position');
        position.add((_g = this.directionalLight) === null || _g === void 0 ? void 0 : _g.position, 'x');
        position.add((_h = this.directionalLight) === null || _h === void 0 ? void 0 : _h.position, 'y');
        position.add((_j = this.directionalLight) === null || _j === void 0 ? void 0 : _j.position, 'z');
        /**
         * Sky dome
         */
        const vertexShader = `varying vec3 vWorldPosition;
        void main() {

            vec4 worldPosition = modelMatrix * vec4( position, 1.0 );
            vWorldPosition = worldPosition.xyz;

            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
        }
    `;
        const fragmentShader = `uniform vec3 topColor;
        uniform vec3 bottomColor;
        uniform float offset;
        uniform float exponent;
    
        varying vec3 vWorldPosition;
    
        void main() {
    
            float h = normalize( vWorldPosition + offset ).y;
            gl_FragColor = vec4( mix( bottomColor, topColor, max( pow( max( h , 0.0), exponent ), 0.0 ) ), 1.0 );
        }
    `;
        const uniforms = {
            topColor: { value: new three__WEBPACK_IMPORTED_MODULE_0__["Color"](0x0077ff) },
            bottomColor: { value: new three__WEBPACK_IMPORTED_MODULE_0__["Color"](0xffffff) },
            offset: { value: 33 },
            exponent: { value: 0.6 },
        };
        uniforms['topColor'].value.copy(this.hemisphereLight.color);
        (_m = (_l = (_k = this.options) === null || _k === void 0 ? void 0 : _k.scene) === null || _l === void 0 ? void 0 : _l.fog) === null || _m === void 0 ? void 0 : _m.color.copy(uniforms['bottomColor'].value);
        const skyGeo = new three__WEBPACK_IMPORTED_MODULE_0__["SphereGeometry"](4000, 32, 15);
        const skyMat = new three__WEBPACK_IMPORTED_MODULE_0__["ShaderMaterial"]({
            uniforms: uniforms,
            vertexShader: vertexShader,
            fragmentShader: fragmentShader,
            side: three__WEBPACK_IMPORTED_MODULE_0__["BackSide"],
        });
        const sky = new three__WEBPACK_IMPORTED_MODULE_0__["Mesh"](skyGeo, skyMat);
        (_p = (_o = this.options) === null || _o === void 0 ? void 0 : _o.scene) === null || _p === void 0 ? void 0 : _p.add(sky);
    }
    update(deltaTime) {
        var _a, _b, _c, _d, _e;
        const pos = (_e = (_d = (_c = (_b = (_a = this.entity) === null || _a === void 0 ? void 0 : _a.entityManger) === null || _b === void 0 ? void 0 : _b.getEntity(_character_character__WEBPACK_IMPORTED_MODULE_1__["Character"])) === null || _c === void 0 ? void 0 : _c.getComponent(_character_character_physics__WEBPACK_IMPORTED_MODULE_2__["CharacterPhysics"])) === null || _d === void 0 ? void 0 : _d.character) === null || _e === void 0 ? void 0 : _e.body.position;
        // this.directionalLight?.position.copy(toThreeVec3(pos!))
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/level/uv-map.ts":
/*!**********************************************!*\
  !*** ./src/app/view-port-3d/level/uv-map.ts ***!
  \**********************************************/
/*! exports provided: UV_MAP_BY_NAME */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UV_MAP_BY_NAME", function() { return UV_MAP_BY_NAME; });
const UV_MAP_BY_NAME = {
    /**
     * 5x2 Billboard
     */
    // done
    '5x2ads-billboard-lightmaps-1001': [
        [1, 0.7],
        [1, 0.3],
        [0, 0.7],
        [0, 0.3],
    ],
    // done
    '5x2ads-billboard-lightmaps-1002': [
        [1, 0.7],
        [1, 0.3],
        [0, 0.7],
        [0, 0.3],
    ],
    // done
    '5x2ads-billboard-lightmaps-1003': [
        [1, 0.7],
        [1, 0.3],
        [0, 0.7],
        [0, 0.3],
    ],
    // done
    '5x2ads-billboard-lightmaps-2001': [
        [1, 0.7],
        [0, 0.7],
        [1, 0.3],
        [0, 0.3],
    ],
    // done
    '5x2ads-billboard-lightmaps-2002': [
        [1, 0.7],
        [1, 0.3],
        [0, 0.7],
        [0, 0.3],
    ],
    // done
    '5x2ads-billboard-lightmaps-3001': [
        [1, 0.7],
        [0, 0.7],
        [1, 0.3],
        [0, 0.3],
    ],
    // done
    '5x2ads-billboard-lightmaps-3002': [
        [1, 0.7],
        [1, 0.3],
        [0, 0.7],
        [0, 0.3],
    ],
    // done
    '5x2ads-billboard-lightmaps-3003': [
        [1, 0.7],
        [1, 0.3],
        [0, 0.7],
        [0, 0.3],
    ],
    // done
    '5x2ads-billboard-lightmaps-3004': [
        [1, 0.7],
        [0, 0.7],
        [1, 0.3],
        [0, 0.3],
    ],
    // done
    '5x2ads-billboard-lightmaps-3005': [
        [1, 0.7],
        [0, 0.7],
        [1, 0.3],
        [0, 0.3],
    ],
    /**
     * 2x3 Bus
     */
    // done
    '2x3ads-bus-lightmaps-1001': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-1002': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-1003': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-1004': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-1005': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-1006': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-2002': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-2004': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-2005': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-2006': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-2007': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-2008': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-2009': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-2010': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-3001': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-3002': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-3003': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-3004': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    // done
    '2x3ads-bus-lightmaps-3005': [
        [0.7, 0.2],
        [0.3, 0.2],
        [0.7, 0.8],
        [0.3, 0.8],
    ],
    /**
     * 2x3 wall
     */
    // done
    '2x3ads-lightmaps-1018': [
        [0.3, 0.8],
        [0.7, 0.8],
        [0.3, 0.2],
        [0.7, 0.2],
    ],
    // done
    '2x3ads-lightmaps-1017': [
        [0.3, 0.8],
        [0.7, 0.8],
        [0.3, 0.2],
        [0.7, 0.2],
    ],
    // done
    '2x3ads-lightmaps-2017': [
        [0.3, 0.8],
        [0.7, 0.8],
        [0.3, 0.2],
        [0.7, 0.2],
    ],
    // done
    '2x3ads-lightmaps-2018': [
        [0.3, 0.8],
        [0.7, 0.8],
        [0.3, 0.2],
        [0.7, 0.2],
    ],
    // done
    '2x3ads-lightmaps-2019': [
        [0.3, 0.8],
        [0.7, 0.8],
        [0.3, 0.2],
        [0.7, 0.2],
    ],
    // done
    '2x3ads-lightmaps-3011': [
        [0.3, 0.8],
        [0.7, 0.8],
        [0.3, 0.2],
        [0.7, 0.2],
    ],
    /**
     * 5x3 wall
     */
    // done
    '5x3ads-lightmaps-1007': [
        [0, 0.8],
        [1, 0.8],
        [0, 0.2],
        [1, 0.2],
    ],
    // done
    '5x3ads-lightmaps-1008': [
        [0, 0.8],
        [1, 0.8],
        [0, 0.2],
        [1, 0.2],
    ],
    // done
    '5x3ads-lightmaps-1009': [
        [0, 0.8],
        [1, 0.8],
        [0, 0.2],
        [1, 0.2],
    ],
    // done
    '5x3ads-lightmaps-2007': [
        [0, 0.8],
        [1, 0.8],
        [0, 0.2],
        [1, 0.2],
    ],
    // done
    '5x3ads-lightmaps-2008': [
        [0, 0.8],
        [1, 0.8],
        [0, 0.2],
        [1, 0.2],
    ],
    // done
    '5x3ads-lightmaps-2009': [
        [0, 0.8],
        [1, 0.8],
        [0, 0.2],
        [1, 0.2],
    ],
    // done
    '5x3ads-lightmaps-3009': [
        [0, 0.8],
        [1, 0.8],
        [0, 0.2],
        [1, 0.2],
    ],
    // done
    '5x3ads-lightmaps-3010': [
        [0, 0.8],
        [1, 0.8],
        [0, 0.2],
        [1, 0.2],
    ],
    // done
    '5x3ads-lightmaps-3011': [
        [1, 0.8],
        [1, 0.2],
        [0, 0.8],
        [0, 0.2],
    ],
    /**
     * 5x2 wall
     */
    // done
    '5x2ads-lightmaps-1013': [
        [0, 0.7],
        [1, 0.7],
        [0, 0.3],
        [1, 0.3],
    ],
    // done
    '5x2ads-lightmaps-1014': [
        [0, 0.7],
        [1, 0.7],
        [0, 0.3],
        [1, 0.3],
    ],
    // done
    '5x2ads-lightmaps-1015': [
        [0, 0.7],
        [1, 0.7],
        [0, 0.3],
        [1, 0.3],
    ],
    // done
    '5x2ads-lightmaps-2013': [
        [0, 0.7],
        [1, 0.7],
        [0, 0.3],
        [1, 0.3],
    ],
    // done
    '5x2ads-lightmaps-2014': [
        [0, 0.7],
        [1, 0.7],
        [0, 0.3],
        [1, 0.3],
    ],
    // done
    '5x2ads-lightmaps-2015': [
        [0, 0.7],
        [1, 0.7],
        [0, 0.3],
        [1, 0.3],
    ],
    // done
    '5x2ads-lightmaps-3015': [
        [0, 0.7],
        [1, 0.7],
        [0, 0.3],
        [1, 0.3],
    ],
    // done
    '5x2ads-lightmaps-3016': [
        [0, 0.7],
        [1, 0.7],
        [0, 0.3],
        [1, 0.3],
    ],
    // done
    '5x2ads-lightmaps-3020': [
        [1, 0.7],
        [1, 0.3],
        [0, 0.7],
        [0, 0.3],
    ],
    /**
     * 5x2 Curved
     */
    '5x2ads-lightmaps-3018': [
        // top 2
        [0.36, 0.7],
        // bottom 2
        [0.36, 0.3],
        // top 3
        [0.42, 0.7],
        // top 0
        [0, 0.7],
        // bottom 0
        [0, 0.3],
        // bottom 3
        [0.42, 0.3],
        // top 4
        [0.48, 0.7],
        // bottom 4
        [0.48, 0.3],
        // top 5
        [0.54, 0.7],
        // bottom 5
        [0.54, 0.3],
        // top 6
        [0.6, 0.7],
        // bottom 6
        [0.6, 0.3],
        // top 7
        [0.66, 0.7],
        // bottom 7
        [0.66, 0.3],
        // top 8
        [0.72, 0.7],
        // bottom 8
        [0.72, 0.3],
        // top 9
        [1, 0.7],
        // bottom 9
        [1, 0.3],
    ],
    /**
     * 5x2 Curved
     */
    '5x3ads-cuved-lightmaps-3001': [
        // top 8
        [0.7, 0.8],
        // bottom 8
        [0.7, 0.2],
        // top 7
        [0.63, 0.8],
        // top 9
        [1, 0.8],
        // bottom 9
        [1, 0.2],
        // bottom 7
        [0.63, 0.2],
        // top 6
        [0.56, 0.8],
        // bottom 6
        [0.56, 0.2],
        // top 5
        [0.5, 0.8],
        // bottom 5
        [0.5, 0.2],
        // top 4
        [0.43, 0.8],
        // bottom 4
        [0.43, 0.2],
        // top 3
        [0.36, 0.8],
        // top 3
        [0.36, 0.2],
        // top 2
        [0.27, 0.8],
        // bottom 2
        [0.27, 0.2],
        // top 0
        [0, 0.8],
        // bottom 0
        [0, 0.2],
    ],
};


/***/ }),

/***/ "./src/app/view-port-3d/physics/conversion-utils.ts":
/*!**********************************************************!*\
  !*** ./src/app/view-port-3d/physics/conversion-utils.ts ***!
  \**********************************************************/
/*! exports provided: bodyToMesh, shapeToGeometry, sceneToWorld */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "bodyToMesh", function() { return bodyToMesh; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "shapeToGeometry", function() { return shapeToGeometry; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "sceneToWorld", function() { return sceneToWorld; });
/* harmony import */ var cannon_es__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! cannon-es */ "./node_modules/cannon-es/dist/cannon-es.js");
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils */ "./src/app/view-port-3d/utils.ts");



function bodyToMesh(body, material) {
    const group = new three__WEBPACK_IMPORTED_MODULE_1__["Group"]();
    group.position.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toThreeVec3"])(body.position));
    group.quaternion.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toThreeQuat"])(body.quaternion));
    const meshes = body.shapes.map((shape) => {
        const geometry = shapeToGeometry(shape);
        return new three__WEBPACK_IMPORTED_MODULE_1__["Mesh"](geometry, material);
    });
    meshes.forEach((mesh, i) => {
        const offset = body.shapeOffsets[i];
        const orientation = body.shapeOrientations[i];
        mesh.position.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toThreeVec3"])(offset));
        mesh.quaternion.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toThreeQuat"])(orientation));
        group.add(mesh);
    });
    return group;
}
function shapeToGeometry(shape) {
    switch (shape.type) {
        case cannon_es__WEBPACK_IMPORTED_MODULE_0__["Shape"].types.SPHERE: {
            const sphere = shape;
            return new three__WEBPACK_IMPORTED_MODULE_1__["SphereGeometry"](sphere.radius, 8, 8);
        }
        case cannon_es__WEBPACK_IMPORTED_MODULE_0__["Shape"].types.PARTICLE: {
            return new three__WEBPACK_IMPORTED_MODULE_1__["SphereGeometry"](0.1, 8, 8);
        }
        case cannon_es__WEBPACK_IMPORTED_MODULE_0__["Shape"].types.PLANE: {
            return new three__WEBPACK_IMPORTED_MODULE_1__["PlaneGeometry"](500, 500, 4, 4);
        }
        case cannon_es__WEBPACK_IMPORTED_MODULE_0__["Shape"].types.BOX: {
            const box = shape;
            return new three__WEBPACK_IMPORTED_MODULE_1__["BoxGeometry"](box.halfExtents.x * 2, box.halfExtents.y * 2, box.halfExtents.z * 2);
        }
        case cannon_es__WEBPACK_IMPORTED_MODULE_0__["Shape"].types.CYLINDER: {
            const cylinder = shape;
            cylinder;
            return new three__WEBPACK_IMPORTED_MODULE_1__["CylinderGeometry"](cylinder.radiusTop, cylinder.radiusBottom, cylinder.height, cylinder.numSegments);
        }
        default: {
            throw new Error(`Shape not recognized: "${shape.type}"`);
        }
    }
}
function sceneToWorld(scene, material, wireframeMaterial) {
    const body = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Body"]({
        position: new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Vec3"](),
        mass: 0,
        material,
        allowSleep: true,
    });
    const group = new three__WEBPACK_IMPORTED_MODULE_1__["Group"]();
    scene.traverse((mesh) => {
        if (mesh.name.match(/^cannono-box[0-9]{0,3}/i) &&
            mesh instanceof three__WEBPACK_IMPORTED_MODULE_1__["Mesh"]) {
            const size = mesh.scale.clone();
            size.multiplyScalar(0.5);
            const halfExtents = Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toCannonVec3"])(size);
            const shape = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Box"](halfExtents);
            const position = Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toCannonVec3"])(mesh.position);
            const quaternion = Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toCannonQuat"])(mesh.quaternion);
            mesh.material = wireframeMaterial;
            body.addShape(shape, position, quaternion);
            const geometry = new three__WEBPACK_IMPORTED_MODULE_1__["BoxBufferGeometry"](mesh.scale.x, mesh.scale.y, mesh.scale.z);
            const wireframe = new three__WEBPACK_IMPORTED_MODULE_1__["Mesh"](geometry, wireframeMaterial);
            wireframe.position.copy(mesh.position);
            wireframe.quaternion.copy(mesh.quaternion);
            wireframe.visible = false;
            group.add(wireframe);
        }
    });
    return {
        body,
        wireframe: group,
        isWireframeVisible: true,
    };
}


/***/ }),

/***/ "./src/app/view-port-3d/physics/physics.ts":
/*!*************************************************!*\
  !*** ./src/app/view-port-3d/physics/physics.ts ***!
  \*************************************************/
/*! exports provided: PHYSICS_FRAME_RATE, Physics */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PHYSICS_FRAME_RATE", function() { return PHYSICS_FRAME_RATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Physics", function() { return Physics; });
/* harmony import */ var cannon_es__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! cannon-es */ "./node_modules/cannon-es/dist/cannon-es.js");
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _utils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../utils */ "./src/app/view-port-3d/utils.ts");
/* harmony import */ var _ecs_entity__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../ecs/entity */ "./src/app/view-port-3d/ecs/entity.ts");




const PHYSICS_FRAME_RATE = 60;
class Physics extends _ecs_entity__WEBPACK_IMPORTED_MODULE_3__["ECSEntity"] {
    constructor(options) {
        super();
        this.physicsFrameTime = 1 / PHYSICS_FRAME_RATE;
        this.physicsMaxPrediction = PHYSICS_FRAME_RATE;
        this.objects = [];
        this.static = [];
        this.collidable = [];
        this.settings = {
            gravity: -9.81,
            // gravity: 0,
            isWireframeVisible: false,
        };
        this.callbacks = {
            togglePhysicsWireframe: (isVisible) => {
                debugger;
            },
        };
        /**
         * THREEJS Materials
         */
        this.defaultWireframeMaterial = new three__WEBPACK_IMPORTED_MODULE_1__["MeshPhongMaterial"]({
            color: new three__WEBPACK_IMPORTED_MODULE_1__["Color"](0xff0000),
        });
        this.wheelWireframeMaterial = new three__WEBPACK_IMPORTED_MODULE_1__["MeshPhongMaterial"]({
            color: new three__WEBPACK_IMPORTED_MODULE_1__["Color"](0x00ffff),
        });
        this.backWheelWireframeMaterial = new three__WEBPACK_IMPORTED_MODULE_1__["MeshPhongMaterial"]({
            color: new three__WEBPACK_IMPORTED_MODULE_1__["Color"](0xff00ff),
        });
        /**
         * CannonJS Materials
         */
        this.defaultMaterial = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Material"]('default');
        this.defaultContactMaterial = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["ContactMaterial"](this.defaultMaterial, this.defaultMaterial, {
            friction: 0.0,
            restitution: 0,
        });
        this.wheelMaterial = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["Material"]('wheelMaterial');
        this.wheelDefaultContactMaterial = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["ContactMaterial"](this.wheelMaterial, this.defaultMaterial, {
            friction: 0.0,
            restitution: 0,
            contactEquationStiffness: 1000,
        });
        this.options = options;
        this.world = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["World"]();
        this.physicsGui = this.options.gui.addFolder('Physics');
        this.physicsGui
            .add(this.world.gravity, 'y')
            .min(-10)
            .max(10)
            .name('gravity');
        this.physicsGui
            .add(this.settings, 'isWireframeVisible')
            .onChange((isWireframeVisible) => {
            if (isWireframeVisible) {
                this.enableWireframes();
            }
            else {
                this.disableWireframes();
            }
        });
        // this.carGui = this.options?.gui.addFolder('Car')
        // this.wheelGui = this.options?.gui.addFolder('Wheels')
    }
    enableWireframes() {
        this.collidable.forEach(({ wireframe, isWireframeVisible }) => {
            isWireframeVisible = true;
            wireframe.visible = true;
        });
    }
    disableWireframes() {
        this.collidable.forEach(({ wireframe, isWireframeVisible }) => {
            isWireframeVisible = false;
            wireframe.visible = false;
        });
    }
    awake() {
        super.awake();
        this.setWorldCharacteristics();
        /**
         * SET PLAYER
         */
    }
    update(deltaTime) {
        /**
         * Pre step
         */
        /**
         * Tick
         */
        this.world.step(1 / PHYSICS_FRAME_RATE, deltaTime, 3);
        /**
         * Post step
         */
        this.objects.forEach(({ body, wireframe }) => {
            if (wireframe) {
                wireframe.position.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toThreeVec3"])(body.position));
            }
        });
        this.collidable.forEach(({ body, wireframe }) => {
            wireframe.position.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toThreeVec3"])(body.position));
            wireframe.quaternion.copy(Object(_utils__WEBPACK_IMPORTED_MODULE_2__["toThreeQuat"])(body.quaternion));
        });
    }
    /**
     * @description
     * World settings
     */
    setWorldCharacteristics() {
        this.world.gravity.set(0, this.settings.gravity, 0);
        this.world.broadphase = new cannon_es__WEBPACK_IMPORTED_MODULE_0__["SAPBroadphase"](this.world);
        this.world.allowSleep = true;
        this.world.addContactMaterial(this.defaultContactMaterial);
        this.world.addContactMaterial(this.wheelDefaultContactMaterial);
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/player/player-controller.ts":
/*!**********************************************************!*\
  !*** ./src/app/view-port-3d/player/player-controller.ts ***!
  \**********************************************************/
/*! exports provided: PlayerController */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PlayerController", function() { return PlayerController; });
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");
/* harmony import */ var _input_keyboard_input__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../input/keyboard.input */ "./src/app/view-port-3d/input/keyboard.input.ts");


class PlayerController extends _ecs_component__WEBPACK_IMPORTED_MODULE_0__["ECSComponent"] {
    constructor(options, carController, characterController, cameraManager) {
        super(options);
        this.carController = null;
        this.characterController = null;
        this.isPlayerInVehicle = false;
        this.carController = carController;
        this.characterController = characterController;
        this.cameraManager = cameraManager;
    }
    awake() {
        var _a;
        this.input = (_a = this.entity) === null || _a === void 0 ? void 0 : _a.getComponent(_input_keyboard_input__WEBPACK_IMPORTED_MODULE_1__["KeyboardInput"]);
        this.player = this.entity;
        const input = this.input;
        input === null || input === void 0 ? void 0 : input.listenKeyUp((event) => {
            var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k, _l, _m;
            if (event.key === 'f') {
                this.isPlayerInVehicle = !this.isPlayerInVehicle;
                if (this.isPlayerInVehicle) {
                    (_a = this.characterController) === null || _a === void 0 ? void 0 : _a.hide();
                    (_b = this.carController) === null || _b === void 0 ? void 0 : _b.enable();
                    this.cameraManager.cameraTarget = (_e = (_d = (_c = this.carController) === null || _c === void 0 ? void 0 : _c.carPhysics) === null || _d === void 0 ? void 0 : _d.car) === null || _e === void 0 ? void 0 : _e.chassis;
                    this.cameraManager.cameraTargetType = 'car';
                }
                else {
                    const car = (_g = (_f = this.carController) === null || _f === void 0 ? void 0 : _f.carPhysics) === null || _g === void 0 ? void 0 : _g.car;
                    (_h = this.characterController) === null || _h === void 0 ? void 0 : _h.show((_j = car.chassis) === null || _j === void 0 ? void 0 : _j.body);
                    (_k = this.carController) === null || _k === void 0 ? void 0 : _k.disable();
                    this.cameraManager.cameraTarget = (_m = (_l = this.characterController) === null || _l === void 0 ? void 0 : _l.characterPhysics) === null || _m === void 0 ? void 0 : _m.character;
                    this.cameraManager.cameraTargetType = 'character';
                }
            }
        });
    }
    update(dt) { }
}


/***/ }),

/***/ "./src/app/view-port-3d/player/player.ts":
/*!***********************************************!*\
  !*** ./src/app/view-port-3d/player/player.ts ***!
  \***********************************************/
/*! exports provided: Player */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Player", function() { return Player; });
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var _ecs_entity__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../ecs/entity */ "./src/app/view-port-3d/ecs/entity.ts");
/* harmony import */ var _input_keyboard_input__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../input/keyboard.input */ "./src/app/view-port-3d/input/keyboard.input.ts");



class Player extends _ecs_entity__WEBPACK_IMPORTED_MODULE_1__["ECSEntity"] {
    constructor() {
        super(...arguments);
        this.acceleration = new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](0, 0, 0);
        this.velocity = new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"]();
        this.position = new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"]();
        this.isPlayerInVehicle = false;
    }
    awake() {
        super.awake();
        const input = this.getComponent(_input_keyboard_input__WEBPACK_IMPORTED_MODULE_2__["KeyboardInput"]);
    }
    update(dt) {
        super.update(dt);
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/tools/stats-tool.ts":
/*!**************************************************!*\
  !*** ./src/app/view-port-3d/tools/stats-tool.ts ***!
  \**************************************************/
/*! exports provided: StatsTool */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatsTool", function() { return StatsTool; });
/* harmony import */ var _ecs_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../ecs/component */ "./src/app/view-port-3d/ecs/component.ts");
/* harmony import */ var stats_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! stats.js */ "./node_modules/stats.js/build/stats.min.js");
/* harmony import */ var stats_js__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(stats_js__WEBPACK_IMPORTED_MODULE_1__);


class StatsTool extends _ecs_component__WEBPACK_IMPORTED_MODULE_0__["ECSComponent"] {
    constructor(elSelector) {
        super();
        this.stats = new stats_js__WEBPACK_IMPORTED_MODULE_1__();
        const container = document.querySelector(elSelector);
        container === null || container === void 0 ? void 0 : container.appendChild(this.stats.dom);
    }
    awake() { }
    update(deltaTime) {
        this.stats.end();
        this.stats.begin();
    }
}


/***/ }),

/***/ "./src/app/view-port-3d/utils.ts":
/*!***************************************!*\
  !*** ./src/app/view-port-3d/utils.ts ***!
  \***************************************/
/*! exports provided: toCannonVec3, toThreeVec3, toThreeQuat, toCannonQuat */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toCannonVec3", function() { return toCannonVec3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toThreeVec3", function() { return toThreeVec3; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toThreeQuat", function() { return toThreeQuat; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "toCannonQuat", function() { return toCannonQuat; });
/* harmony import */ var three__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! three */ "./node_modules/three/build/three.module.js");
/* harmony import */ var cannon_es__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! cannon-es */ "./node_modules/cannon-es/dist/cannon-es.js");


function toCannonVec3(v) {
    return new cannon_es__WEBPACK_IMPORTED_MODULE_1__["Vec3"](v.x, v.y, v.z);
}
function toThreeVec3(v) {
    return new three__WEBPACK_IMPORTED_MODULE_0__["Vector3"](v.x, v.y, v.z);
}
function toThreeQuat(quat) {
    return new three__WEBPACK_IMPORTED_MODULE_0__["Quaternion"](quat.x, quat.y, quat.z, quat.w);
}
function toCannonQuat(quat) {
    return new cannon_es__WEBPACK_IMPORTED_MODULE_1__["Quaternion"](quat.x, quat.y, quat.z, quat.w);
}


/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/__ivy_ngcc__/fesm2015/platform-browser.js");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/vo1/ghq/gitlab.com-smk/vosamoilenko/thesis/src/main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map