import { PLAYER_STATES } from '.'
import { FiniteState } from '../../fsm/finite.state'
import { FiniteStateMachine } from '../../fsm/fsm'
import { KeyboardInput } from '../../input/keyboard.input'
import { CharacterFSM } from '../character.fsm'

export class IdleState extends FiniteState {
  name = PLAYER_STATES.IDLE
  input?: KeyboardInput

  constructor(parent: FiniteStateMachine) {
    super(parent)

    const fsm = this.fsm as CharacterFSM
    this.input = fsm.character.getComponent(KeyboardInput.name)
  }

  enter(prevState?: FiniteState): void {
    const fsm = this.fsm as CharacterFSM
    const { animations } = fsm.character
    const state = animations![this.name]
    const idleAction = state.action
    
    if (!idleAction) {
      return
    }
    
    if (prevState) {
      const prevAction = animations![prevState?.name!].action
      idleAction.time = 0.0
      idleAction.enabled = true
      idleAction.setEffectiveTimeScale(1.0)
      idleAction.setEffectiveWeight(1.0)
      idleAction.crossFadeFrom(prevAction!, 0.25, true)
      idleAction.play()
    } else {
      idleAction.play()
    }
  }

  exit(): void {}

  update(_timeElapsed: number): void {
    const fsm = this.fsm as CharacterFSM

    if (this.input?.keys.forward || this.input?.keys.backward) {
      fsm.setState(PLAYER_STATES.WALK)
    }
  }
}
