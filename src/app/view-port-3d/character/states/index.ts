export enum PLAYER_STATES {
  WALK = 'WALK',
  RUN = 'RUN',
  IDLE = 'IDLE',
}
