import { PLAYER_STATES } from '.'
import { FiniteState } from '../../fsm/finite.state'
import { FiniteStateMachine } from '../../fsm/fsm'
import { KeyboardInput } from '../../input/keyboard.input'
import { CharacterFSM } from '../character.fsm'

export class WalkState extends FiniteState {
  name = PLAYER_STATES.WALK
  input?: KeyboardInput

  constructor(parent: FiniteStateMachine) {
    super(parent)

    const fsm = this.fsm as CharacterFSM
    this.input = fsm.character.getComponent(KeyboardInput.name)
  }

  enter(prevState?: FiniteState): void {
    const fsm = this.fsm as CharacterFSM
    const { animations } = fsm.character
    const curAction = animations![this.name].action

    if (prevState) {
      const prevAction = animations![prevState.name!].action

      curAction!.enabled = true

      if (prevState.name == PLAYER_STATES.RUN) {
        const ratio =
          curAction?.getClip().duration! /
          prevAction?.getClip().duration!
        curAction!.time = prevAction!.time * ratio
        curAction!.crossFadeFrom(prevAction!, 0.4, true)
      } else {
        curAction!.time = 0.0
        curAction!.setEffectiveTimeScale(1.0)
        curAction!.setEffectiveWeight(1)
        curAction!.crossFadeFrom(prevAction!, 0.1, true)
      }

      curAction!.play()
    } else {
      curAction!.play()
    }
  }

  exit(): void {}

  update(_time: number): void {
    const fsm = this.fsm as CharacterFSM

    if (this.input?.keys.forward || this.input?.keys.backward) {
      if (this.input?.keys.shift) {
        fsm.setState(PLAYER_STATES.RUN)
      }
      return
    }

    fsm.setState(PLAYER_STATES.IDLE)
  }
}
