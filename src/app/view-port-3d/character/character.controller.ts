import { ECSComponent } from '../ecs/component'
import { KeyboardInput } from '../input/keyboard.input'
import { Character } from './character'
import { Body } from 'cannon-es'
import { CharacterPhysics } from './character.physics'
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader.js'
import {
  AnimationMixer,
  Bone,
  Group,
  LoadingManager,
  Mesh,
  MeshStandardMaterial,
  Quaternion,
  sRGBEncoding,
  Vector3,
} from 'three'
import { PLAYER_STATES } from './states'
import {
  toCannonQuat,
  toCannonVec3,
  toThreeQuat,
  toThreeVec3,
} from '../utils'
import { CameraManager } from '../camera/camera-manager'
import { Params } from '../ecs/types'
import { Vec3 } from 'math/Vec3'
import { take } from 'rxjs/operators'
import { combineLatest } from 'rxjs'

export class CharacterController extends ECSComponent {
  input?: KeyboardInput
  characterPhysics?: CharacterPhysics
  isHidden: boolean = false
  model?: Group
  bones: Bone[] = []
  mixer?: AnimationMixer
  cameraManager: CameraManager

  constructor(options: Params, cameraManager: CameraManager) {
    super(options)
    this.cameraManager = cameraManager

    combineLatest([
      this.options?.assetManager?.assets.characterVisual,
      this.options?.assetManager?.assets.characterAnimation,
    ])
      .pipe(take(1))
      // @ts-ignore
      .subscribe(([character, _animations]) => {
        const animations = (_animations as unknown) as Map<
          string,
          Group
        >
        this.model = (character as unknown) as Group
        // this.model.visible = false
        this.model.scale.setScalar(0.008)
        this.model.updateWorldMatrix(false, true)
        this.model.position.y = 0.05
        this.options?.scene?.add(this.model)

        // @ts-ignore
        const bones = this.model.children[1].skeleton.bones
        for (let b of bones) {
          this.bones[b.name] = b
        }

        this.model.traverse((c) => {
          c.castShadow = true
          c.receiveShadow = true
          const mesh = c as Mesh
          const material = mesh.material as MeshStandardMaterial
          if (material && material.map) {
            material.map.encoding = sRGBEncoding
          }
        })

        this.mixer = new AnimationMixer(this.model)

        for (const [name, animation] of animations) {
          const clip = animation.animations[0]
          const action = this.mixer?.clipAction(clip)

          this.parent.animations[name] = {
            clip: clip,
            action: action,
          }
        }

        this.parent.fsm?.setState(PLAYER_STATES.IDLE)
      })
  }

  private get parent(): Character {
    return this.entity as Character
  }

  public update(dt: number): void {
    if (this.isHidden) {
      return
    }

    if (this.mixer) {
      this.mixer.update(dt)
    }

    this.cameraManager.orbitControls?.target.copy(this.model?.position!)

  }

  public hide(): void {
    this.model!.visible = false
    // this.characterPhysics?.removeCharacter()
    this.isHidden = true
  }

  public show(car?: Body): void {
    // place left of car
    this.model!.visible = true
    if (car) {
      const distance = new Vector3(2, 0, 0)

      const carPosition = toThreeVec3(car!.position.clone())
      const carQuaternion = toThreeQuat(car.quaternion.clone())

      distance.applyQuaternion(
        new Quaternion().setFromAxisAngle(
          new Vector3(0, 1, 0),
          Math.PI * -0.5,
        ),
      )
      distance.applyQuaternion(carQuaternion)
      distance.add(carPosition)

      // this.characterPhysics?.character?.body.position.copy(
      // toCannonVec3(distance),
      // )

      // this.characterPhysics?.addCharacter(distance)
    } else {
      // this.characterPhysics?.addCharacter()
    }

    this.isHidden = false
  }
}
