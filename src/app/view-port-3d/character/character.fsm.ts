import { FiniteStateMachine } from '../fsm/fsm'
import { Character } from './character'
import { PLAYER_STATES } from './states'
import { IdleState } from './states/idle-state'
import { RunState } from './states/run-state'
import { WalkState } from './states/walk-state'

export class CharacterFSM extends FiniteStateMachine {
  character: Character
  constructor(character: Character) {
    super()

    this.character = character
    this.init()
  }

  public init(): void {
    this.addState(PLAYER_STATES.IDLE, IdleState)
    this.addState(PLAYER_STATES.WALK, WalkState)
    this.addState(PLAYER_STATES.RUN, RunState)
  }

  update(dt: number) {
    super.update(dt)
  }
}
