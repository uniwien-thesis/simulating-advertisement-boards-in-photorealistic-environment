import { ECSEntity } from '../ecs/entity'
import { AnimationAction, AnimationClip, Group } from 'three'
import { CharacterFSM } from './character.fsm'
import { CharacterController } from './character.controller'
import { CharacterPhysics } from './character.physics'
import { Body } from 'cannon-es'
import {
  setCannonQuatToThreeQuat,
  setCannonVecToThreeVec,
} from '../utils'

export interface AnimationMap {
  [key: string]: Animation
}

export interface Animation {
  clip: AnimationClip
  action?: AnimationAction
}

export class Character extends ECSEntity {
  animations: AnimationMap = {}
  fsm?: CharacterFSM

  constructor() {
    super()
    this.fsm = new CharacterFSM(this)
  }

  public awake(): void {
    super.awake()
  }

  public get getCharacterModel(): Group | undefined {
    const characterController = this.getComponent(
      CharacterController.name,
    ) as CharacterController
    return characterController.model
  }
  public get getCharacterBody(): Body | undefined {
    const characterPhysics = this.getComponent(
      CharacterPhysics.name,
    ) as CharacterPhysics

    return characterPhysics.character
  }

  public update(dt: number): void {
    super.update(dt)

    this.fsm?.update(dt)

    setCannonQuatToThreeQuat(
      this.getCharacterBody?.quaternion,
      this.getCharacterModel?.quaternion,
    )
    setCannonVecToThreeVec(
      this.getCharacterBody?.position,
      this.getCharacterModel?.position,
    )
    this.getCharacterModel && (this.getCharacterModel.position.y = 0.1)
  }
}
