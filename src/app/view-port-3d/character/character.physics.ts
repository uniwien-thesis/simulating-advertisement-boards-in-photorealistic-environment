import {
  Body,
  Material,
  Vec3,
  Quaternion as CannonQuat,
} from 'cannon-es'
import { Quaternion, Vector3 } from 'three'
import { ECSComponent } from '../ecs/component'
import { Params } from '../ecs/types'
import { createCharacter } from '../physics/create-character'
import { toCannonQuat, toCannonVec3, toThreeQuat } from '../utils'

export class CharacterPhysics extends ECSComponent {
  character: Body

  public acceleration = new Vector3(0.1, 0, 40.0)
  public rotation = 0.125
  public velocity = new Vector3(0, 0.3, 0)
  public yAxis = new Vec3(0, 1, 0)

  constructor(options: Params, defaultMaterial: Material) {
    super(options)
    this.character = createCharacter(defaultMaterial)
  }

  public update(dt: number): void {
    const input = this.options?.keyboardInput?.vec3Input
    const resultQuaternion = new CannonQuat()
    const currentQuaternion = toThreeQuat(
      this.character!.quaternion.clone(),
    )

    // on shift speed up
    let scaleFactor = 1
    if (this.options?.keyboardInput?.keys.shift) {
      scaleFactor = 2
    }

    // translate
    this.velocity.set(0, 0, 0)
    this.velocity.z +=
      dt * 10 * input!.z * scaleFactor * this.acceleration.z

    // rotate
    resultQuaternion.setFromAxisAngle(
      this.yAxis,
      4.0 * Math.PI * -input!.x * dt * this.rotation,
    )
    currentQuaternion!.multiply(toThreeQuat(resultQuaternion))

    this.velocity.applyQuaternion(currentQuaternion)
    this.character.quaternion.copy(toCannonQuat(currentQuaternion))
    this.character.velocity.copy(toCannonVec3(this.velocity))
  }
}
