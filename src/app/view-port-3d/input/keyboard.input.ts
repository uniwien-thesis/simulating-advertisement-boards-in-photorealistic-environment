import { Vec3 } from 'cannon-es'
import { Vector3 } from 'three'
import { ECSComponent } from '../ecs/component'

export class KeyboardInput extends ECSComponent {
  constructor() {
    super()

    this.listenKeyDown((event) => this.onKeyDown(event))
    this.listenKeyUp((event) => this.onKeyUp(event))
  }

  public keys = {
    forward: false,
    backward: false,
    left: false,
    right: false,
    space: false,
    shift: false,
    digit_1: false,
    digit_2: false,
    digit_3: false,
    digit_4: false,
    digit_0: false,
  }

  public awake(): void {}

  public listenKeyUp(callback: (event: KeyboardEvent) => void): void {
    document.addEventListener('keyup', (e) => callback(e), false)
  }

  get vector3Input(): Vector3 {
    const inputVector = new Vector3()

    inputVector.set(
      this.keys.right ? 1 : this.keys.left ? -1 : 0,
      0,
      this.keys.forward ? 1 : this.keys.backward ? -1 : 0,
    )

    return inputVector
  }

  get vec3Input(): Vec3 {
    const inputVector = new Vec3()

    inputVector.set(
      this.keys.right ? 1 : this.keys.left ? -1 : 0,
      0,
      this.keys.forward ? 1 : this.keys.backward ? -1 : 0,
    )

    return inputVector
  }

  public listenKeyDown(
    callback: (event: KeyboardEvent) => void,
  ): void {
    document.addEventListener('keydown', (e) => callback(e), false)
  }

  private onKeyUp(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 87: // w
        this.keys.forward = false
        break
      case 65: // a
        this.keys.left = false
        break
      case 83: // s
        this.keys.backward = false
        break
      case 68: // d
        this.keys.right = false
        break
      case 32: // SPACE
        this.keys.space = false
        break
      case 16: // SHIFT
        this.keys.shift = false
        break
      case 49:
        this.keys.digit_1 = false
        break
      case 50:
        this.keys.digit_2 = false
        break
      case 51:
        this.keys.digit_3 = false
        break
      case 52:
        this.keys.digit_4 = false
        break
      case 48:
        this.keys.digit_0 = false
        break
    }
  }

  private onKeyDown(event: KeyboardEvent) {
    switch (event.keyCode) {
      case 87: // w
        this.keys.forward = true
        break
      case 65: // a
        this.keys.left = true
        break
      case 83: // s
        this.keys.backward = true
        break
      case 68: // d
        this.keys.right = true
        break
      case 32: // SPACE
        this.keys.space = true
        break
      case 16: // SHIFT
        this.keys.shift = true
        break
      case 49:
        this.keys.digit_1 = true
        break
      case 50:
        this.keys.digit_2 = true
        break
      case 51:
        this.keys.digit_3 = true
        break
      case 52:
        this.keys.digit_4 = true
        break
      case 48:
        this.keys.digit_0 = true
        break
    }
  }

  public update(deltaTime: number): void {}
}
