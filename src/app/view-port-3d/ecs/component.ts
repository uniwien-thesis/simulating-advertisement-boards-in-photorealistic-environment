import { ECSEntity } from './entity'
import { ECSAwake, ECSUpdate, Params } from './types'

export abstract class ECSComponent implements ECSUpdate, ECSAwake {
  public entity?: ECSEntity
  public options?: Params
  isAwake = false

  constructor(options?: Params) {
    this.options = options
  }

  public awake(): void {
    this.isAwake = false
  }

  public abstract update(deltaTime: number): void
}
