import { GUI } from 'dat.gui'
import { Camera, Scene } from 'three'
import { AssetManager } from '../asset-manager'
import { CameraManager } from '../camera/camera-manager'
import { KeyboardInput } from '../input/keyboard.input'

export interface Params {
  scene?: Scene
  cameraManager?: CameraManager
  gui?: GUI
  assetManager?: AssetManager
  keyboardInput?: KeyboardInput
}

export interface ECSUpdate {
  update(deltaTime: number): void
}

export interface ECSAwake {
  isAwake: boolean
  awake(): void
}
