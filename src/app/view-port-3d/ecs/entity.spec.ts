import { ECSComponent } from './component'
import { ECSEntity } from './entity'

class Entity extends ECSEntity {}
class Component1 implements ECSComponent {
  entity = new Entity()
  update(dt: number): void {}
  awake(): void {}
}
class Component2 implements ECSComponent {
  entity = new Entity()
  update(dt: number): void {}
  awake(): void {}
}
class Component3 implements ECSComponent {
  entity = new Entity()
  update(dt: number): void {}
  awake(): void {}
}

describe('[BASE] Entity', () => {
  let e: Entity
  const c1 = new Component1()
  const c2 = new Component2()
  const c3 = new Component3()

  beforeEach(() => {
    e = new Entity()
  })

  it('should add components', () => {
    expect(e.components.size).toBe(0)
    e.addComponent(c1)
    e.addComponent(c2)
    e.addComponent(c3)

    expect(e.components.size).toBe(3)

    expect(e.components.get(c1.constructor.name)).toBe(c1)
    expect(e.components.get(c2.constructor.name)).toBe(c2)
    expect(e.components.get(c3.constructor.name)).toBe(c3)
  })

  it('should remove components', () => {
    e.addComponent(c1)
    e.addComponent(c2)
    e.addComponent(c3)

    expect(e.components.size).toBe(3)

    e.removeComponent(Component2.name)

    expect(e.components.size).toBe(2)
    expect(e.components.get(c1.constructor.name)).toBe(c1)
    expect(e.components.get(c3.constructor.name)).toBe(c3)
  })

  it('should add, remove, get, and check components', () => {
    e.addComponent(c1)
    e.addComponent(c2)
    e.addComponent(c3)

    expect(e.hasComponent(Component1.name)).toBeTruthy()
    expect(e.hasComponent(Component3.name)).toBeTruthy()
  })

  it('should get undefined', () => {
    expect(e.getComponent(Component1.name)).toBeUndefined()
    expect(e.hasComponent(Component1.name)).toBeFalsy()
  })

  it('should update all components', () => {
    const spy1 = spyOn(c1, 'update')
    const spy2 = spyOn(c2, 'update')
    const spy3 = spyOn(c3, 'update')

    expect(spy1).not.toHaveBeenCalled()
    expect(spy2).not.toHaveBeenCalled()
    expect(spy3).not.toHaveBeenCalled()

    e.addComponent(c1)
    e.addComponent(c2)
    e.addComponent(c3)

    const deltaTime = 12
    e.update(deltaTime)

    expect(spy1).toHaveBeenCalledWith(deltaTime)
    expect(spy2).toHaveBeenCalledWith(deltaTime)
    expect(spy3).toHaveBeenCalledWith(deltaTime)
  })

  it('should awake all components', () => {
    const spy1 = spyOn(c1, 'awake')
    const spy2 = spyOn(c2, 'awake')
    const spy3 = spyOn(c3, 'awake')

    expect(spy1).not.toHaveBeenCalled()
    expect(spy2).not.toHaveBeenCalled()
    expect(spy3).not.toHaveBeenCalled()

    e.addComponent(c1)
    e.addComponent(c2)
    e.addComponent(c3)

    e.awake()

    expect(spy1).toHaveBeenCalled()
    expect(spy2).toHaveBeenCalled()
    expect(spy3).toHaveBeenCalled()
  })

  it('should awake components in same order is adding them', () => {
    const spy1 = spyOn(c1, 'awake')
    const spy2 = spyOn(c2, 'awake')
    const spy3 = spyOn(c3, 'awake')

    e.addComponent(c3)
    e.addComponent(c1)
    e.addComponent(c2)

    e.awake()

    expect(spy3).toHaveBeenCalledBefore(spy1)
    expect(spy1).toHaveBeenCalledBefore(spy2)
  })

  it('should update components in same order is adding them', () => {
    const spy1 = spyOn(c1, 'update')
    const spy2 = spyOn(c2, 'update')
    const spy3 = spyOn(c3, 'update')

    e.addComponent(c3)
    e.addComponent(c1)
    e.addComponent(c2)

    e.update(12)

    expect(spy3).toHaveBeenCalledBefore(spy1)
    expect(spy1).toHaveBeenCalledBefore(spy2)
  })
})
