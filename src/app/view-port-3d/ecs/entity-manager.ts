import { ECSEntity } from './entity'
import { ECSAwake, ECSUpdate } from './types'

export class ECSEntityManager implements ECSUpdate, ECSAwake {
  isAwake = false
  entities = new Map<string, ECSEntity>()

  update(dt: number): void {
    for (const [_, entity] of this.entities) {
      entity.update(dt)
    }
  }

  awake(): void {
    for (const [_, entity] of this.entities) {
      entity.awake()
    }
    this.isAwake = true
  }

  addEntity(entity: ECSEntity, key?: string): void {
    const entityKey = key ?? entity.constructor.name
    entity.entityManger = this

    this.entities.set(entityKey, entity)
  }

  getEntity<T extends ECSEntity>(key: string): T | undefined {
    return this.entities.get(key) as T
  }

  public removeEntity(key: string): void {
    this.entities.delete(key)
  }

  public hasEntity(key: string): boolean {
    return this.entities.has(key)
  }
}
