import { ECSComponent } from './component'
import { ECSEntityManager } from './entity-manager'
import { ECSAwake, ECSUpdate } from './types'

export abstract class ECSEntity implements ECSUpdate, ECSAwake {
  protected _components = new Map<string, ECSComponent>()
  entityManger?: ECSEntityManager
  isAwake = false

  public get components(): Map<string, ECSComponent> {
    return this._components
  }

  public addComponent(component: ECSComponent, key?: string): void {
    const componentKey = key ?? component.constructor.name
    component.entity = this

    this._components.set(componentKey, component)
  }

  public getComponent<T extends ECSComponent>(
    key: string,
  ): T | undefined {
    return this._components.get(key) as T
  }

  public removeComponent(key: string): boolean {
    console.log(key.toString())
    console.log(JSON.stringify(this._components))
    return this._components.delete(key)
  }

  public hasComponent(key: string): boolean {
    return this._components.has(key)
  }

  public update(deltaTime: number): void {
    this._components.forEach((component) =>
      component.update(deltaTime),
    )
  }

  public awake(): void {
    this.isAwake = true
    this._components.forEach((component) => component.awake())
  }
}
