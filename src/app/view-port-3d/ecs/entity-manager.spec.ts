import { ECSEntity } from './entity'
import { ECSEntityManager } from './entity-manager'

class Entity1 extends ECSEntity {}
class Entity2 extends ECSEntity {}
class Entity3 extends ECSEntity {}

describe('[BASE] Entity Manager', () => {
  let manager: ECSEntityManager
  const e1 = new Entity1()
  const e2 = new Entity2()
  const e3 = new Entity3()

  beforeEach(() => {
    manager = new ECSEntityManager()
  })

  it('should add entity', () => {
    expect(manager.entities.size).toBe(0)
    manager.addEntity(e1)
    manager.addEntity(e2)
    manager.addEntity(e3)

    expect(manager.entities.size).toBe(3)

    expect(manager.getEntity(e1.constructor.name)).toBe(e1)
    expect(manager.getEntity(e2.constructor.name)).toBe(e2)
    expect(manager.getEntity(e3.constructor.name)).toBe(e3)
  })

  it('should remove entities', () => {
    manager.addEntity(e1)
    manager.addEntity(e2)
    manager.addEntity(e3)

    expect(manager.entities.size).toBe(3)

    manager.removeEntity(Entity2.name)

    expect(manager.entities.size).toBe(2)
    expect(manager.entities.get(e1.constructor.name)).toBe(e1)
    expect(manager.entities.get(e3.constructor.name)).toBe(e3)
  })

  it('should add, remove, get, and check entities', () => {
    manager.addEntity(e1)
    manager.addEntity(e2)
    manager.addEntity(e3)

    expect(manager.hasEntity(Entity1.name)).toBeTruthy()
    expect(manager.hasEntity(Entity3.name)).toBeTruthy()
  })

  it('should get undefined', () => {
    expect(manager.getEntity(Entity1.name)).toBeUndefined()
    expect(manager.hasEntity(Entity1.name)).toBeFalsy()
  })

  it('should update all entities', () => {
    const spy1 = spyOn(e1, 'update')
    const spy2 = spyOn(e2, 'update')
    const spy3 = spyOn(e3, 'update')

    expect(spy1).not.toHaveBeenCalled()
    expect(spy2).not.toHaveBeenCalled()
    expect(spy3).not.toHaveBeenCalled()

    manager.addEntity(e1)
    manager.addEntity(e2)
    manager.addEntity(e3)

    const deltaTime = 12
    manager.update(deltaTime)

    expect(spy1).toHaveBeenCalledWith(deltaTime)
    expect(spy2).toHaveBeenCalledWith(deltaTime)
    expect(spy3).toHaveBeenCalledWith(deltaTime)
  })

  it('should awake all entities', () => {
    const spy1 = spyOn(e1, 'awake')
    const spy2 = spyOn(e2, 'awake')
    const spy3 = spyOn(e3, 'awake')

    expect(spy1).not.toHaveBeenCalled()
    expect(spy2).not.toHaveBeenCalled()
    expect(spy3).not.toHaveBeenCalled()

    manager.addEntity(e1)
    manager.addEntity(e2)
    manager.addEntity(e3)

    manager.awake()

    expect(spy1).toHaveBeenCalled()
    expect(spy2).toHaveBeenCalled()
    expect(spy3).toHaveBeenCalled()
  })


  it('should awake entities in same order as adding them', () => {
    const spy1 = spyOn(e1, 'awake')
    const spy2 = spyOn(e2, 'awake')
    const spy3 = spyOn(e3, 'awake')

    manager.addEntity(e3)
    manager.addEntity(e1)
    manager.addEntity(e2)

    manager.awake()

    expect(spy3).toHaveBeenCalledBefore(spy1)
    expect(spy1).toHaveBeenCalledBefore(spy2)
  })

  it('should update entities in same order as adding them', () => {
    const spy1 = spyOn(e1, 'update')
    const spy2 = spyOn(e2, 'update')
    const spy3 = spyOn(e3, 'update')

    manager.addEntity(e3)
    manager.addEntity(e1)
    manager.addEntity(e2)

    manager.update(12)

    expect(spy3).toHaveBeenCalledBefore(spy1)
    expect(spy1).toHaveBeenCalledBefore(spy2)
  })
})
