import { GUI } from 'dat.gui'
import {
  ACESFilmicToneMapping,
  Clock,
  Color,
  FogExp2,
  PCFSoftShadowMap,
  PerspectiveCamera,
  Scene,
  Texture,
  VideoTexture,
  WebGLRenderer,
  sRGBEncoding,
} from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'

import { AdsController } from './level/ads-controller'
import { AssetManager } from './asset-manager'
import { CameraManager } from './camera/camera-manager'
import { Car } from './car/car'
import { CarController } from './car/car.controller'
import { Character } from './character/character'
import { CharacterController } from './character/character.controller'
import { ECSEntity } from './ecs/entity'
import { ECSEntityManager } from './ecs/entity-manager'
import { Intro } from './intro/intro'
import { KeyboardInput } from './input/keyboard.input'
import { Level } from './level/level'
import { LevelController } from './level/level-controller'
import { PhysicsSystem } from './physics/physics-system'
import { Sky } from './level/sky'
import { StatsTool } from './tools/stats-tool'
import { CarPhysics } from './car/car.physics'
import { CharacterPhysics } from './character/character.physics'

export interface RequiredTextures {
  combinedTexturesMap?: { [key: string]: Texture }
  adsInitialTexturesMap?: { [key: string]: Texture }
  videoTextures?: { [key: string]: VideoTexture }
  lightmapTexture?: Texture
  introTexture?: Texture
}

export class Application extends ECSEntity {
  private controls?: OrbitControls
  private gui = new GUI()
  private renderer?: WebGLRenderer
  private scene: Scene = new Scene()

  public physicsWorker?: Worker

  private keyboardInput: KeyboardInput
  private cameraManager?: CameraManager
  private entityManager = new ECSEntityManager()
  public assetManager?: AssetManager

  private previousTick = 0

  clock = new Clock()
  constructor() {
    super()

    this.keyboardInput = new KeyboardInput()
  }

  public awake(): void {
    this.setThreeJs()
    this.addComponent(new StatsTool('#stats'))

    /**
     * Set camera entity
     */
    this.cameraManager = new CameraManager(
      this.renderer!,
      this.keyboardInput,
      this.gui,
    )
    this.entityManager.addEntity(this.cameraManager)
    this.scene.add(this.cameraManager.camera)

    /**
     * Physics
     */
    const physicsSystem = new PhysicsSystem({
      assetManager: this.assetManager!,
      keyboardInput: this.keyboardInput!,
      scene: this.scene!,
      cameraManager: this.cameraManager!,
    })
    this.entityManager.addEntity(physicsSystem)

    /**
     * Set intro view
     */
    const intro = new Intro(
      this.scene,
      this.cameraManager,
      this.assetManager!,
    )
    this.entityManager.addEntity(intro)

    /**
     * Set level entity
     */
    const level = new Level()
    this.entityManager.addEntity(level)

    /**
     * Set component responsible for ADS interaction
     */
    const adsController = new AdsController({
      scene: this.scene,
      gui: this.gui,
      cameraManager: this.cameraManager,
      assetManager: this.assetManager,
    })
    level.addComponent(adsController)

    /**
     * Set level visual
     */
    const levelController = new LevelController({
      scene: this.scene,
      gui: this.gui,
      assetManager: this.assetManager!,
    })
    level.addComponent(levelController)

    /**
     * Set sky
     */
    const sky = new Sky({
      scene: this.scene,
      gui: this.gui,
    })
    level.addComponent(sky)

    /**
     * Character
     */
    const character = new Character()
    this.entityManager.addEntity(character)

    character.addComponent(this.keyboardInput)
    const characterController = new CharacterController(
      {
        scene: this.scene,
        gui: this.gui,
        assetManager: this.assetManager,
      },
      this.cameraManager,
    )
    character.addComponent(characterController)
    const characterPhysics = new CharacterPhysics(
      { keyboardInput: this.keyboardInput },
      physicsSystem.defaultMaterial,
    )
    character.addComponent(characterPhysics)

    /**
     * Car
     */
    const car = new Car()
    this.entityManager.addEntity(car)
    const carController = new CarController({
      scene: this.scene,
      gui: this.gui,
      assetManager: this.assetManager,
    })
    car.addComponent(carController)
    const carPhysics = new CarPhysics(
      {
        keyboardInput: this.keyboardInput,
      },
      physicsSystem.wheelMaterial,
    )
    car.addComponent(carPhysics)
    physicsSystem.world?.addEventListener('postStep', () =>
      carPhysics.handlePostStep(),
    )

    // switch camera target
    this.cameraManager.getCameraTarget = () => {
      return physicsSystem.isPlayerInVehicle
        ? { model: carController.model!, type: 'car' }
        : { model: characterController.model!, type: 'character' }
    }

    window.requestAnimationFrame(() => {
      this.previousTick = Date.now()
      this.update()
    })

    this.entityManager.awake()
  }

  public update(): void {
    window.requestAnimationFrame(() => this.update())

    const elapsedTime = this.clock.getElapsedTime()
    const deltaTime = elapsedTime - this.previousTick
    this.previousTick = elapsedTime

    this.entityManager.update(deltaTime)

    // this.controls?.update()
    this.renderer?.render(this.scene!, this.cameraManager!.camera!)
  }

  private setThreeJs(): void {
    this.setRenderer()
    this.setScene()
  }

  private setRenderer(): void {
    this.renderer = new WebGLRenderer({
      antialias: true,
    })
    this.renderer.outputEncoding = sRGBEncoding
    // this.renderer.shadowMap.enabled = true
    this.renderer.shadowMap.type = PCFSoftShadowMap
    this.renderer.setPixelRatio(window.devicePixelRatio)
    this.renderer.setSize(window.innerWidth, window.innerHeight)
    this.renderer.domElement.id = 'threejs'
    this.renderer.physicallyCorrectLights = true
    this.renderer.toneMapping = ACESFilmicToneMapping
    this.renderer.shadowMap.enabled = true
    this.renderer.shadowMap.type = PCFSoftShadowMap

    document
      .getElementById('container')
      ?.appendChild(this.renderer.domElement)

    window.addEventListener(
      'resize',
      () => {
        this.onWindowResize()
      },
      false,
    )
  }

  public onWindowResize(): void {
    if (this.cameraManager?.camera) {
      const camera = this.cameraManager.camera
      if (camera instanceof PerspectiveCamera) {
        camera.aspect = window.innerWidth / window.innerHeight
        camera.updateProjectionMatrix()
      }
    }

    this.renderer?.setSize(window.innerWidth, window.innerHeight)
    this.renderer?.setPixelRatio(Math.min(window.devicePixelRatio, 2))
  }

  private setScene(): void {
    this.scene.background = new Color(0xffffff)
    this.scene.fog = new FogExp2(0x89b2eb, 0.002)
  }
}
