import { Body, Material, RaycastVehicle, Vec3 } from 'cannon-es'
import { Group } from 'three'
import { createCarChassis } from './create-car-chassis'
import { createCarWheels } from './create-car-wheels'
import { CarOptions } from './types'

export interface CreateCarOptions {
  carOptions: CarOptions
  wheelOptions: WheelOptions
  wheelMaterial: Material
}

export interface CarStructure {
  chassis: {
    body: Body
    mesh: Group
  } | null
  wheels: {
    body: Body
    mesh: Group
  }[]
  vehicle: RaycastVehicle | null
}

export interface ChassisOptions {
  chassisDepth: number
  chassisWidth: number
  chassisHeight: number
  chassisMass: number
  chassisOffset: Vec3
}

export interface WheelOptions {
  wheelFrontOffsetDepth: number
  wheelBackOffsetDepth: number
  wheelOffsetWidth: number
  wheelOffsetHeight: number

  wheelRadius: number
  wheelDepth: number
  wheelMass: number

  axleLocal: Vec3
  chassisConnectionPointLocal: Vec3
  customSlidingRotationalSpeed: number
  dampingCompression: number
  dampingRelaxation: number
  directionLocal: Vec3
  frictionSlip: number
  maxSuspensionForce: number
  maxSuspensionTravel: number
  radius: number
  rollInfluence: number
  suspensionRestLength: number
  suspensionStiffness: number
  useCustomSlidingRotationalSpeed: boolean
}

/**
 * @description
 * Creates a CannonJs RaycasterVehicle and wireframe
 */
export function createRaycastVehicle(
  options: CreateCarOptions,
): CarStructure {
  const { carOptions, wheelOptions, wheelMaterial } = options

  const car: CarStructure = {
    wheels: [],
    chassis: null,
    vehicle: null,
  }

  /**
   * Chassis
   */
  car.chassis = createCarChassis({
    height: carOptions.chassisHeight,
    width: carOptions.chassisWidth,
    depth: carOptions.chassisDepth,
    position: new Vec3(-78.89380190195465, 0.3, -3.531153492944502),
    chassisOffset: carOptions.chassisOffset,
    mass: 123,
  })

  /**
   * Vehicle
   */
  car.vehicle = new RaycastVehicle({
    chassisBody: car.chassis.body,
  })

  const wheels = createCarWheels(
    car.vehicle,
    wheelMaterial,
    wheelOptions,
  )

  car.wheels = wheels

  return car
}
