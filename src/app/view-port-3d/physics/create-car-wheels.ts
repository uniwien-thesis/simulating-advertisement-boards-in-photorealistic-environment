import {
  Body,
  Cylinder,
  Material,
  Quaternion,
  RaycastVehicle,
  Vec3,
  WheelInfo,
} from 'cannon-es'
import { Color, Group, Mesh, MeshPhongMaterial } from 'three'
import { bodyToMesh } from './conversion-utils'
import { createCarWheel } from './create-car-wheel'

export interface WheelOptions {
  wheelFrontOffsetDepth: number
  wheelBackOffsetDepth: number
  wheelOffsetWidth: number
  wheelOffsetHeight: number

  wheelRadius: number
  wheelDepth: number
  wheelMass: number

  axleLocal: Vec3
  chassisConnectionPointLocal: Vec3
  customSlidingRotationalSpeed: number
  dampingCompression: number
  dampingRelaxation: number
  directionLocal: Vec3
  frictionSlip: number
  maxSuspensionForce: number
  maxSuspensionTravel: number
  radius: number
  rollInfluence: number
  suspensionRestLength: number
  suspensionStiffness: number
  useCustomSlidingRotationalSpeed: boolean
}

// 0 front left
// 1 front right
// 2 back left
// 3 back right
export function createCarWheels(
  vehicle: RaycastVehicle,
  material: Material,
  wheelOptions: WheelOptions,
): { body: Body; mesh: Group }[] {
  const wheelParameters = {
    axleLocal: new Vec3(0, 0, 1),
    chassisConnectionPointLocal:
      wheelOptions.chassisConnectionPointLocal,
    customSlidingRotationalSpeed:
      wheelOptions.customSlidingRotationalSpeed,
    // height: wheelOptions.wheelRadius,
    dampingCompression: wheelOptions.dampingCompression,
    dampingRelaxation: wheelOptions.dampingRelaxation,
    directionLocal: new Vec3(0, -1, 0),
    frictionSlip: wheelOptions.frictionSlip,
    maxSuspensionForce: wheelOptions.maxSuspensionForce,
    maxSuspensionTravel: wheelOptions.maxSuspensionTravel,
    radius: wheelOptions.wheelRadius,
    rollInfluence: wheelOptions.rollInfluence,
    suspensionRestLength: wheelOptions.suspensionRestLength,
    suspensionStiffness: wheelOptions.suspensionStiffness,
    useCustomSlidingRotationalSpeed: true,
  }

  // Front left
  wheelParameters.chassisConnectionPointLocal.set(
    -wheelOptions.wheelFrontOffsetDepth,
    wheelOptions.wheelOffsetHeight,
    wheelOptions.wheelOffsetWidth,
  )
  vehicle.addWheel(wheelParameters)

  // Front right
  wheelParameters.chassisConnectionPointLocal.set(
    -wheelOptions.wheelFrontOffsetDepth,
    wheelOptions.wheelOffsetHeight,
    -wheelOptions.wheelOffsetWidth,
  )
  vehicle.addWheel(wheelParameters)

  // Back left
  wheelParameters.chassisConnectionPointLocal.set(
    wheelOptions.wheelBackOffsetDepth,
    wheelOptions.wheelOffsetHeight,
    wheelOptions.wheelOffsetWidth,
  )
  vehicle.addWheel(wheelParameters)

  // Back right
  wheelParameters.chassisConnectionPointLocal.set(
    wheelOptions.wheelBackOffsetDepth,
    wheelOptions.wheelOffsetHeight,
    -wheelOptions.wheelOffsetWidth,
  )
  vehicle.addWheel(wheelParameters)

  // make wheel bodies and wireframes
  const wheels = []
  for (let i = 0; i < vehicle.wheelInfos.length; i++) {
    const { body: wheelBody, mesh: wheelMesh } = createCarWheel({
      wheelInfo: vehicle.wheelInfos[i],
      material,
      index: i,
      wheelOptions,
    })

    wheels.push({
      body: wheelBody,
      mesh: wheelMesh,
    })
  }

  return wheels
}
