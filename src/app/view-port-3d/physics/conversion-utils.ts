import {
  Box,
  Sphere,
  Shape,
  Cylinder,
  Vec3,
  Material as CannonMaterial,
  Quaternion,
  Body,
} from 'cannon-es'
import {
  BoxGeometry,
  BufferGeometry,
  CylinderGeometry,
  Group,
  Mesh,
  PlaneGeometry,
  SphereGeometry,
  Material,
  Scene,
  BoxBufferGeometry,
  Object3D,
  Box3,
  Vector3,
} from 'three'
import {
  toCannonQuat,
  toCannonVec3,
  toThreeQuat,
  toThreeVec3,
} from '../utils'

/**
 * @description Converts CANNON.Body to THREE.Group
 *
 * @param body: Body
 * @param material: Material
 * @returns THREE.Group
 */
export function bodyToMesh(body: Body, material: Material): Group {
  const group = new Group()
  group.position.copy(toThreeVec3(body.position))
  group.quaternion.copy(toThreeQuat(body.quaternion))

  const meshes = body.shapes.map((shape) => {
    const geometry = shapeToGeometry(shape)

    return new Mesh(geometry, material)
  })

  meshes.forEach((mesh, i) => {
    const offset = body.shapeOffsets[i]
    const orientation = body.shapeOrientations[i]

    mesh.position.copy(toThreeVec3(offset))
    mesh.quaternion.copy(toThreeQuat(orientation))

    group.add(mesh)
  })

  return group
}

export function shapeToGeometry(shape: Shape): BufferGeometry {
  switch (shape.type) {
    case Shape.types.SPHERE: {
      const sphere = shape as Sphere
      return new SphereGeometry(sphere.radius, 8, 8)
    }

    case Shape.types.PARTICLE: {
      return new SphereGeometry(0.1, 8, 8)
    }

    case Shape.types.PLANE: {
      return new PlaneGeometry(500, 500, 4, 4)
    }

    case Shape.types.BOX: {
      const box = shape as Box
      return new BoxGeometry(
        box.halfExtents.x * 2,
        box.halfExtents.y * 2,
        box.halfExtents.z * 2,
      )
    }

    case Shape.types.CYLINDER: {
      const cylinder = shape as Cylinder
      cylinder
      return new CylinderGeometry(
        cylinder.radiusTop,
        cylinder.radiusBottom,
        cylinder.height,
        cylinder.numSegments,
      )
    }

    default: {
      throw new Error(`Shape not recognized: "${shape.type}"`)
    }
  }
}

export function extractVoxelsShapeInfo(
  scene: Group,
): {
  halfExtents: Vec3
  position: Vec3
  quaternion: Quaternion
}[] {
  const shapes: {
    halfExtents: Vec3
    position: Vec3
    quaternion: Quaternion
  }[] = []
  scene.traverse((child) => {
    if (
      !child.name.match(/^cannono-box[0-9]{0,3}/i) ||
      !(child instanceof Mesh)
    ) {
      return
    }
    const halfExtents = toCannonVec3(child.scale)
    const position = toCannonVec3(child.position)
    const quaternion = toCannonQuat(child.quaternion)

    shapes.push({
      halfExtents,
      position,
      quaternion,
    })
  })

  return shapes
}

export function sceneToWorld(
  scene: Group,
  material: CannonMaterial,
  wireframeMaterial: Material,
): { body: Body; mesh: Group } {
  const body = new Body({
    position: new Vec3(),
    mass: 0,
    material,
    allowSleep: true,
    // TODO: check docs
    // sleepSpeedLimit: 0.01
  })

  const group = new Group()
  const infos = extractVoxelsShapeInfo(scene)

  infos.map(({ halfExtents, position, quaternion }) => {
    const shape = new Box(halfExtents)
    body.addShape(shape, position, quaternion)

    const geometry = new BoxBufferGeometry(
      halfExtents.x,
      halfExtents.y,
      halfExtents.z,
    )
    const wireframe = new Mesh(geometry, wireframeMaterial)
    wireframe.position.copy(toThreeVec3(position))
    wireframe.quaternion.copy(toThreeQuat(quaternion))
    group.add(wireframe)
  })

  return {
    body,
    mesh: group,
  }
}

export function sceneToWorld2(
  scene: Group,
  material: CannonMaterial,
  wireframeMaterial: Material,
): { body: Body; mesh: Group } {
  const body = new Body({
    position: new Vec3(),
    mass: 0,
    material,
    allowSleep: true,
    // TODO: check docs
    // sleepSpeedLimit: 0.01
  })

  const group = new Group()
  scene.traverse((mesh) => {
    if (
      mesh.name.match(/^cannono-box[0-9]{0,3}/i) &&
      mesh instanceof Mesh
    ) {
      const size = mesh.scale.clone()
      size.multiplyScalar(0.5)
      const halfExtents = toCannonVec3(size)
      const shape = new Box(halfExtents)
      const position = toCannonVec3(mesh.position)
      const quaternion = toCannonQuat(mesh.quaternion)

      mesh.material = wireframeMaterial
      body.addShape(shape, position, quaternion)

      const geometry = new BoxBufferGeometry(
        mesh.scale.x,
        mesh.scale.y,
        mesh.scale.z,
      )
      const wireframe = new Mesh(geometry, wireframeMaterial)
      wireframe.position.copy(mesh.position)
      wireframe.quaternion.copy(mesh.quaternion)
      wireframe.visible = false
      group.add(wireframe)
    }
  })

  return {
    body,
    mesh: group,
  }
}
