import { Body, Material, Quaternion, Sphere, Vec3 } from 'cannon-es'

export function createCharacter(material: Material): Body {
  const capsuleShape = new Sphere(0.5)
  const characterBody = new Body({
    mass: 1,
    shape: capsuleShape,
    material,
    angularDamping: 0.9,
    linearDamping: 0.99,
    allowSleep: false,
    fixedRotation: true,
  })

  characterBody.position.set(
    -83.89380190195465,
    0.5,
    3.531153492944502,
  )

  const initialRotation = new Quaternion().setFromAxisAngle(
    new Vec3(0, 1, 0),
    Math.PI / 2,
  )

  characterBody.quaternion.copy(initialRotation)

  return characterBody
}
