import {
  Body,
  Cylinder,
  Material,
  Quaternion,
  Vec3,
  WheelInfo,
} from 'cannon-es'
import { Color, Group, MeshPhongMaterial } from 'three'
import { bodyToMesh } from './conversion-utils'
import { WheelOptions } from './create-car-wheels'

interface CreateWheelOption {
  wheelInfo: WheelInfo
  material: Material
  index: number
  wheelOptions: WheelOptions
}

export function createCarWheel(
  options: CreateWheelOption,
): {
  body: Body
  mesh: Group
} {
  const { wheelInfo, material, index, wheelOptions } = options
  const shape = new Cylinder(
    wheelInfo.radius,
    wheelInfo.radius,
    wheelOptions.wheelRadius,
    20,
  )
  const body = new Body({
    mass: wheelOptions.wheelMass,
    material,
  })
  const quaternion = new Quaternion()
  quaternion.setFromAxisAngle(new Vec3(1, 0, 0), Math.PI / 2)

  body.type = Body.KINEMATIC
  body.collisionFilterGroup = 0 // turn of collisions
  body.addShape(shape, new Vec3(), quaternion)
  // 0 front left
  // 1 front right
  // 2 back left
  // 3 back right

  const color = index === 5 ? 0xff0000 : 0xff00ff
  const mat = new MeshPhongMaterial({
    color: new Color(color),
  })

  const wheelDebugMesh = bodyToMesh(body, mat)
  wheelDebugMesh.visible = false

  return {
    body,
    mesh: wheelDebugMesh,
  }
}
