import { ContactMaterial, SAPBroadphase, World } from 'cannon-es'

export function createWorld(
  defaultMaterials: ContactMaterial[],
): World {
  const world = new World()
  world.gravity.set(0, -9.81, 0)
  world.broadphase = new SAPBroadphase(world)
  world.allowSleep = true
  defaultMaterials.forEach((mat) => world.addContactMaterial(mat))

  return world
}
