import { ContactMaterial, Material, Vec3, World } from 'cannon-es'
import { take } from 'rxjs/operators'
import {
  MeshBasicMaterial,
  Quaternion as ThreeQuaternion,
  Scene,
  Vector3,
} from 'three'

import { AssetManager } from '../asset-manager'
import { CameraManager } from '../camera/camera-manager'
import { Car } from '../car/car'
import { CarPhysics } from '../car/car.physics'
import { Character } from '../character/character'
import { CharacterPhysics } from '../character/character.physics'
import { ECSEntity } from '../ecs/entity'
import { KeyboardInput } from '../input/keyboard.input'
import { toThreeVec3 } from '../utils'
import { sceneToWorld2 } from './conversion-utils'
import { createWorld } from './create-world'

export class PhysicsSystem extends ECSEntity {
  defaultMaterial = new Material('default')
  defaultContactMaterial = new ContactMaterial(
    this.defaultMaterial,
    this.defaultMaterial,
    {
      friction: 0.0,
      restitution: 0.3,
    },
  )

  wheelMaterial = new Material('wheelMaterial')
  wheelDefaultContactMaterial = new ContactMaterial(
    this.wheelMaterial,
    this.defaultMaterial,
    {
      friction: 0.0,
      restitution: 0,
      contactEquationStiffness: 1000,
    },
  )

  public acceleration = new Vector3(0.1, 0, 40.0)
  public rotation = 0.125
  public velocity = new Vector3(0, 0.3, 0)
  public yAxis = new Vec3(0, 1, 0)

  isPlayerInVehicle = false
  world?: World

  assetManager: AssetManager
  keyboardInput: KeyboardInput
  scene: Scene
  cameraManager: CameraManager

  constructor(options: {
    physicsWorker?: Worker
    assetManager: AssetManager
    keyboardInput: KeyboardInput
    scene: Scene
    cameraManager: CameraManager
  }) {
    super()
    this.assetManager = options.assetManager
    this.keyboardInput = options.keyboardInput
    this.scene = options.scene
    this.cameraManager = options.cameraManager

    this.world = createWorld([
      this.defaultContactMaterial,
      this.wheelDefaultContactMaterial,
    ])

    this.assetManager.assets.levelPhysics
      .pipe(take(1))
      .subscribe((level) => {
        const { mesh, body } = sceneToWorld2(
          level,
          this.defaultMaterial,
          new MeshBasicMaterial({ color: 0xff0000 }),
        )

        this.scene.add(mesh!)
        this.world?.addBody(body)

        const car = this.entityManger?.getEntity(Car.name) as Car
        const carPhysics = car?.getComponent(
          CarPhysics.name,
        ) as CarPhysics
        carPhysics.car?.vehicle?.addToWorld(this.world!)

        const character = this.entityManger?.getEntity(
          Character.name,
        ) as Character
        const characterPhysics = character?.getComponent(
          CharacterPhysics.name,
        ) as CharacterPhysics
        this.world?.addBody(characterPhysics.character)

        // this.world?.addBody(this.characterBody!)

        // this.options?.scene?.add(this.car?.chassis?.mesh!)
        //
        // this.car?.wheels.forEach((wheel) => {
        // this.options?.scene?.add(wheel.mesh)
        // })
      })

    this.keyboardInput.listenKeyUp((event) => {
      if (event.key === 'f') {
        this.switchTarget()
      }
    })
  }

  update(dt: number) {
    this.world?.step(1 / 60, dt, 3)

    // if (physics.car && carModel) {
    //   carModel.position.copy(
    //     toThreeVec3(physics.car?.chassis?.body.position!),
    //   )
    //   carModel.position.y = 0
    //   carModel.quaternion.copy(
    //     toThreeQuat(physics.car?.chassis?.body.quaternion!),
    //   )
    //   carModel.quaternion.multiplyQuaternions(
    //     carModel.quaternion,
    //     new ThreeQuaternion().setFromAxisAngle(
    //       new Vector3(0, 1, 0),
    //       -Math.PI * 0.5,
    //     ),
    //   )
    // }

    // if (!this.isPlayerInVehicle) {
    //   const resultQuaternion = new Quaternion()
    //   const currentQuaternion = toThreeQuat(
    //     physics.characterBody!.quaternion.clone(),
    //   )

    //   // on shift speed up
    //   let scaleFactor = 1
    //   if (this.keyboardInput.keys.shift) {
    //     scaleFactor = 2
    //   }

    //   // translate
    //   this.velocity.set(0, 0, 0)
    //   this.velocity.z +=
    //     dt * 10 * input.z * scaleFactor * this.acceleration.z

    //   // rotate
    //   resultQuaternion.setFromAxisAngle(
    //     this.yAxis,
    //     4.0 * Math.PI * -input.x * dt * this.rotation,
    //   )
    //   currentQuaternion!.multiply(toThreeQuat(resultQuaternion))

    //   this.velocity.applyQuaternion(currentQuaternion)
    //   physics.characterBody!.quaternion.copy(
    //     toCannonQuat(currentQuaternion),
    //   )
    //   physics.characterBody!.velocity.copy(
    //     toCannonVec3(this.velocity),
    //   )
    // }

    super.update(dt)
  }

  switchTarget(): void {
    this.isPlayerInVehicle = !this.isPlayerInVehicle

    const character = this.entityManger?.getEntity(
      Character.name,
    ) as Character
    const car = this.entityManger?.getEntity(Car.name) as Car
    const carPhysics = car?.getComponent(
      CarPhysics.name,
    ) as CarPhysics
    const characterModel = character.getCharacterModel
    const carModel = car.getCarModel
    const characterPhysics = character?.getComponent(
      CharacterPhysics.name,
    ) as CharacterPhysics

    if (!characterModel || !carModel) {
      return
    }
    if (this.isPlayerInVehicle) {
      characterModel.visible = false
    } else {
      characterModel.visible = true

      const distance = new Vector3(3, 0, 0)

      distance.applyQuaternion(
        new ThreeQuaternion().setFromAxisAngle(
          new Vector3(0, 0, 0),
          Math.PI * -0.5,
        ),
      )
      distance.applyQuaternion(carModel.quaternion)
      distance.add(
        toThreeVec3(carPhysics.car?.chassis?.body.position!),
      )
      characterPhysics.character.position.set(
        distance.x,
        distance.y,
        distance.z,
      )
    }
  }
}

function rotate(options: {}) {}
