import { Body, Box, Vec3 } from 'cannon-es'
import { Color, Group, MeshPhongMaterial } from 'three'
import { bodyToMesh } from './conversion-utils'

interface CreateChassisOptions {
  width: number
  height: number
  depth: number
  mass: number
  chassisOffset: Vec3

  position: Vec3
}

export function createCarChassis(
  options: CreateChassisOptions,
): {
  body: Body
  mesh: Group
} {
  const chassisShape = new Box(
    new Vec3(options.depth, options.height, options.width),
  )
  const chassisBody = new Body({
    mass: options.mass,
  })
  chassisBody.position.set(
    options.position.x,
    options.position.y,
    options.position.z,
  )

  chassisBody.quaternion.setFromAxisAngle(
    new Vec3(0, 1, 0),
    Math.PI * 0.5,
  )

  chassisBody.angularVelocity.set(0, 0, 0)
  chassisBody.addShape(chassisShape, options.chassisOffset)

  const chassisDebugMesh = bodyToMesh(
    chassisBody,
    new MeshPhongMaterial({
      color: new Color(0x00ffff),
    }),
  )
  chassisDebugMesh.visible = false

  return {
    body: chassisBody,
    mesh: chassisDebugMesh,
  }
}
