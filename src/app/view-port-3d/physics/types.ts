import { Material, RaycastVehicle, Vec3 } from 'cannon-es'
import { Group } from 'three'

export interface CarOptions {
  chassisDepth: number
  chassisWidth: number
  chassisHeight: number
  chassisMass: number
  chassisOffset: Vec3
}

export interface WheelOptions {
  wheelFrontOffsetDepth: number
  wheelBackOffsetDepth: number
  wheelOffsetWidth: number
  wheelOffsetHeight: number

  wheelRadius: number
  wheelDepth: number
  wheelMass: number

  axleLocal: Vec3
  chassisConnectionPointLocal: Vec3
  customSlidingRotationalSpeed: number
  dampingCompression: number
  dampingRelaxation: number
  directionLocal: Vec3
  frictionSlip: number
  maxSuspensionForce: number
  maxSuspensionTravel: number
  radius: number
  rollInfluence: number
  suspensionRestLength: number
  suspensionStiffness: number
  useCustomSlidingRotationalSpeed: boolean
}

export interface CarStructure {
  chassis: { body: Body; mesh: Group } | null
  wheels: { body: Body; mesh: Group }[]
  vehicle: RaycastVehicle | null
}

export interface CreateCarOptions {
  carOptions: CarOptions
  wheelOptions: WheelOptions
  wheelMaterial: Material
}
