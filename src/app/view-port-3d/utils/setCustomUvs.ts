import { Mesh } from 'three'

export function setCustomUvs(mesh: Mesh, uvs: number[][]) {
  const uvAttr = mesh.geometry.attributes.uv

  for (let i = 0; i < uvAttr.count; i++) {
    uvAttr.setXY(i, uvs[i][0], uvs[i][1])
  }
  uvAttr.needsUpdate = true
}
