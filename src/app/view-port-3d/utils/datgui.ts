import { GUI } from 'dat.gui'

export function removeFolderFromGUI(name: string, gui: GUI) {
  const folder = gui.__folders[name]
  if (!folder) {
    return
  }

  folder.close()

  //@ts-ignore
  gui.__ul.removeChild(folder.domElement.parentNode)
  delete gui.__folders[name]

  //@ts-ignore
  gui.onResize()
}
