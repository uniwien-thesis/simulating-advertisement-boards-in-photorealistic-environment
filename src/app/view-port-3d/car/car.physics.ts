import { Material } from 'cannon-es'
import { ECSComponent } from '../ecs/component'
import { Params } from '../ecs/types'
import {
  CarStructure,
  createRaycastVehicle,
} from '../physics/create-car'
import {
  setCannonQuatToThreeQuat,
  setCannonVecToThreeVec,
} from '../utils'
import { CAR_OPTIONS, WHEEL_OPTIONS } from './car.settings'

export class CarPhysics extends ECSComponent {
  car?: CarStructure
  wheelMaterial: Material
  shouldBrake = false

  constructor(options: Params, wheelMaterial: Material) {
    super(options)
    this.wheelMaterial = wheelMaterial
    this.car = createRaycastVehicle({
      carOptions: CAR_OPTIONS,
      wheelOptions: WHEEL_OPTIONS,
      wheelMaterial: this.wheelMaterial,
    })
  }

  public awake(): void {
    const keyboardInput = this.options?.keyboardInput
    if (!keyboardInput) {
      debugger
      return
    }

    keyboardInput.listenKeyUp((event) => this.onKeyUp(event))
    keyboardInput.listenKeyDown((event) => this.onKeyDown(event))
  }

  public update(_deltaTime: number): void {}

  public handlePostStep() {
    this.car?.vehicle?.wheelInfos.forEach((wheel, i) => {
      this.car?.vehicle?.updateWheelTransform(i)

      const wheelInfo = this.car?.wheels?.[i]
      const { body, mesh } = wheelInfo!

      const transform = wheel.worldTransform

      body.position.copy(transform.position)
      body.quaternion.copy(transform.quaternion)

      setCannonVecToThreeVec(transform.position, mesh.position)
      setCannonQuatToThreeQuat(transform.quaternion, mesh.quaternion)
    })
  }

  get isPlayerInVehicle(): boolean {
    const physics = this.entity?.entityManger?.getEntity(
      'PhysicsSystem',
    )

    // @ts-ignore
    return physics.isPlayerInVehicle
  }

  /**
   * Apply force to car
   */
  public moveForward(): void {
    if (!this.isPlayerInVehicle) {
      return
    }
    console.log('moving')
    this.car?.vehicle?.applyEngineForce(-500, 2)
    this.car?.vehicle?.applyEngineForce(-500, 3)
  }

  public moveBackward(): void {
    if (!this.isPlayerInVehicle) {
      return
    }

    this.car?.vehicle?.applyEngineForce(500, 2)
    this.car?.vehicle?.applyEngineForce(500, 3)
  }

  public turnLeft(): void {
    if (!this.isPlayerInVehicle) {
      return
    }
    this.car?.vehicle?.setSteeringValue(Math.PI * 0.17, 0)
    this.car?.vehicle?.setSteeringValue(Math.PI * 0.17, 1)
  }

  public turnRight(): void {
    if (!this.isPlayerInVehicle) {
      return
    }

    this.car?.vehicle?.setSteeringValue(-Math.PI * 0.17, 0)
    this.car?.vehicle?.setSteeringValue(-Math.PI * 0.17, 1)
  }

  public resetBreak(): void {
    if (!this.isPlayerInVehicle) {
      return
    }
    this.car?.vehicle?.setBrake(0, 0)
    this.car?.vehicle?.setBrake(0, 1)
    this.car?.vehicle?.setBrake(0, 2)
    this.car?.vehicle?.setBrake(0, 3)
  }

  public break(): void {
    if (!this.isPlayerInVehicle) {
      return
    }
    this.car?.vehicle?.setBrake(10, 0)
    this.car?.vehicle?.setBrake(10, 1)
    this.car?.vehicle?.setBrake(10, 2)
    this.car?.vehicle?.setBrake(10, 3)
  }

  public slowdown(): void {
    if (!this.isPlayerInVehicle) {
      return
    }
    this.car?.vehicle?.applyEngineForce(0, 2)
    this.car?.vehicle?.applyEngineForce(0, 3)
  }

  public resetWheelsRotation() {
    if (!this.isPlayerInVehicle) {
      return
    }
    this.car?.vehicle?.setSteeringValue(0, 0)
    this.car?.vehicle?.setSteeringValue(0, 1)
  }

  public onKeyDown(event: KeyboardEvent): void {
    if (this.shouldBrake) {
      debugger
      return
    }

    this.resetBreak()

    switch (event.key) {
      case 'w':
      case 'ArrowUp':
        this.moveForward()
        break

      case 's':
      case 'ArrowDown':
        this.moveBackward()
        break

      case 'a':
      case 'ArrowLeft':
        this.turnLeft()
        break

      case 'd':
      case 'ArrowRight':
        this.turnRight()
        break

      case 'b':
        this.break()
        break
    }
  }

  public onKeyUp(event: KeyboardEvent): void {
    if (this.shouldBrake) {
      return
    }

    this.resetBreak()

    switch (event.key) {
      case 'w':
      case 'ArrowUp':
      case 's':
      case 'ArrowDown':
        this.slowdown()
        break

      case 'a':
      case 'ArrowLeft':
      case 'd':
      case 'ArrowRight':
        this.resetWheelsRotation()
        break
    }
  }
}
