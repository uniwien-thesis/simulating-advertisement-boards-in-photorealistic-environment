import { take } from 'rxjs/operators'
import {
  Color,
  Group,
  Mesh,
  MeshBasicMaterial,
  Vector3,
  Quaternion,
} from 'three'

import { ECSComponent } from '../ecs/component'
import { Params } from '../ecs/types'
import { toThreeQuat, toThreeVec3 } from '../utils'
import { CarPhysics } from './car.physics'

export class CarController extends ECSComponent {
  isDisabled = true
  carPhysics: CarPhysics | null = null
  model?: Group

  defaultWireframeMaterial = new MeshBasicMaterial({
    color: new Color(0xff0000),
    // wireframe: true,
  })

  constructor(options: Params) {
    super(options)

    this.options?.assetManager?.assets.carVisual
      .pipe(take(1))
      .subscribe((car) => {
        this.setUpModel(car)
      })
  }

  public awake(): void {
    // this.loadModel()
    // this.carPhysics = this.entity!.getComponent(
    //   CarPhysics.name,
    // ) as CarPhysics
    // Apply force on keydown
    // document.addEventListener('keydown', (event) =>
    //   this.onKeyDown(event),
    // )
    // // Reset force on keyup
    // document.addEventListener('keyup', (event) => this.onKeyUp(event))
    // this.disable()
  }

  public update(dt: number): void {
    // this.carModel?.position.copy(
    //   toThreeVec3(this.carPhysics?.car?.chassis?.body.position!),
    // )
    // this.carModel?.position && (this.carModel!.position.y = 0)
    // const q = toThreeQuat(
    //   this.carPhysics?.car?.chassis?.body.quaternion!,
    // )
    // q.multiplyQuaternions(
    //   q,
    //   new Quaternion().setFromAxisAngle(
    //     new Vector3(0, 1, 0),
    //     Math.PI * -0.5,
    //   ),
    // )
    // this.carModel?.quaternion.copy(q)
  }

  setUpModel(group: Group) {
    const factor = 1.1
    group.scale.set(factor, factor, factor)
    this.options?.scene?.add(group)
    this.model = group
  }

  public enable(): void {
    this.carPhysics!.shouldBrake = false
  }

  public disable(): void {
    this.carPhysics!.shouldBrake = true
    this.carPhysics?.break()
  }

  public onKeyDown(event: KeyboardEvent): void {
    if (this.carPhysics!.shouldBrake) {
      return
    }

    this.carPhysics?.resetBreak()

    switch (event.key) {
      case 'w':
      case 'ArrowUp':
        this.carPhysics?.moveForward()
        break

      case 's':
      case 'ArrowDown':
        this.carPhysics?.moveBackward()
        break

      case 'a':
      case 'ArrowLeft':
        this.carPhysics?.turnLeft()
        break

      case 'd':
      case 'ArrowRight':
        this.carPhysics?.turnRight()
        break

      case 'b':
        this.carPhysics?.break()
        break
    }
  }

  public onKeyUp(event: KeyboardEvent): void {
    if (this.carPhysics!.shouldBrake) {
      return
    }

    this.carPhysics?.resetBreak()

    switch (event.key) {
      case 'w':
      case 'ArrowUp':
      case 's':
      case 'ArrowDown':
        this.carPhysics?.slowdown()
        break

      case 'a':
      case 'ArrowLeft':
      case 'd':
      case 'ArrowRight':
        this.carPhysics?.resetWheelsRotation()
        break
    }
  }
}
