import { Vec3, WheelInfoOptions } from 'cannon-es'
import { CarOptions, WheelOptions } from '../physics/types'

export const CHASSIS_WIDTH = 'chassisWidth'
export const CHASSIS_HEIGHT = 'chassisHeight'
export const CHASSIS_DEPTH = 'chassisDepth'
export const CHASSIS_OFFSET = 'chassisOffset'
export const CHASSIS_MASS = 'chassisMass'
export const WHEEL_FRONT_OFFSET_DEPTH = 'wheelFrontOffsetDepth'
export const WHEEL_BACK_OFFSET_DEPTH = 'wheelBackOffsetDepth'
export const WHEEL_OFFSET_WIDTH = 'wheelOffsetWidth'
export const WHEEL_OFFSET_HEIGHT = 'wheelOffsetHeight'
export const WHEEL_RADIUS = 'wheelRadius'
export const WHEEL_DEPTH = 'wheelDepth'
export const WHEEL_MASS = 'wheelMass'
export const AXLE_LOCAL = 'axleLocal'
export const CHASSIS_CONNECTION_POINT_LOCAL =
  'chassisConnectionPointLocal'
export const CUSTOM_SLIDING_ROTATIONAL_SPEED =
  'customSlidingRotationalSpeed'
export const DAMPING_COMPRESSION = 'dampingCompression'
export const DAMPING_RELAXATION = 'dampingRelaxation'
export const DIRECTION_LOCAL = 'directionLocal'
export const FRICTION_SLIP = 'frictionSlip'
export const MAX_SUSPENSION_FORCE = 'maxSuspensionForce'
export const MAX_SUSPENSION_TRAVEL = 'maxSuspensionTravel'
export const RADIUS = 'radius'
export const ROLL_INFLUENCE = 'rollInfluence'
export const SUSPENSION_REST_LENGTH = 'suspensionRestLength'
export const SUSPENSION_STIFFNESS = 'suspensionStiffness'
export const USE_CUSTOM_SLIDING_ROTATIONAL_SPEED =
  'useCustomSlidingRotationalSpeed'

export const CAR_OPTIONS: CarOptions = {
  [CHASSIS_WIDTH]: 0.6,
  [CHASSIS_HEIGHT]: 0.235,
  [CHASSIS_DEPTH]: 1.19,
  [CHASSIS_OFFSET]: new Vec3(0, 0.159, 0),
  [CHASSIS_MASS]: 150,
}

export const WHEEL_OPTIONS: WheelOptions = {
  // wheel position
  [WHEEL_FRONT_OFFSET_DEPTH]: 1.013,
  [WHEEL_BACK_OFFSET_DEPTH]: 0.847,
  [WHEEL_OFFSET_WIDTH]: 0.739,
  [WHEEL_OFFSET_HEIGHT]: 0,

  [WHEEL_RADIUS]: 0.233,
  [WHEEL_DEPTH]: 0.45,
  [WHEEL_MASS]: 5,

  [AXLE_LOCAL]: new Vec3(0, 0, 1),
  [CHASSIS_CONNECTION_POINT_LOCAL]: new Vec3(-1, 0, 1),
  [DIRECTION_LOCAL]: new Vec3(0, -1, 0),
  
  [CUSTOM_SLIDING_ROTATIONAL_SPEED]: -30,
  [DAMPING_COMPRESSION]: 4.4,
  [DAMPING_RELAXATION]: 2.3,
  [FRICTION_SLIP]: 5,
  [MAX_SUSPENSION_FORCE]: 100000,
  [MAX_SUSPENSION_TRAVEL]: 0.3,
  [RADIUS]: 0.6,
  [ROLL_INFLUENCE]: 0.01,
  [SUSPENSION_REST_LENGTH]: 0.3,
  [SUSPENSION_STIFFNESS]: 30,
  [USE_CUSTOM_SLIDING_ROTATIONAL_SPEED]: true,
}

// TODO: remove
export const WHEEL_INDEXES = {
  frontLeft: 0,
  frontRight: 1,
  backLeft: 2,
  backRight: 3,
}
