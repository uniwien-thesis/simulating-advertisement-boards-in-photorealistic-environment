import { ECSEntity } from '../ecs/entity'
import {
  setCannonQuatToThreeQuat,
  setCannonVecToThreeVec,
} from '../utils'
import { CarPhysics } from './car.physics'

import { Body } from 'cannon-es'
import { Group, Quaternion, Vector3 } from 'three'
import { CarController } from './car.controller'

export class Car extends ECSEntity {
  update(dt: number) {
    setCannonQuatToThreeQuat(
      this.getCarChassisBody?.quaternion,
      this.getCarModel?.quaternion,
    )
    this.getCarModel?.quaternion.multiplyQuaternions(
      this.getCarModel.quaternion,
      new Quaternion().setFromAxisAngle(
        new Vector3(0, 1, 0),
        -Math.PI * 0.5,
      ),
    )
    setCannonVecToThreeVec(
      this.getCarChassisBody?.position,
      this.getCarModel?.position,
    )
    this.getCarModel && (this.getCarModel.position.y = 0.05)

  }

  public get getCarModel(): Group | undefined {
    const carController = this.getComponent(
      CarController.name,
    ) as CarController
    return carController.model
  }

  public get getCarChassisBody(): Body | undefined {
    const carPhysics = this.getComponent(
      CarPhysics.name,
    ) as CarPhysics

    return carPhysics.car?.chassis?.body
  }
}
