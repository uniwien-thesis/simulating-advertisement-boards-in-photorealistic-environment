import { GUI } from 'dat.gui'
import {
  CHASSIS_WIDTH,
  CHASSIS_HEIGHT,
  CHASSIS_DEPTH,
  CHASSIS_OFFSET,
  CHASSIS_MASS,
  WHEEL_FRONT_OFFSET_DEPTH,
  WHEEL_BACK_OFFSET_DEPTH,
  WHEEL_OFFSET_WIDTH,
  WHEEL_OFFSET_HEIGHT,
  WHEEL_RADIUS,
  WHEEL_DEPTH,
  WHEEL_MASS,
  CUSTOM_SLIDING_ROTATIONAL_SPEED,
  DAMPING_COMPRESSION,
  DAMPING_RELAXATION,
  FRICTION_SLIP,
  MAX_SUSPENSION_FORCE,
  MAX_SUSPENSION_TRAVEL,
  RADIUS,
  ROLL_INFLUENCE,
  SUSPENSION_REST_LENGTH,
  SUSPENSION_STIFFNESS,
} from './car.settings'
import { CarOptions, WheelOptions } from '../physics/types'

interface VehiclePhysicsGUIOptions {
  gui: GUI
  carOptions: CarOptions
  recreate: () => void
}

interface WheelPhysicsGUIOptions {
  gui: GUI
  wheelOptions: WheelOptions
  recreate: () => void
}

export function addCarGUI(options: VehiclePhysicsGUIOptions): void {
  const { gui, carOptions, recreate } = options

  const folder = gui.addFolder('Chassis')

  folder
    .add(carOptions, CHASSIS_WIDTH)
    .step(0.001)
    .min(0)
    .max(5)
    .name(CHASSIS_WIDTH)
    .onFinishChange(() => recreate())

  folder
    .add(carOptions, CHASSIS_HEIGHT)
    .step(0.001)
    .min(0)
    .max(5)
    .name(CHASSIS_HEIGHT)
    .onFinishChange(() => recreate())

  folder
    .add(carOptions, CHASSIS_DEPTH)
    .step(0.001)
    .min(0)
    .max(5)
    .name(CHASSIS_DEPTH)
    .onFinishChange(() => recreate())

  folder
    .add(carOptions[CHASSIS_OFFSET], 'z')
    .step(0.001)
    .min(-5)
    .max(5)
    .name(CHASSIS_OFFSET + ' x')
    .onFinishChange(() => recreate())

  folder
    .add(carOptions[CHASSIS_OFFSET], 'y')
    .step(0.001)
    .min(-5)
    .max(5)
    .name(CHASSIS_OFFSET + ' y')
    .onFinishChange(() => recreate())

  folder
    .add(carOptions[CHASSIS_OFFSET], 'x')
    .step(0.001)
    .min(-5)
    .max(5)
    .name(CHASSIS_OFFSET + ' z')
    .onFinishChange(() => recreate())

  folder
    .add(carOptions, CHASSIS_MASS)
    .step(0.001)
    .min(0)
    .max(150)
    .name(CHASSIS_MASS)
    .onFinishChange(() => recreate())
}

export function addWheelsGUI(options: WheelPhysicsGUIOptions): void {
  const { gui, wheelOptions, recreate } = options

  const folder = gui.addFolder('Wheels')

  folder
    .add(wheelOptions, WHEEL_FRONT_OFFSET_DEPTH)
    .step(0.001)
    .min(0)
    .max(5)
    .name(WHEEL_FRONT_OFFSET_DEPTH)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, WHEEL_BACK_OFFSET_DEPTH)
    .step(0.001)
    .min(0)
    .max(5)
    .name(WHEEL_BACK_OFFSET_DEPTH)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, WHEEL_OFFSET_WIDTH)
    .step(0.001)
    .min(0)
    .max(5)
    .name(WHEEL_OFFSET_WIDTH)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, WHEEL_OFFSET_HEIGHT)
    .step(0.001)
    .min(0)
    .max(5)
    .name(WHEEL_OFFSET_HEIGHT)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, WHEEL_RADIUS)
    .step(0.001)
    .min(0)
    .max(5)
    .name(WHEEL_RADIUS)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, WHEEL_DEPTH)
    .step(0.001)
    .min(0)
    .max(5)
    .name(WHEEL_DEPTH)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, WHEEL_MASS)
    .step(0.001)
    .min(0)
    .max(5)
    .name(WHEEL_MASS)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, CUSTOM_SLIDING_ROTATIONAL_SPEED)
    .step(1)
    .min(-100)
    .max(100)
    .name(CUSTOM_SLIDING_ROTATIONAL_SPEED)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, DAMPING_COMPRESSION)
    .step(0.1)
    .min(0)
    .max(10)
    .name(DAMPING_COMPRESSION)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, DAMPING_RELAXATION)
    .step(0.1)
    .min(0)
    .max(10)
    .name(DAMPING_RELAXATION)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, FRICTION_SLIP)
    .step(0.1)
    .min(0)
    .max(10)
    .name(FRICTION_SLIP)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, MAX_SUSPENSION_FORCE)
    .step(100)
    .min(0)
    .max(100000)
    .name(MAX_SUSPENSION_FORCE)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, MAX_SUSPENSION_TRAVEL)
    .step(0.001)
    .min(0)
    .max(1)
    .name(MAX_SUSPENSION_TRAVEL)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, RADIUS)
    .step(0.001)
    .min(0)
    .max(1)
    .name(RADIUS)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, ROLL_INFLUENCE)
    .step(0.001)
    .min(0)
    .max(1)
    .name(ROLL_INFLUENCE)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, SUSPENSION_REST_LENGTH)
    .step(0.001)
    .min(0)
    .max(1)
    .name(SUSPENSION_REST_LENGTH)
    .onFinishChange(() => recreate())

  folder
    .add(wheelOptions, SUSPENSION_STIFFNESS)
    .step(0.1)
    .min(0)
    .max(100)
    .name(SUSPENSION_STIFFNESS)
    .onFinishChange(() => recreate())
}
