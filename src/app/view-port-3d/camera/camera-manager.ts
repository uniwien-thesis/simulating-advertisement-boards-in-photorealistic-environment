import { GUI } from 'dat.gui'
import { Vec3 } from 'math/Vec3'
import {
  Camera,
  Group,
  PerspectiveCamera,
  Quaternion,
  Vector3,
  WebGLRenderer,
} from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { ECSEntity } from '../ecs/entity'
import { Params } from '../ecs/types'
import { KeyboardInput } from '../input/keyboard.input'
import { CameraFSM } from './camera.fsm'
import { CAMERA_STATE } from './states'

export class CameraManager extends ECSEntity {
  public getCameraTarget?: () => {model: Group, type: string}
  public camera: Camera
  public renderer?: WebGLRenderer
  public cameraTarget?: Group
  public cameraTargetType?: 'car' | 'character' = 'character'
  public input: KeyboardInput
  public fsm: CameraFSM
  cameraGUI: GUI

  public orbitControls?: OrbitControls
  public settings = {
    isOrbitControlsEnabled: false,
  }

  public cameraState = {
    lookAt: new Vector3(),
    offset: new Vector3(),
  }

  constructor(
    renderer: WebGLRenderer,
    input: KeyboardInput,
    gui: GUI,
  ) {
    super()

    this.input = input
    this.camera = this.makePerspectiveCamera()
    this.renderer = renderer
    this.fsm = new CameraFSM(this)
    this.fsm.setState(CAMERA_STATE.INTRO)

    this.cameraGUI = gui.addFolder('Camera')
    this.cameraGUI
      .add(this.settings, 'isOrbitControlsEnabled')
      .onChange((value) => {
        if (value) {
          this.orbitControls = new OrbitControls(
            this.camera,
            this.renderer?.domElement,
          )
        } else {
          this.orbitControls?.dispose()
          this.orbitControls = undefined
        }
      })
  }

  public makePerspectiveCamera(): PerspectiveCamera {
    const aspect = window.innerWidth / window.innerHeight
    const near = 0.7
    const fov = 60
    const far = 10000.0
    const camera = new PerspectiveCamera(fov, aspect, near, far)
    camera.position.set(0, 0, 1)
    camera.lookAt(0, 0, 0)

    return camera
  }

  public update(dt: number): void {
    super.update(dt)

    if (this.settings.isOrbitControlsEnabled) {
      this.orbitControls?.update()
      // this.orbitControls!.target = this.cameraTarget?.wireframe?.position!
      this.orbitControls!.target = new Vector3()
    }

    this.fsm.update(dt)
  }
}
