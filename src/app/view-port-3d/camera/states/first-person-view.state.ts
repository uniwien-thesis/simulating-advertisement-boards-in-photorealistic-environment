import { Vector3, Camera, Quaternion, MathUtils } from 'three'
import { CAMERA_STATE } from '.'
import { FiniteState } from '../../fsm/finite.state'
import { toThreeVec3, toThreeQuat } from '../../utils'
import { CameraFSM } from '../camera.fsm'
// this.character.velocity.length()
const cameraOptions = {
  offset: new Vector3(-1, 5, 1.8),
  lookAt: new Vector3(0, 5, 20),
}

export class FirstPersonViewState extends FiniteState {
  name = CAMERA_STATE.FIRST_PERSON_VIEW

  public enter(prevState?: FiniteState): void {
    const fsm = this.fsm as CameraFSM
  }

  public exit(): void {}

  public update(dt: number): void {
    const fsm = this.fsm as CameraFSM
    const proxy = fsm.proxy

    if (!proxy.cameraTarget) {
      return
    }

    if (proxy.settings.isOrbitControlsEnabled) {
      return
    }

    this.updateCameraPosition(
      dt,
      {
        position: proxy.cameraTarget.position,
        quaternion: proxy.cameraTarget.quaternion,
      },
      proxy.camera,
      proxy.cameraState.offset,
      proxy.cameraState.lookAt,
    )

    if (fsm.proxy.input.keys.digit_1) {
      fsm.setState(CAMERA_STATE.THIRD_PERSON_VIEW)
    } else if (fsm.proxy.input.keys.digit_3) {
      fsm.setState(CAMERA_STATE.GLOBAL_VIEW)
    }
  }

  private updateCameraPosition(
    timeElapsed: number,
    cameraTarget: { position: Vector3; quaternion: Quaternion },
    camera: Camera,
    currentLookAt: Vector3,
    currentOffset: Vector3,
  ): void {
    const targetPosition = cameraTarget.position
    const targetQuaternion = cameraTarget.quaternion

    const constantOffset = cameraOptions.offset.clone()
    const constantLookAt = cameraOptions.lookAt.clone()

    constantLookAt.applyQuaternion(targetQuaternion)
    constantLookAt.add(targetPosition)
    constantLookAt.y = cameraOptions.lookAt.y

    constantOffset.applyQuaternion(targetQuaternion)
    constantOffset.add(targetPosition)
    constantOffset.y = cameraOptions.offset.y

    const t = 1.0 - Math.pow(0.01, timeElapsed)

    currentLookAt.copy(constantOffset)
    currentOffset.copy(constantLookAt)

    currentLookAt.y = cameraOptions.offset.y
    currentLookAt.y = cameraOptions.offset.y

    camera.position.copy(currentLookAt)
    camera.lookAt(currentOffset)
  }

  private calculateIdealOffset(
    position: Vector3,
    quaternion: Quaternion,
  ): Vector3 {
    const idealOffset = cameraOptions.offset.clone()

    idealOffset.applyQuaternion(quaternion)
    idealOffset.add(position)

    return idealOffset
  }

  private calculateIdealLookAt(
    position: Vector3,
    quaternion: Quaternion,
  ): Vector3 {
    const idealLookAt = cameraOptions.lookAt.clone()
    idealLookAt.applyQuaternion(quaternion)
    idealLookAt.add(position)

    return idealLookAt
  }
}
