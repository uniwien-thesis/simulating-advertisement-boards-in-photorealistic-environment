import { Vector3, Camera, Quaternion, Group } from 'three'
import { CAMERA_STATE } from '.'
import { FiniteState } from '../../fsm/finite.state'
import { toThreeQuat, toThreeVec3 } from '../../utils'
import { CameraFSM } from '../camera.fsm'

const cameraOptions = {
  offset: new Vector3(-2, 2, -6),
  lookAt: new Vector3(0, 5, 20),
}
const cameraCarOptions = {
  offset: new Vector3(0, 4, 6),
  lookAt: new Vector3(0, 4, -10),
}

export class ThirdPersonViewState extends FiniteState {
  name = CAMERA_STATE.THIRD_PERSON_VIEW

  public enter(prevState?: FiniteState): void {
    const fsm = this.fsm as CameraFSM
  }

  public exit(): void {}

  public update(dt: number): void {
    const fsm = this.fsm as CameraFSM
    const proxy = fsm.proxy
    const target = proxy.getCameraTarget?.()

    if (!target) {
      debugger
      return
    }

    if (proxy.settings.isOrbitControlsEnabled) {
      debugger
      return
    }

    this.updateCameraPosition(
      dt,
      target,
      proxy.camera,
      proxy.cameraState.offset,
      proxy.cameraState.lookAt,
    )

    if (fsm.proxy.input.keys.digit_2) {
      fsm.setState(CAMERA_STATE.FIRST_PERSON_VIEW)
    } else if (fsm.proxy.input.keys.digit_3) {
      fsm.setState(CAMERA_STATE.GLOBAL_VIEW)
    }
  }

  public followCar(
    timeElapsed: number,
    cameraTarget: Group,
    camera: Camera,
    currentLookAt: Vector3,
    currentOffset: Vector3,
  ): void {
    const targetPosition = cameraTarget.position.clone()
    const targetQuaternion = cameraTarget.quaternion.clone()

    const constantOffset = cameraCarOptions.offset.clone()
    const constantLookAt = cameraCarOptions.lookAt.clone()

    const defaultRotation = new Quaternion().setFromAxisAngle(
      new Vector3(0, 1, 0),
      Math.PI,
    )

    constantLookAt.applyQuaternion(defaultRotation)
    constantLookAt.applyQuaternion(targetQuaternion)

    constantOffset.applyQuaternion(defaultRotation)
    constantOffset.applyQuaternion(targetQuaternion)

    constantOffset.add(targetPosition)
    constantLookAt.add(targetPosition)
    const t = 1.0 - Math.pow(0.01, timeElapsed)

    camera.position.lerp(constantOffset, t)
    camera.lookAt(constantLookAt)
  }

  public followCharacter(
    timeElapsed: number,
    cameraTarget: { position: Vector3; quaternion: Quaternion },
    camera: Camera,
    currentLookAt: Vector3,
    currentOffset: Vector3,
  ): void {
    const targetPosition = cameraTarget.position.clone()
    const targetQuaternion = cameraTarget.quaternion.clone()
    const constantOffset = cameraOptions.offset.clone()
    const constantLookAt = cameraOptions.lookAt.clone()

    constantLookAt.applyQuaternion(targetQuaternion)
    constantLookAt.add(targetPosition)

    constantOffset.applyQuaternion(targetQuaternion)
    constantOffset.add(targetPosition)

    const t = 1.0 - Math.pow(0.001, timeElapsed)

    // currentLookAt.lerp(constantOffset, t)
    // currentOffset.lerp(constantLookAt, t)

    camera.position.copy(constantOffset)
    camera.lookAt(constantLookAt)
  }

  private updateCameraPosition(
    timeElapsed: number,
    cameraTarget: { model: Group; type: string },
    camera: Camera,
    currentLookAt: Vector3,
    currentOffset: Vector3,
  ): void {
    if (cameraTarget.type === 'car') {
      this.followCar(
        timeElapsed,
        cameraTarget.model,
        camera,
        currentLookAt,
        currentOffset,
      )
    } else {
      this.followCharacter(
        timeElapsed,
        cameraTarget.model,
        camera,
        currentLookAt,
        currentOffset,
      )
    }
  }

  private calculateIdealLookAt(
    position: Vector3,
    quaternion: Quaternion,
  ): Vector3 {
    const idealLookAt = cameraOptions.lookAt.clone()
    idealLookAt.applyQuaternion(quaternion)
    idealLookAt.add(position)

    return idealLookAt
  }
}
