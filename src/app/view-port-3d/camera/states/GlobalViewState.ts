import { Entity } from 'src/app/base/Entity'
import { FiniteState } from 'src/app/base/FiniteState'
import { PlayerInput } from 'src/app/player/PlayerInput'
import { FiniteStateUpdateOptions } from 'src/app/types'
import { Camera, Vector3 } from 'three'
import { CAMERA_STATE } from '.'
import { CameraFSM } from '../CameraFSM'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'

export class GlobalViewState extends FiniteState {
  name = CAMERA_STATE.GLOBAL_VIEW

  public enter(prevState?: FiniteState): void {}

  public exit(): void {}

  private updateCameraPosition(
    timeElapsed: number,
    cameraTarget: Entity,
    camera: Camera,
    currentLookAt: Vector3,
    currentOffset: Vector3,
  ): void {
    const { offsetAndLookAt } = this.parent.proxy
    if (!offsetAndLookAt) {
      return
    }
    const t = 1.0 - Math.pow(0.01, timeElapsed)

    const map = offsetAndLookAt[this.name]
    currentLookAt.lerp(map.lookAt, t)
    currentOffset.lerp(map.offset, t)

    camera.position.copy(currentOffset)
    camera.lookAt(currentLookAt)
  }

  public update(
    timeElapsed: number,
    options: FiniteStateUpdateOptions,
  ): void {
    const { cameraTarget } = options
    const input = options.input as PlayerInput

    this.updateCameraPosition(
      timeElapsed,
      cameraTarget!,
      this.parent.proxy.camera!,
      this.parent.proxy.currentLookAt!,
      this.parent.proxy.currentOffset!,
    )

    if (input.keys.digit_1) {
      this.parent.setState(CAMERA_STATE.THIRD_PERSON_VIEW)
    } else if (input.keys.digit_2) {
      this.parent.setState(CAMERA_STATE.FIRST_PERSON_VIEW)
    }
  }
}
