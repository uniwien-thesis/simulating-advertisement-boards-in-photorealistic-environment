import { Vector3 } from 'three'
import { CAMERA_STATE } from '.'
import { FiniteState } from '../../fsm/finite.state'
import { CameraFSM } from '../camera.fsm'

export class IntroState extends FiniteState {
  name = CAMERA_STATE.INTRO

  public enter(prevState?: FiniteState): void {}

  public exit(): void {}

  public update(dt: number): void {
    const fsm = this.fsm as CameraFSM
    const proxy = fsm.proxy

    if (!proxy.cameraTarget) {
      return
    }

    if (proxy.settings.isOrbitControlsEnabled) {
      return
    }

    proxy.camera.lookAt(new Vector3())
  }
}
