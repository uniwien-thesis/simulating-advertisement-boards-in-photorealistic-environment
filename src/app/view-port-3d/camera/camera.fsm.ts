import { Camera, Vector, Vector3 } from 'three'
import { FiniteStateMachine } from '../fsm/fsm'
import { KeyboardInput } from '../input/keyboard.input'
import { CameraManager } from './camera-manager'
import { CAMERA_STATE } from './states'
import { FirstPersonViewState } from './states/first-person-view.state'
import { ThirdPersonViewState } from './states/third-person-view.state'
import { IntroState } from './states/intro.state'
export const CAMERA_OFFSET_AND_LOOK_AT = {
  [CAMERA_STATE.THIRD_PERSON_VIEW]: {
    offset: new Vector3(-5, 8, -15),
    lookAt: new Vector3(0, 5, 20),
  },
  [CAMERA_STATE.FIRST_PERSON_VIEW]: {
    offset: new Vector3(-1, 5, 1.8),
    lookAt: new Vector3(0, 5, 20),
  },
  [CAMERA_STATE.GLOBAL_VIEW]: {
    offset: new Vector3(20, 50, -150),
    lookAt: new Vector3(0, 0, 0),
  },
}

export interface CameraFSMOptions {
  input: KeyboardInput
  camera: Camera
  lookAt: Vector3
  offset: Vector3
}

export class CameraFSM extends FiniteStateMachine {
  public proxy: CameraManager

  constructor(proxy: CameraManager) {
    super()
    this.proxy = proxy

    /**
     * add possible states
     */
    this.addState(
      CAMERA_STATE.THIRD_PERSON_VIEW,
      ThirdPersonViewState,
    )
    this.addState(
      CAMERA_STATE.FIRST_PERSON_VIEW,
      FirstPersonViewState,
    )
    this.addState(CAMERA_STATE.INTRO, IntroState)
  }
  update(dt: number) {
    super.update(dt)
  }
}
