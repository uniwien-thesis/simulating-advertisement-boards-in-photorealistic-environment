import { Mesh } from 'three'

export interface Object3D {
  mesh: Mesh
  body: Body
}
