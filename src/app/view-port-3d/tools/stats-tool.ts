import { ECSComponent } from '../ecs/component';
import * as Stats from 'stats.js';

export class StatsTool extends ECSComponent {
  stats: Stats = new Stats();

  constructor(elSelector: string) {
    super();
    const container = document.querySelector(elSelector);
    container?.appendChild(this.stats.dom);
  }

  public awake(): void {}

  public update(deltaTime: number): void {
    this.stats.end();
    this.stats.begin();
  }
}
