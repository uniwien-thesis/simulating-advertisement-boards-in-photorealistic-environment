import { FiniteStateMachine } from './fsm'

export abstract class FiniteState {
  public name: string | null = null
  public fsm: FiniteStateMachine

  constructor(fsm: FiniteStateMachine) {
    this.fsm = fsm
  }

  public abstract exit(): void
  public abstract enter(prevState?: FiniteState): void
  public abstract update(timeElapsed: number): void
}
