import { FiniteState } from './finite.state'

interface FiniteStateMap {
  [name: string]: FiniteStateClass
}

type FiniteStateClass = {
  new (parent: FiniteStateMachine): FiniteState
}

export abstract class FiniteStateMachine {
  private states: FiniteStateMap = {}
  public currentState: FiniteState | null = null

  public addState(key: string, type: FiniteStateClass) {
    this.states[key] = type
  }

  public setState(key: string) {
    const prevState = this.currentState

    if (prevState) {
      if (prevState.name === key) {
        return
      }
      prevState.exit()
    }

    try {
      const stateClass = this.states[key]
      const state = new stateClass(this)
      this.currentState = state
      state.enter(prevState!)
    } catch (e: any) {
      console.error(`NO FINITE STATE FOR NAME: ${key}`, e)
      debugger
    }
  }

  public update(timeElapsed: number) {
    if (this.currentState) {
      this.currentState.update(timeElapsed)
    }
  }
}
