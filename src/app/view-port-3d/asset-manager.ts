import {
  Group,
  LoadingManager,
  Texture,
  TextureLoader,
  VideoTexture,
} from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { FBXLoader } from 'three/examples/jsm/loaders/FBXLoader'
import { ReplaySubject } from 'rxjs'
import { PLAYER_STATES } from './character/states'

const RATIO_2X3 = '2x3'
const RATIO_5X2 = '5x2'
const RATIO_5X3 = '5x3'

interface LoadTargetPathOptions {
  absoluteAssetPath: string
  relativeFilePath: string
}

interface LoadBatchTargetPathOptions {
  absoluteAssetPath: string
  relativeFilePaths: string[]
}

export interface Assets {
  /**
   * Textures
   */
  combinedTexturesMap: ReplaySubject<Map<string, Texture>>
  adsInitialTexturesMap: ReplaySubject<Map<string, Texture>>
  videoTextures: ReplaySubject<Map<number, VideoTexture>>
  lightmapTexture: ReplaySubject<Texture>
  introTexture?: Texture
  /**
   * Models
   */
  levelVisual: ReplaySubject<Group>
  adsVisual: ReplaySubject<Group>
  levelPhysics: ReplaySubject<Group>
  carVisual: ReplaySubject<Group>
  characterVisual: ReplaySubject<Group>
  characterAnimation: ReplaySubject<Map<string, Group>>
}

function tryCatch(func: () => void) {
  try {
    func()
  } catch (e) {
    console.error('!!! EXCEPTION !!!')
    console.error(e)
  }
}

/**
 * @description
 * Manages all upload of scene assets
 * and takes responsibility for the loading state
 */
export class AssetManager {
  private loadingManager = new LoadingManager(
    // loaded
    () => this.onLoaded(),
    // progress
    (_name, currentIndex, total) => {
      this.progress$.next(currentIndex / total)
    },
  )

  public textureLoader = new TextureLoader(this.loadingManager)
  public gltfLoader = new GLTFLoader(this.loadingManager)
  public fbxLoader = new FBXLoader(this.loadingManager)

  public loaded$ = new ReplaySubject<void>()
  public progress$ = new ReplaySubject<number>()
  public assets: Assets = {
    combinedTexturesMap: new ReplaySubject<Map<string, Texture>>(),
    adsInitialTexturesMap: new ReplaySubject<Map<string, Texture>>(),
    videoTextures: new ReplaySubject<Map<number, VideoTexture>>(),
    lightmapTexture: new ReplaySubject<Texture>(),
    introTexture: undefined,
    levelVisual: new ReplaySubject<Group>(),
    levelPhysics: new ReplaySubject<Group>(),
    carVisual: new ReplaySubject<Group>(),
    characterVisual: new ReplaySubject<Group>(),
    characterAnimation: new ReplaySubject<Map<string, Group>>(),
    adsVisual: new ReplaySubject<Group>(),
  }
  public videoElements: HTMLVideoElement[] = []

  private onLoaded(): void {
    console.log('loaded')
    this.loaded$.next()
  }

  public loadLevelVisualModel(): void {
    this.gltfLoader.setPath('assets/models/city/')
    tryCatch(() => {
      this.gltfLoader.load('model-city.glb', (gltf) => {
        this.assets.levelVisual.next(gltf.scene)
      })
    })
  }

  public loadLevelPhysicsModel() {
    this.gltfLoader.setPath('assets/models/city/')

    tryCatch(() => {
      this.gltfLoader.load('model-phys.glb', (gltf) => {
        this.assets.levelPhysics.next(gltf.scene)
      })
    })
  }

  public loadCarVisualModel() {
    this.gltfLoader.setPath('assets/models/city/')

    tryCatch(() => {
      this.gltfLoader.load('car.glb', (gltf) => {
        this.assets.carVisual.next(gltf.scene)
      })
    })
  }

  public loadAdsVisualModel() {
    this.gltfLoader.setPath('assets/models/city/')

    tryCatch(() => {
      this.gltfLoader.load('model-ads.glb', (gltf) => {
        this.assets.adsVisual.next(gltf.scene)
      })
    })
  }

  public loadCharacterVisualModel() {
    this.fbxLoader.setPath('assets/models/character/guard/')

    tryCatch(() => {
      this.fbxLoader.load('castle_guard_01.fbx', (character) => {
        this.assets.characterVisual.next(character)
      })
    })

    /**
     * Load animations
     */
    const animations = new Map<string, Group>()

    this.fbxLoader.load('Sword And Shield Idle.fbx', (idle) => {
      this.fbxLoader.load('Sword And Shield Run.fbx', (run) => {
        this.fbxLoader.load('Sword And Shield Walk.fbx', (walk) => {
          animations.set(PLAYER_STATES.IDLE, idle)
          animations.set(PLAYER_STATES.RUN, run)
          animations.set(PLAYER_STATES.WALK, walk)

          this.assets.characterAnimation.next(animations)
        })
      })
    })
  }

  public loadBakedTextures(): void {
    const TEXTURE_COUNT = 43

    const textures = new Map<string, Texture>()
    this.textureLoader.setPath('assets/textures/baked/')
    for (let i = 1; i <= TEXTURE_COUNT; i++) {
      const key = `part-${i}`
      const textureName = `${key}_Bake1_cyclesbake_COMBINED.jpg`

      tryCatch(() => {
        this.textureLoader.load(textureName, (texture) => {
          textures.set(key, texture)
        })
      })

      this.assets.combinedTexturesMap.next(textures)
    }
  }

  public loadLightMapTextures(): void {
    this.textureLoader.setPath('assets/textures/lightmaps/')
    const path = `lightmap.jpg`

    this.textureLoader.load(path, (texture) =>
      this.assets.lightmapTexture.next(texture),
    )
  }

  public loadAdsTemplateTextures(): void {
    const textures = new Map<string, Texture>()

    const names = [
      { type: RATIO_5X2, path: 'advertisement-5x2.jpg' },
      { type: RATIO_5X3, path: 'advertisement-5x3.jpg' },
      { type: RATIO_2X3, path: 'advertisement-2x3.jpg' },
    ]

    this.textureLoader.setPath('assets/textures/ads/templates/')
    for (let i = 0; i < names.length; i++) {
      const item = names[i]
      const path = `${item.path}`
      this.textureLoader.load(path, (texture) => {
        textures.set(item.type, texture)
        if (i === 2) {
          this.assets.adsInitialTexturesMap.next(textures)
        }
      })
    }
  }

  public loadExampleVideos(): void {
    const videoTextures = new Map<number, VideoTexture>()
    const key = 'example-video'
    const path = 'assets/textures/videos/'

    for (let i = 1; i < 7; i++) {
      const video = document.createElement('video')
      const url = `${path}${key}-${i}.mp4`
      video.src = url
      video.loop = true

      const texture = new VideoTexture(video)

      videoTextures.set(i, texture)
      this.videoElements.push(video)
    }

    this.assets.videoTextures.next(videoTextures)
  }

  public playAllVideos(): void {
    this.videoElements.forEach((el) => el.play())
  }

  public loadTexture(
    options: LoadTargetPathOptions,
  ): Promise<Texture> {
    this.textureLoader.setPath(options.absoluteAssetPath)
    return this.textureLoader.loadAsync(options.relativeFilePath)
  }
}
