import { GUI } from 'dat.gui'
import {
  BackSide,
  Color,
  DirectionalLight,
  DirectionalLightHelper,
  HemisphereLight,
  HemisphereLightHelper,
  Mesh,
  ShaderMaterial,
  SphereGeometry,
  Vector3,
} from 'three'
import { Character } from '../character/character'
import { CharacterPhysics } from '../character/character.physics'
import { ECSComponent } from '../ecs/component'
import { Params } from '../ecs/types'

export class Sky extends ECSComponent {
  hemisphereLight?: HemisphereLight
  hemisphereLightHelper?: HemisphereLightHelper

  directionalLight?: DirectionalLight
  directionalLightHelper?: DirectionalLightHelper

  settings = {
    HSL: new Vector3(0.1, 1, 0.95),
    position: new Vector3(-1, 1.75, 1),
    // positionScalar: 30,
    d: 200,
    far: 3500,
    bias: -0.0001,
  }

  constructor(options: Params) {
    super(options)
  }

  public awake(): void {
    /**
     * Hemisphere light
     */
    this.hemisphereLight = new HemisphereLight(
      0xffffff,
      0xffffff,
      0.6,
    )
    this.hemisphereLight.color.setHSL(0.6, 1, 0.6)
    this.hemisphereLight.groundColor.setHSL(0.095, 1, 0.75)
    this.hemisphereLight.position.set(0, 50, 0)

    this.hemisphereLightHelper = new HemisphereLightHelper(
      this.hemisphereLight,
      10,
    )
    this.options?.scene?.add(this.hemisphereLight)
    this.options?.scene?.add(this.hemisphereLightHelper)

    /**
     * Directional light
     */
    this.directionalLight = new DirectionalLight(0xffffff, 2)
    this.directionalLight.color.setHSL(
      this.settings.HSL.x,
      this.settings.HSL.y,
      this.settings.HSL.z,
    )
    this.directionalLight.position.set(57, 58, 39)

    // this.directionalLight.position.multiplyScalar(
    //   this.settings.positionScalar,
    // )

    this.directionalLight.castShadow = true

    this.directionalLight.shadow.mapSize.width = 2048
    this.directionalLight.shadow.mapSize.height = 2048

    this.directionalLight.shadow.camera.left = -this.settings.d
    this.directionalLight.shadow.camera.right = this.settings.d
    this.directionalLight.shadow.camera.top = this.settings.d
    this.directionalLight.shadow.camera.bottom = -this.settings.d

    this.directionalLight.shadow.camera.far = this.settings.far
    this.directionalLight.shadow.bias = this.settings.bias

    // this.directionalLightHelper = new DirectionalLightHelper(
    //   this.directionalLight,
    //   10,
    // )
    this.options?.scene?.add(this.directionalLight)
    // this.options?.scene?.add(this.directionalLightHelper)

    /**
     * Sky dome
     */
    const vertexShader = `varying vec3 vWorldPosition;
        void main() {

            vec4 worldPosition = modelMatrix * vec4( position, 1.0 );
            vWorldPosition = worldPosition.xyz;

            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
        }
    `

    const fragmentShader = `uniform vec3 topColor;
        uniform vec3 bottomColor;
        uniform float offset;
        uniform float exponent;
    
        varying vec3 vWorldPosition;
    
        void main() {
    
            float h = normalize( vWorldPosition + offset ).y;
            gl_FragColor = vec4( mix( bottomColor, topColor, max( pow( max( h , 0.0), exponent ), 0.0 ) ), 1.0 );
        }
    `
    const uniforms = {
      topColor: { value: new Color(0x0077ff) },
      bottomColor: { value: new Color(0xffffff) },
      offset: { value: 33 },
      exponent: { value: 0.6 },
    }
    uniforms['topColor'].value.copy(this.hemisphereLight.color)

    this.options?.scene?.fog?.color.copy(
      uniforms['bottomColor'].value,
    )

    const skyGeo = new SphereGeometry(4000, 32, 15)
    const skyMat = new ShaderMaterial({
      uniforms: uniforms,
      vertexShader: vertexShader,
      fragmentShader: fragmentShader,
      side: BackSide,
    })

    const sky = new Mesh(skyGeo, skyMat)
    this.options?.scene?.add(sky)
  }

  public update(deltaTime: number): void {}
}
