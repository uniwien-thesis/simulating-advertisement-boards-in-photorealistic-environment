import { combineLatest } from 'rxjs'
import { take } from 'rxjs/operators'
import {
  Color,
  Group,
  Mesh,
  MeshBasicMaterial,
  ShadowMaterial,
  sRGBEncoding,
  Texture,
  TextureLoader,
} from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { AssetManager } from '../asset-manager'
import { ECSComponent } from '../ecs/component'
import { Params } from '../ecs/types'

interface LevelControllerOptions extends Params {
  assetManager: AssetManager
}

export const RATIO_2X3 = '2x3'
export const RATIO_5X2 = '5x2'
export const RATIO_5X3 = '5x3'

export class LevelController extends ECSComponent {
  defaultWireframeMaterial = new MeshBasicMaterial({
    color: new Color(0xff0000),
  })

  textures: { [key: string]: Texture }[] = []

  textureLoader = new TextureLoader()
  gltfLoader = new GLTFLoader()

  constructor(options: LevelControllerOptions) {
    super()

    this.options = options

    combineLatest([
      this.options.assetManager?.assets.levelVisual,
      this.options.assetManager?.assets.combinedTexturesMap,
    ])
      .pipe(take(1))
      // @ts-ignore
      .subscribe(([group, combinedTexturesMap]) => {
        // @ts-ignore
        this.setUpModel(group, combinedTexturesMap)
      })
  }

  public awake(): void {}

  public update(deltaTime: number): void {}

  private setUpModel(
    group: Group,
    combinedTexturesMap: Map<string, Texture>,
  ): void {
    const shadowGroup = new Group()
    group.traverse((child) => {
      if (child instanceof Mesh) {
        const key = child.name
        const texture = combinedTexturesMap.get(key)
        if (!texture) {
          return
        }

        // TODO: UNCOMMENT
        const shadowMesh = child.clone()
        const shadowMaterial = new ShadowMaterial()
        shadowMaterial.opacity = 0.2
        shadowMesh.material = shadowMaterial
        shadowMesh.receiveShadow = true
        shadowGroup.add(shadowMesh)

        const bakedMaterial = new MeshBasicMaterial({
          map: texture,
        })
        child.material = bakedMaterial
        child.material.map.flipY = false

        if (child.material && child.material.map) {
          child.material.map.encoding = sRGBEncoding
        }
      }
    })

    this.options?.scene?.add(group)
    // this.options?.scene?.add(shadowGroup)
  }
}
