import {
  BufferAttribute,
  Color,
  Mesh,
  MeshBasicMaterial,
  Texture,
  Raycaster,
  Vector3,
  VideoTexture,
  Group,
} from 'three'
import { ECSComponent } from '../ecs/component'
import { UV_MAP_BY_NAME } from './uv-map'
import { GUI } from 'dat.gui'
import { Params } from '../ecs/types'
import { take, tap, map } from 'rxjs/operators'
import { combineLatest } from 'rxjs'
import { setCustomUvs } from '../utils/setCustomUvs'
import { removeFolderFromGUI } from '../utils/datgui'

export class AdsController extends ECSComponent {
  raycaster = new Raycaster()
  selectedBanner?: Mesh
  banners: Mesh[] = []
  groupOfBanners = new Group()
  videoTextures?: Map<number, VideoTexture>

  constructor(options: Params) {
    super(options)

    combineLatest([
      this.options?.assetManager?.assets.adsInitialTexturesMap,
      this.options?.assetManager?.assets.lightmapTexture,
      this.options?.assetManager?.assets.adsVisual,
      this.options?.assetManager?.assets.videoTextures,
    ])
      .pipe(take(1))

      .subscribe((assets) => {
        const [
          // @ts-ignore
          initialTextures,
          // @ts-ignore
          lightMapTexture,
          // @ts-ignore
          adsVisual,
          // @ts-ignore
          videoTextures,
        ] = assets

        this.videoTextures = videoTextures

        // @ts-ignore
        this.groupOfBanners = adsVisual
        this.setUpModel(
          // @ts-ignore
          initialTextures,
          lightMapTexture,
          this.groupOfBanners,
        )
      })
  }

  public awake(): void {
    const inputEl = document.querySelector('#texture-upload')
    if (!inputEl) {
      return
    }


    inputEl.addEventListener('input', (event: Event) =>
      this.onTextureLoad(event),
    )

    document.addEventListener('click', (event) =>
      this.onADSMeshSelect(event),
    )
  }

  async onTextureLoad(event: Event): Promise<void> {
    const target = event.target as HTMLInputElement
    const files = target.files
    if (!files!.length) {
      return
    }

    const file = files![0]
    const { type } = file

    const fileUrl = URL.createObjectURL(file)
    let texture: Texture | VideoTexture

    if (type.includes('image')) {
      this.options?.assetManager?.textureLoader.setPath('')
      texture = await this.options?.assetManager?.textureLoader.loadAsync(
        fileUrl,
      )!
    } else {
      var video = document.createElement('video')
      video.src = fileUrl
      video.loop = true
      video.play()

      texture = new VideoTexture(video)
    }

    // @ts-ignore
    this.selectedBanner!.material.map = texture
    // @ts-ignore
    this.selectedBanner!.material.map.needsUpdate = true
    // @ts-ignore
    this.selectedBanner!.material.needsUpdate = true
  }

  onADSMeshSelect(event: MouseEvent) {
    const mouse3D = new Vector3(
      (event.clientX / window.innerWidth) * 2 - 1,
      -(event.clientY / window.innerHeight) * 2 + 1,
      0.5,
    )

    this.raycaster.setFromCamera(
      mouse3D,
      this.options?.cameraManager?.camera!,
    )
    var intersects = this.raycaster.intersectObjects(this.banners)

    if (!intersects.length) {
      return
    }

    const mesh = intersects[0].object as Mesh

    if (mesh) {
      if (
        this.selectedBanner &&
        mesh.name !== this.selectedBanner.name
      ) {
        removeFolderFromGUI(
          this.selectedBanner.name,
          this.options?.gui!,
        )
      }

      this.selectedBanner = mesh

      const doesFolderWithGivenNameExist = this.options?.gui!
        .__folders[this.selectedBanner!.name]

      if (!doesFolderWithGivenNameExist) {
        /**
         * Create a folder for a selected mesh
         */
        const selectedObjectFolder = this.options?.gui!.addFolder(
          this.selectedBanner!.name,
        )

        selectedObjectFolder!
          .add(this, 'importVideo')
          .name('import video/image')
        selectedObjectFolder!
          .add(this, 'setVideo1ToSelectedMesh')
          .name('video 1')
        selectedObjectFolder!
          .add(this, 'setVideo2ToSelectedMesh')
          .name('video 2')
        selectedObjectFolder!
          .add(this, 'setVideo3ToSelectedMesh')
          .name('video 3')
        selectedObjectFolder!
          .add(this, 'setVideo4ToSelectedMesh')
          .name('video 4')
        selectedObjectFolder!
          .add(this, 'setVideo5ToSelectedMesh')
          .name('video 5')
        selectedObjectFolder!
          .add(this, 'setVideo6ToSelectedMesh')
          .name('video 6')
      }
    } else {
      if (this.selectedBanner) {
        removeFolderFromGUI(
          this.selectedBanner.name,
          this.options?.gui!,
        )
      }

      this.selectedBanner = undefined
    }
  }

  importVideo() {
    const input = document.querySelector(
      '#texture-upload',
    ) as HTMLInputElement

    input.click()
  }

  setVideoToSelectedMesh(videoIndex: number) {
    // @ts-ignore
    if (!this.selectedBanner?.material.map) {
      debugger
      return
    }

    const videoTexture = this.videoTextures?.get(videoIndex)
    // @ts-ignore
    this.selectedBanner.material.map = videoTexture
    // @ts-ignore
    this.selectedBanner.material.needsUpdate = true
  }

  setVideo1ToSelectedMesh() {
    this.setVideoToSelectedMesh(1)
  }
  setVideo2ToSelectedMesh() {
    this.setVideoToSelectedMesh(2)
  }
  setVideo3ToSelectedMesh() {
    this.setVideoToSelectedMesh(3)
  }
  setVideo4ToSelectedMesh() {
    this.setVideoToSelectedMesh(4)
  }
  setVideo5ToSelectedMesh() {
    this.setVideoToSelectedMesh(5)
  }
  setVideo6ToSelectedMesh() {
    this.setVideoToSelectedMesh(6)
  }

  getInitialADSTextureMap(
    name: string,
    textures: Map<string, Texture>,
  ): Texture {
    const ratioMatch = name.match(/\dx\d/)
    const type = ratioMatch![0]
    const texture = textures.get(type)

    return texture!
  }

  public update(deltaTime: number): void {}

  setUpModel(
    initialTextures: Map<string, Texture>,
    lightMapTexture: Texture,
    group: Group,
  ): void {
    group.traverse((child) => {
      if (!group) {
        debugger
      }

      if (!(child instanceof Mesh)) {
        return
      }

      if (!child.name.includes('ads')) {
        debugger
      }

      /**
       * get lightmapTexture atlas for mesh
       */
      const lightmapTexture = lightMapTexture
      if (!lightmapTexture) {
        debugger
        return
      }

      /**
       * get initial frame texture
       */
      const texture = this.getInitialADSTextureMap(
        child.name,
        initialTextures,
      )

      lightmapTexture.flipY = false
      const material = new MeshBasicMaterial({
        color: new Color(0xffffff),
        map: texture,
        // @ts-ignore
        lightMap: lightmapTexture,
      })
      child.material = material

      // set secondary UV
      const uvs = child.geometry.attributes.uv.clone().array
      child.geometry.setAttribute('uv2', new BufferAttribute(uvs, 2))

      this.setPrimaryUvs(child)

      child.geometry.uvsNeedUpdate = true

      this.banners.push(child)
    })
    this.options?.scene?.add(group)
  }

  setPrimaryUvs(mesh: Mesh) {
    // @ts-ignore
    const uvs = UV_MAP_BY_NAME[mesh.name]
    try {
      setCustomUvs(mesh, uvs)
    } catch (e) {
      debugger
    }
  }
}
