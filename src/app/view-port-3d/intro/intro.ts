import { delay, take, takeUntil, takeWhile } from 'rxjs/operators'
import { gsap } from 'gsap'
import {
  Camera,
  Group,
  Mesh,
  NearestFilter,
  PlaneGeometry,
  Raycaster,
  Scene,
  ShaderMaterial,
  Vector3,
} from 'three'

import { AssetManager } from '../asset-manager'
import { ECSEntity } from '../ecs/entity'
import { fromEvent, Subject } from 'rxjs'
import { CameraManager } from '../camera/camera-manager'
import { PLAYER_STATES } from '../character/states'
import { CAMERA_STATE } from '../camera/states'

export class Intro extends ECSEntity {
  destroyed$ = new Subject<void>()
  // takeUntil(this.destroyedSubject),

  private scene: Scene
  private raycaster = new Raycaster()
  private assetManager: AssetManager

  private overlayGeometry = new PlaneGeometry(2, 2, 1, 1)
  private boxGeometry = new PlaneGeometry(0.5, 0.3, 1, 1)

  /** Meshes */
  private progress?: Mesh
  private overlay?: Mesh
  private bg?: Mesh
  private border?: Mesh
  private text?: Mesh
  cameraManager: CameraManager

  private percent?: number = 0

  constructor(
    scene: Scene,
    cameraManager: CameraManager,
    assetManager: AssetManager,
  ) {
    super()

    this.scene = scene
    this.assetManager = assetManager
    this.cameraManager = cameraManager

    this.assetManager.progress$
      .pipe(takeWhile((value) => value < 1))
      .subscribe((value) => {
        // @ts-ignore
        gsap.to(this.progress?.material.uniforms.progress, {
          duration: 1.5,
          ease: 'expo.inOut',
          value,
        })
      })

    this.assetManager.loaded$
      .pipe(take(1), delay(1500))
      .subscribe((_) => {
        const options = {
          duration: 1,
          value: 0.0,
          ease: 'expo.inOut',
        }
        // @ts-ignore
        gsap.to(this.overlay.material.uniforms.uAlpha, { ...options })
        // @ts-ignore
        gsap.to(this.border.material.uniforms.uAlpha, { ...options })
        // @ts-ignore
        gsap.to(this.progress.material.uniforms.uAlpha, {
          ...options,
          duration: 0.8,
        })
        // @ts-ignore
        gsap.to(this.text.material.uniforms.uAlpha, { ...options })

        fromEvent(document, 'mousemove')
          .pipe(takeUntil(this.destroyed$))
          .subscribe((event) =>
            this.intersectText(event as MouseEvent),
          )
        fromEvent(document, 'click')
          .pipe(takeUntil(this.destroyed$))
          .subscribe((event) => this.onClick(event as MouseEvent))
      })
  }

  public onClick(event: MouseEvent) {
    if (this.hasIntersectionWithButton(event)) {
      this.cameraManager.fsm.setState(CAMERA_STATE.THIRD_PERSON_VIEW)
      this.assetManager.playAllVideos()

      setTimeout(() => {
        this.destroyed$.next()
        this.destroyed$.complete()
        this.scene.remove(this.overlay!)
        this.scene.remove(this.border!)
        this.scene.remove(this.progress!)
        this.scene.remove(this.text!)
        this.scene.remove(this.bg!)
      }, 500)
    }
  }

  public hasIntersectionWithButton(event: MouseEvent): boolean {
    const mouse3D = new Vector3(
      (event.clientX / window.innerWidth) * 2 - 1,
      -(event.clientY / window.innerHeight) * 2 + 1,
      0.5,
    )

    this.raycaster.setFromCamera(mouse3D, this.cameraManager.camera)
    const intersects = this.raycaster.intersectObjects([this.text!])

    if (!intersects.length) {
      return false
    }
    return true
  }

  intersectText(event: MouseEvent): any {
    const mouse3D = new Vector3(
      (event.clientX / window.innerWidth) * 2 - 1,
      -(event.clientY / window.innerHeight) * 2 + 1,
      0.5,
    )

    this.raycaster.setFromCamera(mouse3D, this.cameraManager.camera)
    var intersects = this.raycaster.intersectObjects([this.text!])

    if (!intersects.length) {
      // @ts-ignore
      gsap.to(this.text.material.uniforms.uScale, {
        value: 1.0,
        duration: 0.3,
      })

      return
    }

    // @ts-ignore
    gsap.to(this.text.material.uniforms.uScale, {
      value: 2.0,
      duration: 0.6,
    })
  }

  public awake(): void {
    super.awake()

    this.createBgMesh()
    this.createOverlayMesh()
    this.createBorderMesh()
    this.createProgressMesh()
    this.createTextMesh()
  }

  public update(deltaTime: number): void {
    super.update(deltaTime)
  }

  createBgMesh() {
    const vertexShader = `
      varying vec2 vUv;
      
      void main()
      {
        vUv = uv;
        gl_Position = vec4(position.x, position.y, 0.1 , 1.0);
      }
    `
    const fragmentShader = `
      varying vec2 vUv;
      uniform float uAlpha;
      uniform sampler2D uTexture;

      void main()
      {
          gl_FragColor = vec4(uAlpha);
      }
    `

    const bgMaterial = new ShaderMaterial({
      uniforms: { uAlpha: { value: 1 } },
      vertexShader,
      fragmentShader,
    })

    this.bg = new Mesh(this.overlayGeometry, bgMaterial)
    this.scene.add(this.bg)
  }

  public createOverlayMesh(): void {
    const vertexShader = `
      varying vec2 vUv;
      
      void main()
      {
        vUv = uv;
        gl_Position = vec4(position.x, position.y, 0.0 , 1.0);
      }
    `
    const fragmentShader = `
      varying vec2 vUv;
      uniform float uAlpha;
      uniform sampler2D uTexture;

      void main()
      {
          gl_FragColor = texture2D(uTexture, vUv);
          gl_FragColor.a = uAlpha;
      }
    `
    const uvs = [
      [0, 1], // top right
      [1, 1], // top left
      [0, 0.5], // bottom left
      [1, 0.5], // bottom right
    ]

    const overlayMaterial = new ShaderMaterial({
      uniforms: {
        uTexture: { value: this.assetManager.assets.introTexture },
        uAlpha: { value: 1 },
      },
      vertexShader,
      fragmentShader,
    })
    overlayMaterial.transparent = true

    this.overlay = new Mesh(this.overlayGeometry, overlayMaterial)
    setCustomUvs(this.overlay, uvs)
    this.scene.add(this.overlay)
  }

  public createBorderMesh(): void {
    const vertexShader = `
      varying vec2 coords2d;
      void main()
      {
        gl_Position = vec4(position.x, position.y, -0.1 , 1.0);
        gl_Position.y -= 0.5;

        coords2d = vec2(gl_Position.x, gl_Position.y);
      }
    `
    const fragmentShader = `
      varying vec2 coords2d;
      uniform float uAlpha;

      void main()
      {
        float top = -0.35;

        gl_FragColor = vec4(1, 1, 1, uAlpha);

        bool emptyHorizontalSpace = coords2d.x > -0.23 && coords2d.x < 0.23;
        bool emptyVerticalSpace = coords2d.y < top - 0.03 && coords2d.y > top - 0.27;
        bool isMasked = emptyHorizontalSpace && emptyVerticalSpace;
        
        if (isMasked) {
          gl_FragColor.a = 0.0;
        }
      }
    `
    // const uvs =
    const material = new ShaderMaterial({
      uniforms: {
        uAlpha: { value: 1 },
      },
      fragmentShader,
      vertexShader,
    })
    material.transparent = true
    this.border = new Mesh(this.boxGeometry, material)
    this.scene.add(this.border)
  }

  public createProgressMesh(): void {
    const vertexShader = `
      // varying vec2 vUv;
      varying vec2 coords2d;
      uniform float progress;

      void main()
      {

        // vUv = uv;
        gl_Position = vec4(position, 1.0);
        gl_Position.y -= 0.5;
        gl_Position.z = -0.2;
        float xOffset = (1.0 - progress) * 0.5;
        gl_Position.x = gl_Position.x - xOffset;
        coords2d = gl_Position.xy;
      }`
    const fragmentShader = `
      // varying vec2 vUv;
      varying vec2 coords2d;
      uniform sampler2D u_texture;
      uniform float uAlpha;

      void main()
      {
        float top = -0.35;

        bool emptyHorizontalSpace = coords2d.x > -0.23 && coords2d.x < 0.23;
        bool emptyVerticalSpace = coords2d.y < top - 0.03 && coords2d.y > top - 0.27;
        bool isMasked = emptyHorizontalSpace && emptyVerticalSpace;
        
        // gl_FragColor = texture2D(u_texture, vUv);
        gl_FragColor = vec4(1,0,0,uAlpha);

        if (!isMasked) {
          gl_FragColor.a = 0.0;
        }
      }`
    const uvs = [
      [0.5, 0.4], // top right
      [0.8, 0.4], // top left
      [0.8, 0.3], // bottom left
      [0.9, 0.3], // bottom right
    ]
    const progressMaterial = new ShaderMaterial({
      uniforms: {
        u_texture: { value: this.assetManager.assets.introTexture },
        uAlpha: { value: 1 },
        progress: { value: this.percent },
      },
      fragmentShader,
      vertexShader,
    })
    progressMaterial.transparent = true
    this.progress = new Mesh(
      this.boxGeometry.clone(),
      progressMaterial,
    )
    setCustomUvs(this.progress, uvs)
    // progress.material.transparent = true
    this.scene.add(this.progress)
  }

  public createTextMesh(): void {
    const vertexShader = `
    varying vec2 vUv;
    uniform float uAlpha;
    uniform float uScale;

    void main()
    {
      vUv = uv;
      gl_Position = vec4(position.x, position.y, -0.1 , 1.0);
      gl_Position.xy *= uScale;
    }`
    const fragmentShader = `
    varying vec2 vUv;
    uniform sampler2D u_texture;
    uniform float uAlpha;

    void main()
    {
        vec2 vUvWithOffset = vUv;
        vUvWithOffset.y -= (1.0 - uAlpha) * 0.1;

        gl_FragColor = texture2D(u_texture, vUvWithOffset);
        gl_FragColor.xyz = vec3(uAlpha);
    }`
    const uvs = [
      [0.1, 0.4], // top right
      [0.4, 0.4], // top left
      [0.1, 0.3], // bottom left
      [0.4, 0.3], // bottom right
    ]

    const textMaterial = new ShaderMaterial({
      uniforms: {
        u_texture: { value: this.assetManager.assets.introTexture },
        uAlpha: { value: 1 },
        uScale: { value: 1 },
      },
      vertexShader,
      fragmentShader,
    })
    this.text = new Mesh(this.boxGeometry, textMaterial)
    setCustomUvs(this.text, uvs)

    // @ts-ignore
    this.text.material.transparent = true
    this.scene.add(this.text)
  }
}

function setCustomUvs(mesh: Mesh, uvs: number[][]) {
  const uvAttr = mesh.geometry.attributes.uv

  // @ts-ignore
  mesh.geometry.uvsNeedUpdate = true

  for (let i = 0; i < uvAttr.count; i++) {
    const arr = Array.prototype.slice.call(
      mesh.geometry.attributes.position.array,
    )
    uvAttr.setXY(i, uvs[i][0], uvs[i][1])
  }
  uvAttr.needsUpdate = true
}
