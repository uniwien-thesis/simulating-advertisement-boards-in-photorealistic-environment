import { Vector3 as ThreeVec, Quaternion as ThreeQuat } from 'three'
import {
  Vec3 as CannonVec,
  Quaternion as CannonQuat,
} from 'cannon-es'

export function toCannonVec3(v: ThreeVec): CannonVec {
  return new CannonVec(v.x, v.y, v.z)
}

export function toThreeVec3(v: CannonVec): ThreeVec {
  return new ThreeVec(v.x, v.y, v.z)
}

export function toThreeQuat(quat: CannonQuat): ThreeQuat {
  return new ThreeQuat(quat.x, quat.y, quat.z, quat.w)
}

export function toCannonQuat(quat: ThreeQuat): CannonQuat {
  return new CannonQuat(quat.x, quat.y, quat.z, quat.w)
}

export function setCannonVecToThreeVec(
  value?: CannonVec,
  target?: ThreeVec,
): void {
  if (!value || !target) {
    return
  }
  target.set(value.x, value.y, value.z)
}

export function setThreeVecToCannonVec(
  value?: ThreeVec,
  target?: CannonVec,
): void {
  if (!value || !target) {
    return
  }
  target.set(value.x, value.y, value.z)
}

export function setCannonQuatToThreeQuat(
  value?: CannonQuat,
  target?: ThreeQuat,
): void {
  if (!value || !target) {
    return
  }

  target.set(value.x, value.y, value.z, value.w)
}

export function setThreeQuatToCannonQuat(
  value?: ThreeQuat,
  target?: CannonQuat,
): void {
  if (!value || !target) {
    return
  }

  target.set(value.x, value.y, value.z, value.w)
}
