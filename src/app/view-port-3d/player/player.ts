import { ECSEntity } from '../ecs/entity'

export class Player extends ECSEntity {
  public isPlayerInVehicle = false

  public awake(): void {
    super.awake()
  }

  public update(dt: number): void {
    super.update(dt)
  }
}
