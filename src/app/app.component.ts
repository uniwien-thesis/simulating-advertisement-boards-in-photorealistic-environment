import { from } from 'rxjs'
import { Component, OnInit } from '@angular/core'

import { Application } from './view-port-3d'
import { AssetManager } from './view-port-3d/asset-manager'
import { take } from 'rxjs/operators'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'thesis'

  application = new Application()
  assetManager = new AssetManager()

  constructor() {
    this.application.assetManager = this.assetManager

    // fetch textures for intro state
    from(
      this.assetManager.loadTexture({
        absoluteAssetPath: 'assets/textures/',
        relativeFilePath: 'loading-bg-atlas.png',
      }),
    )
      .pipe(take(1))
      .subscribe((textureAtlas) => {
        console.log('[*] initial awake')

        this.assetManager.assets.introTexture = textureAtlas
        this.application.awake()
      })
  }

  ngOnInit(): void {
    /**
     * Load textures
     */
    this.assetManager.loadBakedTextures()
    this.assetManager.loadLightMapTextures()
    this.assetManager.loadAdsTemplateTextures()
    this.assetManager.loadExampleVideos()

    /**
     * Load models
     */
    this.assetManager.loadLevelVisualModel()
    this.assetManager.loadLevelPhysicsModel()
    this.assetManager.loadCarVisualModel()
    this.assetManager.loadAdsVisualModel()
    this.assetManager.loadCharacterVisualModel()
  }
}
