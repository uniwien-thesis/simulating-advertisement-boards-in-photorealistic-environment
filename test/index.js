const world = new CANNON.World()
const scene = new THREE.Scene()

const velocity = new THREE.Vector3()
const acceleration = new THREE.Vector3(1, 0.125, 50.0)
const decceleration = new THREE.Vector3(-0.0005, -0.0001, -5.0)

const directionInput = {
  forward: false,
  backward: false,
  left: false,
  right: false,
  space: false,
}

/**
 * Default threejs and cannon materials
 */
const defaultMaterial = new CANNON.Material('default')
const defaultContactMaterial = new CANNON.ContactMaterial(
  defaultMaterial,
  defaultMaterial,
  {
    friction: 0.0,
    restitution: 0.7,
  },
)
world.defaultContactMaterial = defaultContactMaterial
/**
 * Setup CANNON world
 */
world.gravity.set(0, -9.81, 0)
world.broadphase = new CANNON.SAPBroadphase(world)
world.solver.iterations = 10
world.allowSleep = true
world.addContactMaterial(defaultContactMaterial)

const debugMaterial = new THREE.MeshBasicMaterial({
  color: new THREE.Color(0xff0000),
  wireframe: true,
})

/**
 * Setup Renderer
 */
let prevTick = 0

const renderer = new THREE.WebGLRenderer({
  antialias: true,
})
renderer.setPixelRatio(window.devicePixelRatio)
renderer.setSize(window.innerWidth, window.innerHeight)
renderer.domElement.id = 'threejs'
renderer.setClearColor(new THREE.Color(0xffffff), 1)
document.getElementById('container')?.appendChild(renderer.domElement)

/**
 * Set event listeners
 */
window.addEventListener(
  'resize',
  () => {
    onWindowResize()
  },
  false,
)
addKeiListener()

/**
 * Setup Camera
 */
const fov = 75
const aspect = window.innerWidth / window.innerHeight
const near = 1.0
const far = 10000.0
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far)
camera.position.set(0, 10, 15)
camera.lookAt(0, 0, 0)
const controls = new THREE.OrbitControls(camera, renderer.domElement)

/**
 * Setup Objects in scene
 */

const objects = []

const floor = addFloor()
const cube = addCube({
  size: new CANNON.Vec3(5, 5, 5),
  pos: new CANNON.Vec3(0, 5, 0),
  color: 0x0000ff,
  linearDamping: 0.9,
  angularDamping: 0.9,
  // mass: 0
})
const wall = addCube({
  size: new CANNON.Vec3(10, 10, 10),
  pos: new CANNON.Vec3(10, 5, 0),
  color: 0xffff00,

  mass: 0,
})
objects.push(floor)
objects.push(cube)
objects.push(wall)

/**
 * TICK
 */
function tick(timeElapsed = 0) {
  const dt = (timeElapsed - prevTick) / 1000
  prevTick = timeElapsed

  controls.update()
  renderer.render(scene, camera)
  world.step(1 / 60, dt, 3)

  // // slowdown
  // const frameDecceleration = new THREE.Vector3(0, 0, 0)
  // frameDecceleration.multiplyVectors(cube.body.velocity, decceleration)
  // frameDecceleration.multiplyScalar(dt)

  // frameDecceleration.z =
  //   Math.sign(frameDecceleration.z) *
  //   Math.min(Math.abs(frameDecceleration.z), Math.abs(cube.body.velocity.z))

  // velocity.add(frameDecceleration)

  // cube.body.velocity.copy(cube.body.velocity.vadd(frameDecceleration))

  velocity.set(0, 0, 0)

  const currentQuaternion = new THREE.Quaternion(
    cube.body.quaternion.x,
    cube.body.quaternion.y,
    cube.body.quaternion.z,
    cube.body.quaternion.w,
  )

  // react to input
  if (directionInput.forward) {
    velocity.z += acceleration.z * dt
  }
  if (directionInput.backward) {
    velocity.z -= acceleration.z * dt
  }

  const pressed = Object.values(directionInput).some((v) =>
    Boolean(v),
  )
  if (pressed) {
    console.log(cube.body.velocity.z)
  }

  const axis = new THREE.Vector3()
  if (directionInput.left) {
    axis.set(0, 1, 0)
    const resultQuaternion = new THREE.Quaternion()

    resultQuaternion.setFromAxisAngle(
      axis,
      4.0 * Math.PI * dt * acceleration.y,
    )

    currentQuaternion.multiply(resultQuaternion)
  }
  if (directionInput.right) {
    axis.set(0, 1, 0)
    const resultQuaternion = new THREE.Quaternion()

    resultQuaternion.setFromAxisAngle(
      axis,
      4.0 * -Math.PI * dt * acceleration.y,
    )

    currentQuaternion.multiply(resultQuaternion)
  }

  velocity.applyQuaternion(currentQuaternion)

  cube.body.velocity.x += velocity.x
  cube.body.velocity.z += velocity.z

  

  cube.body.quaternion.copy(currentQuaternion)

  objects.forEach(({ body, wireframe }) => {
    wireframe.position.copy(body.position)
    wireframe.quaternion.copy(body.quaternion)
  })

  requestAnimationFrame((t) => tick(t))
}

function addCube({
  size,
  pos,
  mass = 1,
  color = 0x0000ff,
  linearDamping = 0.01,
  angularDamping = 0.01,
}) {
  const boxGeometry = new THREE.BoxGeometry(size.x, size.y, size.z)

  const boxBody = new CANNON.Body({
    shape: new CANNON.Box(
      new CANNON.Vec3(size.x / 2, size.y / 2, size.z / 2),
    ),
    mass,
    material: defaultMaterial,
    linearDamping,
    angularDamping,
  })
  boxBody.position.set(pos.x, pos.y, pos.z)

  boxBody.addEventListener('collide', () => {
    console.log('collission')
  })
  world.add(boxBody)

  const wireframeMesh = new THREE.Mesh(boxGeometry, debugMaterial)
  wireframeMesh.position.set(
    boxBody.position.x,
    boxBody.position.y,
    boxBody.position.z,
  )
  scene.add(wireframeMesh)

  return {
    body: boxBody,
    wireframe: wireframeMesh,
  }
}

function addFloor() {
  const planeBody = new CANNON.Body({
    shape: new CANNON.Plane(),
    mass: 0,
    material: defaultMaterial,
  })
  const planeGeometry = new THREE.PlaneGeometry(50, 50)
  const planeMesh = new THREE.Mesh(planeGeometry, debugMaterial)

  planeBody.quaternion.setFromAxisAngle(
    new CANNON.Vec3(1, 0, 0),
    -Math.PI * 0.5,
  )
  world.add(planeBody)

  planeMesh.position.set(
    planeBody.position.x,
    planeBody.position.y,
    planeBody.position.z,
  )
  planeMesh.quaternion.set(
    planeBody.quaternion.x,
    planeBody.quaternion.y,
    planeBody.quaternion.z,
    planeBody.quaternion.w,
  )

  scene.add(planeMesh)

  return {
    body: planeBody,
    wireframe: planeMesh,
  }
}

function onWindowResize() {
  if (camera) {
    camera.aspect = window.innerWidth / window.innerHeight
    camera.updateProjectionMatrix()
  }

  renderer?.setSize(window.innerWidth, window.innerHeight)
  renderer?.setPixelRatio(Math.min(window.devicePixelRatio, 2))
}

function onKeyDown(event) {
  console.log('onKeyDown')
  switch (event.keyCode) {
    case 87: // w
      directionInput.forward = true
      break
    case 65: // a
      directionInput.left = true
      break
    case 83: // s
      directionInput.backward = true
      break
    case 68: // d
      directionInput.right = true
      break
    case 32: // SPACE
      directionInput.space = true
      break
  }
}

function onKeyUp(event) {
  switch (event.keyCode) {
    case 87: // w
      directionInput.forward = false
      break
    case 65: // a
      directionInput.left = false
      break
    case 83: // s
      directionInput.backward = false
      break
    case 68: // d
      directionInput.right = false
      break
    case 32: // SPACE
      directionInput.space = false
      break
  }
}

function addKeiListener() {
  document.addEventListener('keydown', (e) => onKeyDown(e), false)
  document.addEventListener('keyup', (e) => onKeyUp(e), false)
}

tick()
